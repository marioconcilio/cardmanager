package com.marioconcilio.cardmanager.util;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class FormatterTests {

	@Test
	public void testParseDate() {
		Calendar calendar = new GregorianCalendar(1989, 0, 9);
		Date date = Formatter.parseDate("09/01/1989");
		
		assertTrue(date.compareTo(calendar.getTime()) == 0);
	}

	@Test
	public void testParseDateWithoutMask() {
		Calendar calendar = new GregorianCalendar(1989, 0, 9);
		Date date = Formatter.parseDateWithoutMask("09011989");
		
		assertTrue(date.compareTo(calendar.getTime()) == 0);
	}

	@Test
	public void testFormatDate() {
		Calendar calendar = new GregorianCalendar(1989, 0, 9);
		
		assertEquals("09/01/1989", Formatter.formatDate(calendar.getTime()));
	}

	@Test
	public void testParseCurrency() {
		assertTrue(new BigDecimal("5.6").compareTo(Formatter.parseCurrency("R$ 5,60")) == 0);
		assertTrue(new BigDecimal(2000).compareTo(Formatter.parseCurrency("2.000,00")) == 0);
		assertTrue(new BigDecimal("1234.56").compareTo(Formatter.parseCurrency("1.234,56")) == 0);
	}
	
	@Test
	public void testParseCurrencyToInt() {
		assertEquals(123, Formatter.parseCurrencyToInt("R$ 123,00"));
		assertEquals(1234, Formatter.parseCurrencyToInt("R$ 1.234,56"));
		assertEquals(200, Formatter.parseCurrencyToInt("200,00"));
	}

	@Test
	public void testFormatCurrencyBigDecimal() {
		assertEquals("R$ 5,60", Formatter.formatCurrency(new BigDecimal("5.6")));
		assertEquals("R$ 2.000,00", Formatter.formatCurrency(new BigDecimal(2000)));
		assertEquals("R$ 1.234,56", Formatter.formatCurrency(new BigDecimal(1234.56)));
	}

	@Test
	public void testFormatCurrencyDouble() {
		assertEquals("R$ 5,60", Formatter.formatCurrency(5.6));
		assertEquals("R$ 2.000,00", Formatter.formatCurrency(2000));
		assertEquals("R$ 1.234,56", Formatter.formatCurrency(1234.56));
	}

	@Test
	public void testParseCnpj() {
		assertEquals(1L, Formatter.parseCnpj("00.000.000/0000-01"));
		assertEquals(1234567000189L, Formatter.parseCnpj("01.234.567/0001-89"));
		assertEquals(11234567000189L, Formatter.parseCnpj("11.234.567/0001-89"));
	}

	@Test
	public void testFormatCnpj() {
		assertEquals("00.000.000/0000-01", Formatter.formatCnpj(1L));
		assertEquals("01.234.567/0001-89", Formatter.formatCnpj(1234567000189L));
		assertEquals("11.234.567/0001-89", Formatter.formatCnpj(11234567000189L));
	}

	@Test
	public void testParseCep() {
		assertEquals(6030180, Formatter.parseCep("06030-180"));
		assertEquals(12345678, Formatter.parseCep("12345-678"));
	}

	@Test
	public void testFormatCep() {
		assertEquals("06030-180", Formatter.formatCep(6030180));
		assertEquals("12345-678", Formatter.formatCep(12345678));
	}

	@Test
	public void testFormatBillNumber() {
		assertEquals("005", Formatter.formatBillNumber(5));
		assertEquals("056", Formatter.formatBillNumber(56));
		assertEquals("567", Formatter.formatBillNumber(567));
		assertEquals("123 001", Formatter.formatBillNumber(123001));
	}
	
	@Test
	public void testUpperCaseFirst() {
		assertEquals("One two three", Formatter.upperCaseFirst("one two three"));
		assertEquals("Mario", Formatter.upperCaseFirst("mario"));
	}
	
	@Test
	public void testUpperCaseAll() {
		assertEquals("One Two Three", Formatter.upperCaseAll("one two three"));
		assertEquals("Mario", Formatter.upperCaseAll("mario"));
	}
	
	@Test
	public final void testParseLong() {
		long number = Formatter.parseLong("527");
		assertThat(number, is(527L));
		
		number = Formatter.parseLong("527 001");
		assertThat(number, is(527001L));
		
		number = Formatter.parseLong("527001");
		assertThat(number, is(527001L));
		
		number = Formatter.parseLong("asd");
		assertThat(number, is(-1L));
	}
	
	@Test
	public final void testParseInt() {
		int number = Formatter.parseInt("527");
		assertThat(number, is(527));
		
		number = Formatter.parseInt("527 001");
		assertThat(number, is(527001));
		
		number = Formatter.parseInt("527001");
		assertThat(number, is(527001));
		
		number = Formatter.parseInt("asd");
		assertThat(number, is(-1));
	}

}
