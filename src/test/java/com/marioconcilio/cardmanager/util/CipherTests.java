package com.marioconcilio.cardmanager.util;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class CipherTests {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testDigestSHA512() {
		System.out.println("ozzy2109: " + Cipher.digestSHA512("ozzy2109"));
		System.out.println("ariane: " + Cipher.digestSHA512("ariane"));
	}

}
