package com.marioconcilio.cardmanager.controller;

import com.marioconcilio.cardmanager.model.vo.CieloCard;
import org.junit.*;

import java.io.File;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class CieloControllerTests {

    private static CieloController controller;

    @BeforeClass
    public static void setUp() throws Exception {
        controller = new CieloController();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        controller = null;
    }

    @Test
    public void testProcessExtrato() throws Exception {
        final File file = new File(getClass().getClassLoader().getResource("cielo.txt").getFile());
        final List<CieloCard> list = controller.processExtrato(file);

        list.forEach(System.out::println);
        assertThat(list, notNullValue());
        assertThat(list, hasSize(36));
    }
}
