package com.marioconcilio.cardmanager.controller;

import com.marioconcilio.cardmanager.model.vo.Bill;
import com.marioconcilio.cardmanager.model.vo.Client;
import com.marioconcilio.cardmanager.util.DateUtils;
import org.junit.*;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.exparity.hamcrest.date.DateMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class BillControllerTests {

	private static BillController sut;

    @BeforeClass
    public static void setUp() throws Exception {
        sut = new BillController();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        sut = null;
    }

    @Test
	public void testProcessRemessa() throws Exception {
        final File file1 = new File(getClass().getClassLoader().getResource("CBR1702.128").getFile());
        final List<Bill> list1 = sut.processRemessa(file1);
        assertThat(list1, notNullValue());
        assertThat(list1, hasSize(35));

        final File file2 = new File(getClass().getClassLoader().getResource("CBR0603.129").getFile());
        final List<Bill> list2 = sut.processRemessa(file2);
        assertThat(list2, notNullValue());
        assertThat(list2, hasSize(24));
	}

    @Test
    public void testProcessListRemessa() throws Exception {
        final File file1 = new File(getClass().getClassLoader().getResource("CBR1702.128").getFile());
        final File file2 = new File(getClass().getClassLoader().getResource("CBR0603.129").getFile());
        final List<File> files = Arrays.asList(file1, file2);
        final List<Bill> list = sut.processListRemessa(files);
        assertThat(list, notNullValue());
        assertThat(list, hasSize(59));
    }

    @Test
	public void testProcessRetorno() throws IOException {
        final File file1 = new File(getClass().getClassLoader().getResource("RETORNO1.ret").getFile());
        final List<Bill> list1 = sut.processRetorno(file1);
        assertThat(list1, notNullValue());
        assertThat(list1, containsInAnyOrder(
                hasProperty("id", is(563L)),
                hasProperty("id", is(502006L)),
                hasProperty("id", is(545007L)),
                hasProperty("id", is(564L)),
                hasProperty("id", is(569L))
        ));

        final File file2 = new File(getClass().getClassLoader().getResource("RETORNO2.ret").getFile());
        final List<Bill> list2 = sut.processRetorno(file2);
        assertThat(list2, notNullValue());
        assertThat(list2, contains(
                hasProperty("id", is(565001L))
        ));

        final File file3 = new File(getClass().getClassLoader().getResource("RETORNO3.ret").getFile());
        final List<Bill> list3 = sut.processRetorno(file3);
        assertThat(list3, notNullValue());
        assertThat(list3, containsInAnyOrder(
                hasProperty("id", is(472L)),
                hasProperty("id", is(456001L)),
                hasProperty("id", is(480L))
        ));
	}

    @Test
    public void testProcessListRetorno() throws Exception {
        final File file1 = new File(getClass().getClassLoader().getResource("RETORNO1.ret").getFile());
        final File file2 = new File(getClass().getClassLoader().getResource("RETORNO2.ret").getFile());
        final File file3 = new File(getClass().getClassLoader().getResource("RETORNO3.ret").getFile());
        final List<File> files = Arrays.asList(file1, file2, file3);
        final List<Bill> list = sut.processListRetorno(files);
        assertThat(list, notNullValue());
        assertThat(list, contains(
                hasProperty("id", is(472L)),
                hasProperty("id", is(456001L)),
                hasProperty("id", is(480L)),
                hasProperty("id", is(563L)),
                hasProperty("id", is(502006L)),
                hasProperty("id", is(545007L)),
                hasProperty("id", is(564L)),
                hasProperty("id", is(569L)),
                hasProperty("id", is(565001L))
        ));
    }

    @Test
    public void testRecoverBill() throws Exception {
        final String line1 = "0010001300001P 010181040000000047406 25130654245440536   71112572001         2003201700000000007985000000002N15022017100000000000000000000026000000000000000000000000000000000000000000000000000000572                      2052000090019008869 ";
        final String line2 = "0010001300002Q 012057305153000164GRAFICA CART LTDA ME                    AV SALVADOR SINDONA, 157                               06050210OSASCO         SP00000000000000000                                       0000000000000000000000012345678";
        final Bill bill = sut.recoverBill(line1, line2);

        assertThat(bill, notNullValue());
        assertThat(bill.getClient(), is(new Client(57305153000164L)));
        assertThat(bill.getId(), is(572001L));
        assertThat(bill.getValue(), closeTo(BigDecimal.valueOf(798.50), BigDecimal.valueOf(0.001)));
        assertThat(bill.getDate(), sameDay(DateUtils.asDate(2017, 3, 20)));
        assertThat(bill.getStatus(), is(Bill.BillStatus.ABERTO));
    }

    @Test
    public void testUpdateBill() throws Exception {
        final String line = " 7 25130654245440369 502006       GRAFICA CART LTDA ME         01032017 0703  1981 LQB              831,50             26,57J      3,85";
        final Bill bill = sut.updateBill(line);

        assertThat(bill, notNullValue());
        assertThat(bill.getClient(), is(new Client(57305153000164L)));
        assertThat(bill.getId(), is(502006L));
        assertThat(bill.getValue(), closeTo(BigDecimal.valueOf(831.50), BigDecimal.valueOf(0.001)));
        assertThat(bill.getDate(), sameDay(DateUtils.asDate(2017, 3, 1)));
        assertThat(bill.getStatus(), is(Bill.BillStatus.PAGO));
    }

    @Test
    public void testSortRetornoFiles() throws Exception {
        final File file1 = new File(getClass().getClassLoader().getResource("RETORNO1.ret").getFile());
        final File file2 = new File(getClass().getClassLoader().getResource("RETORNO2.ret").getFile());
        final File file3 = new File(getClass().getClassLoader().getResource("RETORNO3.ret").getFile());

        assertThat(sut.sortRetornoFiles(file1, file1), is(0));
        assertThat(sut.sortRetornoFiles(file1, file2), is(-1));
        assertThat(sut.sortRetornoFiles(file1, file3), is(1));
        assertThat(sut.sortRetornoFiles(file2, file1), is(1));
        assertThat(sut.sortRetornoFiles(file2, file2), is(0));
        assertThat(sut.sortRetornoFiles(file2, file3), is(1));
        assertThat(sut.sortRetornoFiles(file3, file1), is(-1));
        assertThat(sut.sortRetornoFiles(file3, file2), is(-1));
        assertThat(sut.sortRetornoFiles(file3, file3), is(0));
    }
}
