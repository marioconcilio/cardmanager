package com.marioconcilio.cardmanager.controller;

import com.marioconcilio.cardmanager.model.vo.PSMCard;
import com.marioconcilio.cardmanager.model.vo.Card.PSMCardType;
import com.marioconcilio.cardmanager.util.DateUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class PSMControllerTests {
	
	private static PSMController controller;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		controller = new PSMController();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		controller = null;
	}

	@Test
	public final void testProcessExtrato() throws Exception {
		final File file = new File(getClass().getClassLoader().getResource("pagseguro.xml").getFile());
		final List<PSMCard> list = controller.processExtrato(file);
		
		assertThat(list, notNullValue());
		assertThat(list, hasSize(83));
	}

	@SuppressWarnings("unchecked")
	@Test
	public final void testProcessList() throws Exception {
		// cards should be found
		final PSMCard card2 = new PSMCard(DateUtils.asDate(2016, 12, 21), PSMCardType.CREDITO_AVISTA, new BigDecimal("50.00"));
		final PSMCard card1 = new PSMCard(DateUtils.asDate(2016, 12, 16), PSMCardType.DEBITO, new BigDecimal("56.00"));
		final PSMCard card5 = new PSMCard(DateUtils.asDate(2016, 12, 19), PSMCardType.DEBITO, new BigDecimal("320.00")); // joined ---\
		final PSMCard card6 = new PSMCard(DateUtils.asDate(2016, 12, 19), PSMCardType.DEBITO, new BigDecimal("261.00"));	// joined ---/
		final PSMCard card0 = new PSMCard(DateUtils.asDate(2016, 12, 19), PSMCardType.DEBITO, new BigDecimal("38.00"));
		
		// cards should NOT be found
		final PSMCard card3 = new PSMCard(DateUtils.asDate(2016, 12, 22), PSMCardType.DEBITO, new BigDecimal("15.00"));
		final PSMCard card4 = new PSMCard(DateUtils.asDate(2016, 12, 14), PSMCardType.DEBITO, new BigDecimal("666.00"));
		final PSMCard card7 = new PSMCard(DateUtils.asDate(2016, 12, 19), PSMCardType.DEBITO, new BigDecimal("266.00"));
		final PSMCard card8 = new PSMCard(DateUtils.asDate(2016, 12, 16), PSMCardType.DEBITO, new BigDecimal("556.00"));
		final PSMCard card9 = new PSMCard(DateUtils.asDate(2016, 12, 16), PSMCardType.DEBITO, new BigDecimal("6.00"));
		
		final List<PSMCard> list = new ArrayList<>();
		list.add(card1);
		list.add(card2);
		list.add(card3);
		list.add(card4);
		list.add(card5);
		list.add(card6);
		list.add(card7);
		list.add(card8);
		list.add(card9);
		list.add(card0);
		
		final List<PSMCard> processedList = controller.processList(list);
		assertThat(processedList, notNullValue());
		assertThat(processedList, hasSize(5));
		assertThat(processedList, containsInAnyOrder(
				hasProperty("value", is(new BigDecimal("50.00"))),
				hasProperty("value", is(new BigDecimal("56.00"))),
				hasProperty("value", is(new BigDecimal("320.00"))),
				hasProperty("value", is(new BigDecimal("261.00"))),
				hasProperty("value", is(new BigDecimal("38.00")))
				));

		final List<PSMCard> notFound = controller.getNotFound();
		assertThat(notFound, notNullValue());
		assertThat(notFound, hasSize(5));		
		assertThat(notFound, containsInAnyOrder(
				hasProperty("value", is(new BigDecimal("15.00"))),
				hasProperty("value", is(new BigDecimal("666.00"))),
				hasProperty("value", is(new BigDecimal("266.00"))),
				hasProperty("value", is(new BigDecimal("556.00"))),
				hasProperty("value", is(new BigDecimal("6.00")))
				));
	}
	
	@Test
	public final void testCardsSubsetSum() {
		final Date date = DateUtils.asDate(2016, 12, 19);
		final PSMCardType type = PSMCardType.DEBITO;
		
		final PSMCard card1 = new PSMCard(date, type, new BigDecimal("320.00"));
		final PSMCard card2 = new PSMCard(date, type, new BigDecimal("261.00"));
		final PSMCard card3 = new PSMCard(date, type, new BigDecimal("26.00"));
		final PSMCard card4 = new PSMCard(date, type, new BigDecimal("35.00"));
		final PSMCard card5 = new PSMCard(date, type, new BigDecimal("452.00"));
		final PSMCard card6 = new PSMCard(date, type, new BigDecimal("395.00"));
		final PSMCard card7 = new PSMCard(date, type, new BigDecimal("11.00"));
		final PSMCard card8 = new PSMCard(date, type, new BigDecimal("200.00"));
		final PSMCard card9 = new PSMCard(date, type, new BigDecimal("267.00"));
		
		final List<PSMCard> list = new ArrayList<>();
		list.add(card1);
		list.add(card2);
		list.add(card3);
		list.add(card4);
		list.add(card5);
		list.add(card6);
		list.add(card7);
		list.add(card8);
		list.add(card9);
		
		final List<PSMCard> result = controller.cardsSubsetSum(list, new BigDecimal("1018.00"));
		assertThat(result, notNullValue());
		assertThat(result, hasSize(5));
		assertThat(result, containsInAnyOrder(card1, card4, card5, card7, card8));
	}
	
}
