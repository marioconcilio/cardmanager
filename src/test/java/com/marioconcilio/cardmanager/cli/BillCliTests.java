package com.marioconcilio.cardmanager.cli;

import com.marioconcilio.cardmanager.model.vo.Bill;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.InputStreamReader;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class BillCliTests {
	
	private static BillCli cli;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final MyBufferedReader br = new MyBufferedReader(new InputStreamReader(System.in));
		cli = new BillCli(br);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		cli = null;
	}
	
	@Test
	public final void testSortBillNumber() {
		// 527 & 527
		Bill b1 = new Bill(527);
		Bill b2 = new Bill(527);
		int result = cli.sortBillNumber(b1, b2);
		assertThat(result, is(0));
		
		// 526 & 527
		b1 = new Bill(526);
		result = cli.sortBillNumber(b1, b2);
		assertThat(result, is(-1));
		
		// 526 & 520
		b2 = new Bill(520);
		result = cli.sortBillNumber(b1, b2);
		assertThat(result, is(1));
		
		// 527 001 & 528
		b1 = new Bill(527001);
		b2 = new Bill(528);
		result = cli.sortBillNumber(b1, b2);
		assertThat(result, is(-1));
		
		// 528 & 527 001
		result = cli.sortBillNumber(b2, b1);
		assertThat(result, is(1));
		
		// 527 001 & 527 002
		b1 = new Bill(527001);
		b2 = new Bill(527002);
		result = cli.sortBillNumber(b1, b2);
		assertThat(result, is(-1));
		
		// 527 002 & 527 001
		result = cli.sortBillNumber(b2, b1);
		assertThat(result, is(1));
	}

}
