package com.marioconcilio.cardmanager.cli;

import com.marioconcilio.cardmanager.model.vo.Card.PSMCardType;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

import static org.exparity.hamcrest.date.DateMatchers.sameDay;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class PSMCliTests {
	
	private static PSMCli cli;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final MyBufferedReader br = new MyBufferedReader(new InputStreamReader(System.in));
		cli = new PSMCli(br);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		cli = null;
	}

	@Test
	public final void testParseDate() {
		Date date = cli.parseDate("20/01/17");
		assertThat(date, notNullValue());
		assertThat(date, sameDay(LocalDate.of(2017, 1, 20)));
		
		date = cli.parseDate("20/01/2017");
		assertThat(date, notNullValue());
		assertThat(date, sameDay(LocalDate.of(2017, 1, 20)));
		
		date = cli.parseDate("asd");
		assertThat(date, nullValue());
		
		date = cli.parseDate("200");
		assertThat(date, nullValue());
	}
	
	@Test
	public final void testParseValue() {
		BigDecimal value = cli.parseValue("123.45");
		assertThat(value, notNullValue());
		assertThat(value, is(new BigDecimal("123.45")));
		
		value = cli.parseValue("123,45");
		assertThat(value, nullValue());
		
		value = cli.parseValue("asd");
		assertThat(value, nullValue());
	}
	
	@Test
	public final void testParseCardType() {
		PSMCardType type = cli.parseCardType(PSMCli.CMD_DEBIT);
		assertThat(type, notNullValue());
		assertThat(type, is(PSMCardType.DEBITO));
		
		type = cli.parseCardType(PSMCli.CMD_CV);
		assertThat(type, notNullValue());
		assertThat(type, is(PSMCardType.CREDITO_AVISTA));
		
		type = cli.parseCardType(PSMCli.CMD_CP);
		assertThat(type, notNullValue());
		assertThat(type, is(PSMCardType.CREDITO_PARCELADO));
		
		type = cli.parseCardType("asd");
		assertThat(type, nullValue());
	}

}
