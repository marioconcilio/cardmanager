package com.marioconcilio.cardmanager.model.vo;

import com.marioconcilio.cardmanager.model.vo.Product.Currency;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class TintaTests {
	
	private static Tinta tinta;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		tinta = new Tinta();
		tinta.setNome("Amarelo Europa");
		tinta.setMarca("Cromos");
		tinta.setPrecoKg(BigDecimal.valueOf(37.5));
		tinta.setUn(BigDecimal.valueOf(2.50));
		tinta.setCurrency(Currency.BRL);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		tinta = null;
	}

	@Test
	public final void testEqualsObject() {
		Tinta t1 = new Tinta();
		t1.setNome("Vermelho");
		t1.setMarca("Cromos");
		
		Tinta t2 = new Tinta();
		t2.setNome("Vermelho");
		t2.setMarca("Cromos");
		
		Tinta t3 = new Tinta();
		t3.setNome("Azul");
		t3.setMarca("Flint");
		
		Tinta t4 = t1;
		
		assertThat(t1, is(t2));
		assertThat(t1, is(t4));
		assertThat(t1, is(not(t3)));
	}
	
	@Test
	public final void testUnProperty() {
		assertThat(tinta.unProperty().get(), is("2.5kg"));
	}
	
	@Test
	public final void testDescriptionProperty() {
		assertThat(tinta.descriptionProperty().get(), is("Amarelo Europa"));
	}
	
	@Test
	public final void testDetailProperty() {
		assertThat(tinta.detailProperty().get(), is("Cromos"));
	}
	
	@Test
	public final void testPrecoUn() {
		assertThat(tinta.precoUn(), is(BigDecimal.valueOf(93.75)));
	}

}
