package com.marioconcilio.cardmanager.model.vo;

import com.marioconcilio.cardmanager.model.vo.Product.Currency;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class BlanquetaTests {
	
	private static Blanqueta blanq;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		blanq = new Blanqueta();
		blanq.setLarg(260);
		blanq.setComp(471);
		blanq.setNome("Abdick 360");
		blanq.setBlanquetaLona(new BlanquetaLona(3, BigDecimal.valueOf(135.00)));
		blanq.setCurrency(Currency.USD);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		blanq = null;
	}

	@Test
	public final void testUnProperty() {
		assertThat(blanq.unProperty().get(), is("Un"));
	}
	
	@Test
	public final void testDescriptionProperty() {
		assertThat(blanq.descriptionProperty().get(), is("260x471 3 lonas"));
	}
	
	@Test
	public final void testDetailProperty() {
		assertThat(blanq.detailProperty().get(), is("Abdick 360"));
	}
	
	@Test
	public final void testPrecoUn() {
		assertThat(blanq.precoUn(), closeTo(BigDecimal.valueOf(59.52), BigDecimal.valueOf(0.01)));
	}

}
