package com.marioconcilio.cardmanager.model.vo;

import com.marioconcilio.cardmanager.model.vo.Product.Currency;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class ChapaTests {
	
	private static Chapa chapa;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		chapa = new Chapa();
		chapa.setLarg(250);
		chapa.setComp(370);
		chapa.setCurrency(Currency.USD);
		chapa.setChapaMarca(new ChapaMarca("IBF", "0.15", BigDecimal.valueOf(15.50)));
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		chapa = null;
	}

	@Test
	public final void testUnProperty() {
		assertThat(chapa.unProperty().get(), is("Un"));
	}
	
	@Test
	public final void testDescriptionProperty() {
		assertThat(chapa.descriptionProperty().get(), is("250x370"));
	}
	
	@Test
	public final void testDetailProperty() {
		assertThat(chapa.detailProperty().get(), is("IBF"));
	}
	
	@Test
	public final void testPrecoUn() {
		assertThat(chapa.precoUn(), closeTo(BigDecimal.valueOf(5.16), BigDecimal.valueOf(0.01)));
	}

}
