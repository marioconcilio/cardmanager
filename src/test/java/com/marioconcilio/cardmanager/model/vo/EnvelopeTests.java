package com.marioconcilio.cardmanager.model.vo;

import com.marioconcilio.cardmanager.model.vo.Product.Currency;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class EnvelopeTests {
	
	private static Envelope ev1, ev2;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		// 097x125 90g Branco Cx c/ 250
		ev1 = new Envelope();
		ev1.setLarg(97);
		ev1.setComp(125);
		ev1.setGrs(90);
		ev1.setTipo("Branco");
		ev1.setUn(250);
		ev1.setPrecoMilheiro(BigDecimal.valueOf(96.00));
		ev1.setCurrency(Currency.BRL);
		
		ev2 = new Envelope();
		ev2.setLarg(114);
		ev2.setComp(229);
		ev2.setGrs(75);
		ev2.setTipo("Branco");
		ev2.setUn(1000);
		ev2.setNome("Oficio");
		ev2.setPrecoMilheiro(BigDecimal.valueOf(60.00));
		ev2.setCurrency(Currency.BRL);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ev1 = null;
		ev2 = null;
	}

	@Test
	public final void testUnProperty() {
		assertThat(ev1.unProperty().get(), is("Cx c/ 250"));
		assertThat(ev2.unProperty().get(), is("Cx c/ 1000"));
	}
	
	@Test
	public final void testDescriptionProperty() {
		assertThat(ev1.descriptionProperty().get(), is("097x125 90g"));
		assertThat(ev2.descriptionProperty().get(), is("114x229 75g"));
	}
	
	@Test
	public final void testDetailProperty() {
		assertThat(ev1.detailProperty().get(), is("Branco"));
		assertThat(ev2.detailProperty().get(), is("Oficio Branco"));
	}
	
	@Test
	public final void testPrecoUn() {
		assertThat(ev1.precoUn(), is(BigDecimal.valueOf(24).setScale(2)));
		assertThat(ev2.precoUn(), is(BigDecimal.valueOf(60).setScale(2)));
	}
}
