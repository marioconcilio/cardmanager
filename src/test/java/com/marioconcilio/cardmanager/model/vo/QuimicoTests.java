package com.marioconcilio.cardmanager.model.vo;

import com.marioconcilio.cardmanager.model.vo.Product.Currency;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class QuimicoTests {
	
	private static Quimico quimico;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		quimico = new Quimico();
		quimico.setNome("Solucao de Fonte");
		quimico.setMarca("DUPLIFONT 1203");
		quimico.setUn("5l");
		quimico.setPreco(BigDecimal.valueOf(22.50));
		quimico.setCurrency(Currency.BRL);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		quimico = null;
	}

	@Test
	public final void testUnProperty() {
		assertThat(quimico.unProperty().get(), is("5l"));
	}
	
	@Test
	public final void testDescriptionProperty() {
		assertThat(quimico.descriptionProperty().get(), is("Solucao de Fonte"));
	}
	
	@Test
	public final void testDetailProperty() {
		assertThat(quimico.detailProperty().get(), is("DUPLIFONT 1203"));
	}
	
	@Test
	public final void testPrecoUn() {
		assertThat(quimico.precoUn(), is(BigDecimal.valueOf(22.50)));
	}

}
