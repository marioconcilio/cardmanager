package com.marioconcilio.cardmanager.model.vo;

import com.marioconcilio.cardmanager.model.vo.Product.Currency;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class DiversosTests {
	
	private static Diversos d1, d2;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		d1 = new Diversos();
		d1.setNome("Arame 20");
		d1.setUn("Rolo");
		d1.setPreco(BigDecimal.valueOf(50.0));
		d1.setCurrency(Currency.BRL);
		
		d2 = new Diversos();
		d2.setNome("Vegetal A4 (210x297)");
		d2.setUn("Cx (100fls)");
		d2.setPreco(BigDecimal.valueOf(7.5));
		d2.setCurrency(Currency.USD);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		d1 = null;
		d2 = null;
	}

	@Test
	public final void testUnProperty() {
		assertThat(d1.unProperty().get(), is("Rolo"));
		assertThat(d2.unProperty().get(), is("Cx (100fls)"));
	}
	
	@Test
	public final void testDescriptionProperty() {
		assertThat(d1.descriptionProperty().get(), is("Arame 20"));
		assertThat(d2.descriptionProperty().get(), is("Vegetal A4 (210x297)"));
	}
	
	@Test
	public final void testDetailProperty() {
		assertThat(d1.detailProperty().get(), is(nullValue()));
		assertThat(d2.detailProperty().get(), is(nullValue()));
	}
	
	@Test
	public final void testPrecoUn() {
		assertThat(d1.precoUn(), closeTo(BigDecimal.valueOf(50), BigDecimal.valueOf(0.01)));
		assertThat(d2.precoUn(), closeTo(BigDecimal.valueOf(27), BigDecimal.valueOf(0.01)));
	}

}
