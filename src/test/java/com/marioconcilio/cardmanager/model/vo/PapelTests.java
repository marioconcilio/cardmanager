package com.marioconcilio.cardmanager.model.vo;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class PapelTests {
	
	private static Papel papel;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		PapelTipo tipo = new PapelTipo();
		tipo.setId(0);
		tipo.setTipo("Offset");
		tipo.setDescricao("Offset até 150g");
		tipo.setPrecoKg(new BigDecimal("5.40"));
		
		papel = new Papel();
		papel.setPapelTipo(tipo);
		papel.setGrs(50);
		papel.setLargura(66);
		papel.setComprimento(96);
		papel.setFls(500);
		papel.setPeso(new BigDecimal("15.840"));
		papel.setMarca("Multiverde");
		papel.setQtde(42);
		
//		tipo.getPapeis().add(papel);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		papel = null;
	}

	@Test
	public void testGetPapelTipo() {
		PapelTipo tipo = papel.getPapelTipo();
		assertNotNull(tipo);
		assertEquals(tipo.getTipo(), "Offset");
		assertEquals(tipo.getDescricao(), "Offset até 150g");
		assertTrue(tipo.getPrecoKg().compareTo(new BigDecimal("5.40")) == 0);
	}

	@Test
	public void testPapelTipoProperty() {
		PapelTipo tipo = papel.papelTipoProperty().get();
		assertNotNull(tipo);
		assertEquals(tipo.getTipo(), "Offset");
		assertEquals(tipo.getDescricao(), "Offset até 150g");
		assertTrue(tipo.getPrecoKg().compareTo(new BigDecimal("5.40")) == 0);
	}

	@Test
	public void testGetGrs() {
		assertEquals(papel.getGrs(), 50);
	}

	@Test
	public void testGrsProperty() {
		assertEquals(papel.grsProperty().get(), 50);
	}

	@Test
	public void testGetLargura() {
		assertEquals(papel.getLargura(), 66);
	}

	@Test
	public void testLarguraProperty() {
		assertEquals(papel.larguraProperty().get(), 66);
	}

	@Test
	public void testGetComprimento() {
		assertEquals(papel.getComprimento(), 96);
	}

	@Test
	public void testComprimentoProperty() {
		assertEquals(papel.comprimentoProperty().get(), 96);
	}

	@Test
	public void testGetFormato() {
		assertEquals(papel.getFormato(), "66x96");
	}

	@Test
	public void testFormatoProperty() {
		assertEquals(papel.formatoProperty().get(), "66x96");
	}

	@Test
	public void testGetFls() {
		assertEquals(papel.getFls(), 500);
	}

	@Test
	public void testFlsProperty() {
		assertEquals(papel.flsProperty().get(), 500);
	}

	@Test
	public void testGetPeso() {
		assertTrue(papel.getPeso().compareTo(new BigDecimal("15.840")) == 0);
	}

	@Test
	public void testPesoPropery() {
		assertTrue(papel.pesoPropery().get().compareTo(new BigDecimal("15.840")) == 0);
	}

	@Test
	public void testGetMarca() {
		assertEquals(papel.getMarca(), "Multiverde");
	}

	@Test
	public void testMarcaProperty() {
		assertEquals(papel.marcaProperty().get(), "Multiverde");
	}

	@Test
	public void testGetQtde() {
		assertEquals(papel.getQtde(), 42);
	}

	@Test
	public void testQtdeProperty() {
		assertEquals(papel.qtdeProperty().get(), 42);
	}

	@Test
	public void testToString() {
		assertEquals(papel.toString(), "Offset 50g 66x96 500fls 15.840kg");
	}

}
