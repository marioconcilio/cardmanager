package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Blanqueta;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class BlanquetaDAOTests {
	
	private static BlanquetaDAO dao;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dao = new BlanquetaDAO();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		dao = null;
	}

	@Test
	public final void testList() {
		final List<Blanqueta> list = dao.list();
		list.forEach(System.out::println);
		assertThat(list, notNullValue());
		assertThat(list, hasSize(greaterThan(0)));
	}
	
	@Test
	public final void testFetch() {
		List<Blanqueta> list = dao.fetch("hamada");
		assertThat(list, notNullValue());
		assertThat(list, hasSize(6));
		
		list = dao.fetch("650");
		assertThat(list, notNullValue());
		assertThat(list, hasSize(4));
		
		list = dao.fetch("260x471");
		assertThat(list, notNullValue());
		assertThat(list, hasSize(2));
		
		list = dao.fetch("asd");
		assertThat(list, notNullValue());
		assertThat(list, empty());
	}

}
