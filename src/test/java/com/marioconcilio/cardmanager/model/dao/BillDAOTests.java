package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Bill;
import com.marioconcilio.cardmanager.util.DateUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.exparity.hamcrest.date.DateMatchers.sameDay;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class BillDAOTests {
	
	private static BillDAO dao;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dao = new BillDAO();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		dao = null;
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public final void testFetchBillsByNumber() {
		// single bill
		List<Bill> list = dao.fetchBills(554);
		assertThat(list, notNullValue());
		assertThat(list, hasSize(1));
		assertThat(list, contains(
				hasProperty("date", sameDay(DateUtils.asDate(2017, 2, 15)))
				));
		
		// multiple bill
		list = dao.fetchBills(527);
		assertThat(list, notNullValue());
		assertThat(list, hasSize(2));
		assertThat(list, containsInAnyOrder(
				hasProperty("date", sameDay(DateUtils.asDate(2017, 01, 26))),
				hasProperty("date", sameDay(DateUtils.asDate(2017, 01, 11)))
				));
		
		// bill does not exist
		list = dao.fetchBills(1);
		assertThat(list, notNullValue());
		assertThat(list, is(empty()));
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public final void testFetchBillsByName() {
		List<Bill> list = dao.fetchBills("roberto");
		assertThat(list, notNullValue());
		assertThat(list, hasSize(6));
		assertThat(list, containsInAnyOrder(
				hasProperty("id", is(100001L)),
				hasProperty("id", is(100002L)),
				hasProperty("id", is(157002L)),
				hasProperty("id", is(157001L)),
				hasProperty("id", is(100003L)),
				hasProperty("id", is(157003L))
				));
	}

}
