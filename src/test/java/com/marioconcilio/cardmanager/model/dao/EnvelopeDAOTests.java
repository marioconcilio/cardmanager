package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Envelope;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class EnvelopeDAOTests {
	
	private static EnvelopeDAO dao;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dao = new EnvelopeDAO();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		dao = null;
	}

	@Test
	public final void testList() {
		final List<Envelope> list = dao.list();
		list.forEach(System.out::println);
		assertThat(list, notNullValue());
		assertThat(list, hasSize(greaterThan(0)));
	}
	
	@Test
	public final void testFetch() {
		List<Envelope> list = dao.fetch("185x248");
		assertThat(list, notNullValue());
		assertThat(list, hasSize(4));
		
		list = dao.fetch("125");
		assertThat(list, notNullValue());
		assertThat(list, hasSize(4));
		
		list = dao.fetch("comercial");
		assertThat(list, notNullValue());
		assertThat(list, hasSize(2));
		
		list = dao.fetch("asd");
		assertThat(list, notNullValue());
		assertThat(list, empty());
	}

}
