package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Dolar;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

public class DolarDAOTests {
	
	private static DolarDAO dao;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dao = new DolarDAO();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		dao = null;
	}

	@Test
	public final void testGet() {
		final Dolar dolar = dao.get();
		assertThat(dolar, notNullValue());
		assertThat(dolar.getId(), is(0L));
	}

}
