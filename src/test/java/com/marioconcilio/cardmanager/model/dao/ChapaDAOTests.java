package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Chapa;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class ChapaDAOTests {
	
	private static ChapaDAO dao;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dao = new ChapaDAO();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		dao = null;
	}

	@Test
	public final void testList() {
		final List<Chapa> list = dao.list();
		list.forEach(System.out::println);
		assertThat(list, notNullValue());
		assertThat(list, not(empty()));
	}
	
	@Test
	public final void testFetch() {
		List<Chapa> list = dao.fetch("370");
		assertThat(list, notNullValue());
		assertThat(list, hasSize(4));
		
		list = dao.fetch("250x370");
		assertThat(list, notNullValue());
		assertThat(list, hasSize(2));
		
		list = dao.fetch("250X370");
		assertThat(list, notNullValue());
		assertThat(list, hasSize(2));
		
		list = dao.fetch("250x 370xsdf");
		assertThat(list, notNullValue());
		assertThat(list, hasSize(2));
		
		list = dao.fetch("asd");
		assertThat(list, notNullValue());
		assertThat(list, empty());
	}

}
