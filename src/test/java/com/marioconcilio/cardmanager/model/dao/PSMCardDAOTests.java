package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.PSMCard;
import com.marioconcilio.cardmanager.model.vo.Card.PSMCardType;
import com.marioconcilio.cardmanager.util.DateUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class PSMCardDAOTests {
	
	private static PSMCardDAO dao;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dao = new PSMCardDAO();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		dao = null;
	}

	@Test
	public final void testListAllConsumerOfObservableListOfPSMCard() {
//		fail("Not yet implemented");
	}

	@Test
	public final void testList() {
//		fail("Not yet implemented");
	}
	
	@Test
	public final void testListCardsFrom() {
		
	}

	@Test
	public final void testFetchCardWithCard() {
		// card exists
		final PSMCard card = new PSMCard();
		card.setCardType(PSMCardType.DEBITO);
		card.setDate(DateUtils.asDate(2016, 12, 15));
		card.setValue(new BigDecimal("154.00"));
		
		PSMCard fetch = dao.fetchCard(card);
		assertNotNull(fetch);
		assertEquals(card.getCardType(), fetch.getCardType());
		assertEquals(card.getDate(), fetch.getDate());
		assertEquals(card.getValue(), fetch.getValue());
		
		// card does not exist
		card.setValue(new BigDecimal("69.00"));
		fetch = dao.fetchCard(card);
		assertNull(fetch);
		
		// card exists and is ok
		card.setDate(DateUtils.asDate(2016, 12, 14));
		card.setValue(new BigDecimal("68.00"));
		fetch = dao.fetchCard(card);
		assertNull(fetch);
	}
	
	@Test
	public final void testFetchCardWithAttrib() {
		final PSMCardType type = PSMCardType.DEBITO;
		Date date = DateUtils.asDate(2016, 12, 15);
		BigDecimal value = new BigDecimal("154.00");
		
		PSMCard fetch = dao.fetchCard(date, type, value);
		assertNotNull(fetch);
		assertEquals(type, fetch.getCardType());
		assertEquals(date, fetch.getDate());
		assertEquals(value, fetch.getValue());
		
		// card does not exist
		value = new BigDecimal("69.00");
		fetch = dao.fetchCard(date, type, value);
		assertNull(fetch);
		
		// card exists and is ok
		date = DateUtils.asDate(2016, 12, 14);
		value = new BigDecimal("68.00");
		fetch = dao.fetchCard(date, type, value);
		assertNull(fetch);
	}
	
	@Test
	public final void testFetchCardsByValue() {
		List<PSMCard> list = dao.fetchCards(new BigDecimal("200.00"));
		assertThat(list, notNullValue());
		assertThat(list, hasSize(2));
		
		list = dao.fetchCards(new BigDecimal("0.00"));
		assertThat(list, notNullValue());
		assertThat(list, empty());
	}
	
	@Test
	public final void testFetchCardsByDate() {
		List<PSMCard> list = dao.fetchCards(DateUtils.asDate(2017, 1, 16));
		assertThat(list, notNullValue());
		assertThat(list, hasSize(3));
		
		list = dao.fetchCards(DateUtils.asDate(2000, 1, 1));
		assertThat(list, notNullValue());
		assertThat(list, empty());
	}

}
