package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Tinta;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class TintaDAOTests {
	
	private static TintaDAO dao;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dao = new TintaDAO();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		dao = null;
	}

	@Test
	public final void testList() {
		final List<Tinta> list = dao.list();
		list.forEach(System.out::println);
		assertThat(list, notNullValue());
		assertThat(list, hasSize(greaterThan(0)));
	}
	
	@Test
	public final void testFetch() {
		List<Tinta> list = dao.fetch("vermelho");
		assertThat(list, notNullValue());
		assertThat(list, hasSize(5));
		
		list = dao.fetch("asd");
		assertThat(list, notNullValue());
		assertThat(list, empty());
	}

}
