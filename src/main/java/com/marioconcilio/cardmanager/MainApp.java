package com.marioconcilio.cardmanager;

import com.marioconcilio.cardmanager.cli.ConstantsCli;
import com.marioconcilio.cardmanager.cli.Main;
import com.marioconcilio.cardmanager.model.dao.UserDAO;
import com.marioconcilio.cardmanager.model.vo.User;
import com.marioconcilio.cardmanager.util.Constants;
import com.marioconcilio.cardmanager.util.Formatter;
import com.marioconcilio.cardmanager.view.dialog.DialogComponents;
import com.marioconcilio.cardmanager.view.dialog.DialogInterface;
import com.marioconcilio.cardmanager.view.LoginDialog;
import com.marioconcilio.cardmanager.view.root.RootViewController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.function.BiConsumer;

public class MainApp extends Application {
	
	private static final Logger logger = LogManager.getLogger();
	
	private static Stage primaryStage;
	private BorderPane rootLayout;
	private User user;
	
	private static BiConsumer<Integer, String> changeConsumer;

	@Override
	public void start(Stage stage) {
		setUserAgentStylesheet(STYLESHEET_MODENA);
		
		if (changeConsumer != null)
			changeConsumer.accept(15, "Iniciando JavaFX...");
		
		primaryStage = stage;
		primaryStage.setTitle(String.join(" ", Constants.APP_NAME, Constants.APP_VERSION));
		primaryStage.setOnCloseRequest(event -> {
			logger.info("{} logged out", Formatter.upperCaseFirst(user.getLogin()));
			Platform.exit();
			System.exit(0);
		});
	
		/*
		 * Login
		 */
		LoginDialog login = new LoginDialog();
		login.setOnSuccess(user -> {
			this.user = user;
			initRootLayout();
			logger.info(ConstantsCli.LINE);
			logger.info("{} logged in", Formatter.upperCaseFirst(user.getLogin()));
		});
		
		if (changeConsumer != null)
			changeConsumer.accept(70, "Conectando ao Banco de Dados...");

		new UserDAO().listAll(list -> {
			if (changeConsumer != null)
				changeConsumer.accept(99, "Terminando...");

			login.show();
			if (changeConsumer != null)
				changeConsumer.accept(100, "Pronto");
		});
	}
	
	private void initRootLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("view/root/RootView.fxml"));
			rootLayout = loader.load();

			Scene scene = new Scene(rootLayout, 1024, 768);
			scene.getStylesheets().add(getClass().getClassLoader().getResource("bootstrap3.css").toString());
			scene.getStylesheets().add(getClass().getClassLoader().getResource("gui.css").toString());
			primaryStage.setResizable(true);
			primaryStage.setScene(scene);
			primaryStage.show();
			
			RootViewController controller = loader.getController();
			controller.setMainApp(this);
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private DialogComponents createDialog(String fxmlPath) {	
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource(fxmlPath));
			Pane pane = loader.load();
			
			Stage dialogStage = new Stage();
		    dialogStage.setTitle(Constants.APP_NAME);
		    dialogStage.initModality(Modality.WINDOW_MODAL);
		    dialogStage.initOwner(primaryStage);
		    dialogStage.initStyle(StageStyle.UTILITY);
		    dialogStage.setResizable(false);
		    
		    Scene scene = new Scene(pane);
			scene.getStylesheets().add(getClass().getClassLoader().getResource("bootstrap3.css").toString());
			scene.getStylesheets().add(getClass().getClassLoader().getResource("gui.css").toString());
		    dialogStage.setScene(scene);
		    
		    DialogInterface controller = loader.getController();
		    controller.setStage(dialogStage);
		    
		    return new DialogComponents(dialogStage, controller);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static DialogComponents openDialog(String path) throws IOException {
		final FXMLLoader loader = new FXMLLoader();
		loader.setLocation(MainApp.class.getResource(path));
		final Pane pane = loader.load();

		final Stage dialogStage = new Stage();
		dialogStage.setTitle(Constants.APP_NAME);
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initOwner(primaryStage);
		dialogStage.initStyle(StageStyle.UTILITY);
		dialogStage.setResizable(false);

		final Scene scene = new Scene(pane);
		scene.getStylesheets().add(MainApp.class.getClassLoader().getResource("bootstrap3.css").toString());
		scene.getStylesheets().add(MainApp.class.getClassLoader().getResource("gui.css").toString());
		dialogStage.setScene(scene);

		final DialogInterface controller = loader.getController();
		controller.setStage(dialogStage);

		return new DialogComponents(dialogStage, controller);
	}
	
	public void showSettings() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/settings/SettingsView.fxml"));
			GridPane pane = loader.load();
			
			rootLayout.setCenter(pane);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void showAboutDialog() {
		DialogComponents components = this.createDialog("view/about/AboutView.fxml");
		components.getStage().showAndWait();
	}
	
	/*
	 * Getters
	 */
	
	public Stage getPrimaryStage() {
		return primaryStage;
	}
	
	public BorderPane getRootLayout() {
		return this.rootLayout;
	}
	
	public User getUser() {
		return this.user;
	}
	
	public static void setOnChange(BiConsumer<Integer, String> c) {
		changeConsumer = c;
	}

	public static void main(String[] args) {
		launch(args);
	}
}
