package com.marioconcilio.cardmanager.view.settings;

import com.marioconcilio.cardmanager.model.dao.SettingsDAO;
import com.marioconcilio.cardmanager.model.vo.Settings;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;

import java.text.DecimalFormat;
import java.text.ParsePosition;

public class SettingsViewController {
	
	@FXML
	private TextField debitTextField;
	
	@FXML
	private TextField onDemandTextField;
	
	@FXML
	private TextField onTermTextField;
	
	@FXML
	private Button saveButton;
	
	private Settings settings;
	
	@FXML
	private void initialize() {
		this.saveButton.setOnAction(event -> this.save());
		
		new SettingsDAO().listAll(list -> {
			this.settings = list.get(0);
			this.debitTextField.setText(settings.getRateDebit());
			this.onDemandTextField.setText(settings.getRateCreditOnDemand());
			this.onTermTextField.setText(settings.getRateCreditOnTerm());
		});
		
		DecimalFormat format = new DecimalFormat("#.0");
		// TODO: fix backspace not working on last number to delete
		this.debitTextField.setTextFormatter(new TextFormatter<>(c -> {
			ParsePosition parsePosition = new ParsePosition(0);
			Object object = format.parse(c.getControlNewText(), parsePosition);

			if (object == null || parsePosition.getIndex() < c.getControlNewText().length())
				return null;
			else
				return c;
		}));
		
		this.onDemandTextField.setTextFormatter(new TextFormatter<>(c -> {
			ParsePosition parsePosition = new ParsePosition(0);
			Object object = format.parse(c.getControlNewText(), parsePosition);

			if (object == null || parsePosition.getIndex() < c.getControlNewText().length())
				return null;
			else
				return c;
		}));
		
		this.onTermTextField.setTextFormatter(new TextFormatter<>(c -> {
			ParsePosition parsePosition = new ParsePosition(0);
			Object object = format.parse(c.getControlNewText(), parsePosition);

			if (object == null || parsePosition.getIndex() < c.getControlNewText().length())
				return null;
			else
				return c;
		}));
	}

	private void save() {
		String debit = this.debitTextField.getText();
		String demand = this.onDemandTextField.getText();
		String term = this.onTermTextField.getText();
		
		this.settings.setRateDebit(debit);
		this.settings.setRateCreditOnDemand(demand);
		this.settings.setRateCreditOnTerm(term);
		
		new SettingsDAO().update(this.settings);
	}
}
