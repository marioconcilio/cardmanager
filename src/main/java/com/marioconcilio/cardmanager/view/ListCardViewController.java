package com.marioconcilio.cardmanager.view;

import com.marioconcilio.cardmanager.model.vo.Card;
import com.marioconcilio.cardmanager.model.vo.Card.CardType;
import com.marioconcilio.cardmanager.util.Formatter;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.math.BigDecimal;
import java.util.Date;

public abstract class ListCardViewController<T extends Card> {

    @FXML
    protected TableView<T> tableView;

    @FXML
    protected TableColumn<T, Date> dateColumn;

    @FXML
    protected TableColumn<T, CardType> typeColumn;

    @FXML
    protected TableColumn<T, BigDecimal> valueColumn;

    @FXML
    protected ProgressIndicator progressIndicator;

    @FXML
    protected void initialize() {
        this.tableView.setPlaceholder(new Label(""));
        this.dateColumn.setCellValueFactory(cardData -> cardData.getValue().dateProperty());
        this.typeColumn.setCellValueFactory(cardData -> cardData.getValue().cardTypeProperty());
        this.valueColumn.setCellValueFactory(cardData -> cardData.getValue().valueProperty());

        this.dateColumn.getStyleClass().add("row-align-center");
        this.typeColumn.getStyleClass().add("row-align-left");
        this.valueColumn.getStyleClass().add("row-align-right");

        // double click on table
        this.tableView.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2)
                this.setCardOk();
        });

        // format date
        this.dateColumn.setCellFactory(column -> {
            return new TableCell<T, Date>() {
                @Override
                protected void updateItem(Date item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty)
                        setText(null);
                    else
                        setText(Formatter.formatDate(item));
                }
            };
        });

        // format value
        this.valueColumn.setCellFactory(column -> {
            return new TableCell<T, BigDecimal>() {
                @Override
                protected void updateItem(BigDecimal item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty)
                        setText(null);
                    else
                        setText(Formatter.formatCurrency(item));
                }
            };
        });

        // green row when card is ok
        final PseudoClass greenRow = PseudoClass.getPseudoClass("green-row");
        this.tableView.setRowFactory(p -> new TableRow<T>() {
            @Override
            protected void updateItem(T card, boolean empty) {
                super.updateItem(card, empty);
                if (card == null || empty) return;

                pseudoClassStateChanged(greenRow, card.isOk());
            }
        });
    }

    public abstract void populateTable();
    public abstract void processExtrato();
    public abstract void setCardOk();
    public abstract void deleteCard();

}
