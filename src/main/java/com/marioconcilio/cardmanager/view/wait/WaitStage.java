package com.marioconcilio.cardmanager.view.wait;

import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class WaitStage extends Stage {
	
	private final VBox vbox;
	private ProgressIndicator progressIndicator;
	private Window owner;
	private Parent[] parents;
	
	public WaitStage(Window owner) {
		this.owner = owner;
		
		initModality(Modality.APPLICATION_MODAL);
	    initOwner(owner);
	    initStyle(StageStyle.TRANSPARENT);
	    setResizable(false);
	    
	    progressIndicator = new ProgressIndicator();
	    progressIndicator.setPrefSize(75.0, 75.0);
	    
	    vbox = new VBox(20);
	    vbox.setAlignment(Pos.CENTER);
	    vbox.getChildren().add(progressIndicator);
	    
	    final Rectangle rect = new Rectangle(200, 200);
	    rect.setArcHeight(30);
	    rect.setArcWidth(30);
	    
	    vbox.setClip(rect);
//	    vbox.setStyle(
//	            "-fx-background-color: rgba(255, 255, 255, 0.5);" +
//	            "-fx-effect: dropshadow(gaussian, black, 50, 0, 0, 0);"
//	            "-fx-background-insets: 50;"
//	        );
	    
	    final Scene scene = new Scene(vbox, 200, 200);
	    scene.setFill(Color.TRANSPARENT);
	    setScene(scene);
	    
	    if (owner != null) {
	    	Parent parent = owner.getScene().getRoot();
		    parent.setEffect(new GaussianBlur());
		    parent.setDisable(true);
	    }
	    
	}
	
	public WaitStage(Window owner, String message) {
		this(owner);
	    
	    final Label label = new Label(message);
	    label.setFont(new Font(14));

	    vbox.getChildren().add(label);
	}
	
	public WaitStage(Window owner, String message, Parent... parents) {
		this(owner, message);
		this.parents = parents;
		
		for (Parent p : parents) {
			p.setEffect(new GaussianBlur());
			p.setDisable(true);
		}
	}
	
	@Override
	public void close() {
		if (owner != null) {
			Parent parent = owner.getScene().getRoot();
		    parent.setEffect(null);
		    parent.setDisable(false);
		}
		
		if (parents != null) {
			for (Parent p : parents) {
				p.setEffect(null);
				p.setDisable(false);
			}
		}
	    
	    super.close();
	}
	
	public ProgressIndicator getProgressIndicator() {
		return this.progressIndicator;
	}

}
