package com.marioconcilio.cardmanager.view.list.product;

import com.marioconcilio.cardmanager.model.dao.*;
import com.marioconcilio.cardmanager.model.vo.Product;
import com.marioconcilio.cardmanager.util.Formatter;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;

public class ListProductViewController {
	
	@FXML
    private TextField filterField;
	
	@FXML
	private TableView<Product> tableView;
	
	@FXML
	private TableColumn<Product, String> descColumn;
	
	@FXML
	private TableColumn<Product, String> detailColumn;
	
	@FXML
	private TableColumn<Product, String> unColumn;
	
	@FXML
	private TableColumn<Product, String> precoColumn;
	
	@FXML
	private TableColumn<Product, Number> qtdeColumn;
	
	@FXML
	private BorderPane pane;
	
	@FXML
	private ProgressIndicator progressIndicator;
	
//	private RootViewController rootViewController;
	
	@FXML
	public void initialize() {
		this.tableView.setPlaceholder(new Label(""));
		
		/*
		 * value factory
		 */
		this.descColumn.setCellValueFactory(p -> p.getValue().descriptionProperty());
		this.detailColumn.setCellValueFactory(p -> p.getValue().detailProperty());
		this.unColumn.setCellValueFactory(p -> p.getValue().unProperty());
		this.qtdeColumn.setCellValueFactory(p -> p.getValue().qtdeProperty());
		this.precoColumn.setCellValueFactory(p -> {
			String str = Formatter.formatCurrency(p.getValue().precoUn());
			return new ReadOnlyObjectWrapper<String>(str);
		});
		
		/*
		 * color
		 *
		final PseudoClass redRow = PseudoClass.getPseudoClass("red_row");
		this.tableView.setRowFactory(p -> new TableRow<Product>() {
			@Override
			protected void updateItem(Product product, boolean empty) {
				super.updateItem(product, empty);
				if (product == null || empty) return;
				
				pseudoClassStateChanged(redRow, (product.getQtde() <= 0));
			}
		});
		*/
	}
	
	private void listProduct(AbstractDAO<Product> dao) {
		dao.setProgressIndicator(progressIndicator);
		dao.listAll(list -> {
			// 1. Wrap the ObservableList in a FilteredList (initially display all data).
	        FilteredList<Product> filteredData = new FilteredList<>(list, c -> true);

	        // 2. Set the filter Predicate whenever the filter changes.
	        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
	            filteredData.setPredicate(p -> {
	                // If filter text is empty, display all.
	                if (newValue == null || newValue.isEmpty()) {
	                    return true;
	                }

	                String lowerCaseFilter = newValue.toLowerCase();
	                return p.descriptionProperty().get().toLowerCase().contains(lowerCaseFilter) ||
	                		p.detailProperty().get().toLowerCase().contains(lowerCaseFilter);
	            });
	        });

	        // 3. Wrap the FilteredList in a SortedList. 
	        SortedList<Product> sortedData = new SortedList<>(filteredData);
	     
	        // 4. Bind the SortedList comparator to the TableView comparator.
	        sortedData.comparatorProperty().bind(tableView.comparatorProperty());
			
	        tableView.setItems(sortedData);
			progressIndicator.setVisible(false);
		});
	}
	
	@SuppressWarnings("unchecked")
	public void listBlanqueta() {
		AbstractDAO<?> dao = new BlanquetaDAO();
		listProduct((AbstractDAO<Product>) dao);
	}
	
	@SuppressWarnings("unchecked")
	public void listChapa() {
		AbstractDAO<?> dao = new ChapaDAO();
		listProduct((AbstractDAO<Product>) dao);
	}
	
	@SuppressWarnings("unchecked")
	public void listDiversos() {
		AbstractDAO<?> dao = new DiversosDAO();
		listProduct((AbstractDAO<Product>) dao);
	}
	
	@SuppressWarnings("unchecked")
	public void listEnvelope() {
		AbstractDAO<?> dao = new EnvelopeDAO();
		listProduct((AbstractDAO<Product>) dao);
	}
	
	@SuppressWarnings("unchecked")
	public void listQuimico() {
		AbstractDAO<?> dao = new QuimicoDAO();
		listProduct((AbstractDAO<Product>) dao);
	}
	
	@SuppressWarnings("unchecked")
	public void listTinta() {
		AbstractDAO<?> dao = new TintaDAO();
		listProduct((AbstractDAO<Product>) dao);
	}

}
