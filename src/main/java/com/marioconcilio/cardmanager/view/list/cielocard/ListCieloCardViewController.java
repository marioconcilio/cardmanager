package com.marioconcilio.cardmanager.view.list.cielocard;

import com.marioconcilio.cardmanager.controller.CieloController;
import com.marioconcilio.cardmanager.model.dao.CieloCardDAO;
import com.marioconcilio.cardmanager.model.vo.CieloCard;
import com.marioconcilio.cardmanager.view.AlertFactory;
import com.marioconcilio.cardmanager.view.FileChooserFactory;
import com.marioconcilio.cardmanager.view.ListCardViewController;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ListCieloCardViewController extends ListCardViewController<CieloCard> {
	
	@FXML
	@Override
	public void initialize() {
		super.initialize();
		populateTable();
	}

	@Override
	public void populateTable() {
		final CieloCardDAO dao = new CieloCardDAO();
		dao.setProgressIndicator(progressIndicator);
		dao.listAll(list -> {
			tableView.setItems(list);
			progressIndicator.setVisible(false);
		});
	}

	@Override
	public void processExtrato() {
        final File file = FileChooserFactory.openCieloExtrato();
        if (file == null) {
            return;
        }

        final CieloController controller = new CieloController();
        try {
            List<CieloCard> list = controller.processExtrato(file);
            list.forEach(System.out::println);

            CieloCardDAO dao = new CieloCardDAO();
            int total = dao.saveAll(list);

            AlertFactory.showInfo("Importado com sucesso", total + " entradas importadas.");
        }
        catch (IOException e) {
            AlertFactory.showError(e, "Erro ao importar extrato.");
        }

    }

	@Override
	public void setCardOk() {
		final CieloCard card = this.tableView.getSelectionModel().getSelectedItem();
		if (card.isOk())
			return;

		final ButtonType button = AlertFactory.showConfirmation("Deseja marcar como recebido?", card.toString());
		if (button == ButtonType.OK) {
			card.setOk(true);

			final CieloCardDAO dao = new CieloCardDAO();
			try {
				dao.update(card);
			}
			catch (Exception e) {
				AlertFactory.showError(e, "Não foi possível atualizar cartão.");
			}

			populateTable();
		}
	}

	@Override
	public void deleteCard() {
		final CieloCard card = this.tableView.getSelectionModel().getSelectedItem();
        final ButtonType button = AlertFactory.showConfirmation("Remover entrada?", card.toString());
        if (button == ButtonType.OK) {
            populateTable();
        }
	}
}
