package com.marioconcilio.cardmanager.view.list.client;

import com.marioconcilio.cardmanager.MainApp;
import com.marioconcilio.cardmanager.model.dao.ClientDAO;
import com.marioconcilio.cardmanager.model.vo.Bill;
import com.marioconcilio.cardmanager.model.vo.Bill.BillStatus;
import com.marioconcilio.cardmanager.model.vo.Client;
import com.marioconcilio.cardmanager.util.Formatter;
import com.marioconcilio.cardmanager.view.client.ClientViewController;
import com.marioconcilio.cardmanager.view.dialog.DialogComponents;
import com.marioconcilio.cardmanager.view.orcamento.OrcamentoViewController;
import com.marioconcilio.cardmanager.view.root.RootViewController;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class ListClientViewController {
	
	@FXML
	private TableView<Client> tableView;
	
	@FXML
    private TextField filterField;
	
	@FXML
	private TableColumn<Client, Number> cnpjColumn;
	
	@FXML
	private TableColumn<Client, String> nameColumn;
	
	@FXML
	private TableColumn<Client, String> addressColumn;
	
	@FXML
	private TableColumn<Client, String> cityColumn;
	
	@FXML
	private TableColumn<Client, String> stateColumn;
	
	@FXML
	private ProgressIndicator progressIndicator;
	
	@FXML
	private BorderPane borderPane;
	
	private RootViewController rootViewController;
	
	@FXML
	public void initialize() {
		/*
		 * Setup
		 */
		this.tableView.setPlaceholder(new Label(""));
		this.cnpjColumn.setCellValueFactory(clientData -> clientData.getValue().idProperty());
		this.nameColumn.setCellValueFactory(clientData -> clientData.getValue().nameProperty());
		this.addressColumn.setCellValueFactory(clientData -> clientData.getValue().addressProperty());
		this.cityColumn.setCellValueFactory(clientData -> clientData.getValue().cityProperty());
		this.stateColumn.setCellValueFactory(clientData -> clientData.getValue().stateProperty());
		
		this.cnpjColumn.setCellFactory((TableColumn<Client, Number> column) -> new TableCell<Client, Number>() {
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty)
                    setText(null);
                else
                    setText(Formatter.formatCnpj(item.longValue()));
            }
        });
		
		final PseudoClass yellowRow = PseudoClass.getPseudoClass("yellow-row");
		final PseudoClass redRow = PseudoClass.getPseudoClass("red-row");
		this.tableView.setRowFactory(p -> new TableRow<Client>() {
			@Override
			protected void updateItem(Client client, boolean empty) {
				super.updateItem(client, empty);
				if (client == null || empty) return;
				
				List<Bill> bills = client.getBills();
				BigDecimal creditSum = bills.stream()
						.filter(bill -> bill.getStatus() != BillStatus.PAGO)
						.map(Bill::getValue)
						.reduce(BigDecimal.ZERO, BigDecimal::add);
				
				BigDecimal creditLimit = new BigDecimal(client.getCreditLimit());
				
				// 10% to limit
				pseudoClassStateChanged(yellowRow, creditSum.compareTo(creditLimit.multiply(new BigDecimal(0.9))) == 1);
				
				// above limit
				pseudoClassStateChanged(redRow, creditSum.compareTo(creditLimit) == 1);
			}
		});
		
//		listAllClients();
	}
	
	public void listAllClients() {
		this.tableView.setOnMouseClicked(event -> {
			if (event.getClickCount() == 2) {
//				rootViewController.openClient(client);
				try {
					final Client client = tableView.getSelectionModel().getSelectedItem();
					final DialogComponents dialog = MainApp.openDialog("view/client/ClientView.fxml");
					final ClientViewController dialogController = (ClientViewController) dialog.getController();
					dialogController.setClient(client);
					dialog.showAndWait();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		
		/*
		 * Fetch
		 */
		ClientDAO dao = new ClientDAO();
		dao.setProgressIndicator(progressIndicator);
		dao.listAll(list -> {
			// 1. Wrap the ObservableList in a FilteredList (initially display all data).
	        FilteredList<Client> filteredData = new FilteredList<>(list, c -> true);

	        // 2. Set the filter Predicate whenever the filter changes.
	        filterField.textProperty().addListener((observable, oldValue, newValue) -> filteredData.setPredicate(client -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name of every client with filter text.
                String lowerCaseFilter = newValue.toLowerCase();
                return client.getName().toLowerCase().contains(lowerCaseFilter);
            }));

	        // 3. Wrap the FilteredList in a SortedList. 
	        SortedList<Client> sortedData = new SortedList<>(filteredData);
	     
	        // 4. Bind the SortedList comparator to the TableView comparator.
	        sortedData.comparatorProperty().bind(tableView.comparatorProperty());
			
	        tableView.setItems(sortedData);
//			tableView.setItems(list);
			progressIndicator.setVisible(false);
		});
	}

	public void setClients(List<Client> clients, OrcamentoViewController controller) {	
		this.cnpjColumn.setVisible(false);
		
		Label title = new Label("Selecionar Cliente");
		title.setPadding(new Insets(10, 10, 0, 10));
		
		HBox bottom = new HBox(10);
		Button cancel = new Button("Cancelar");
		Button add = new Button("Selecionar");
		add.disableProperty().bind(Bindings.isNull(tableView.getSelectionModel().selectedItemProperty()));
		
		ImageView cancelImageView = new ImageView(new Image(getClass().getResourceAsStream("/resources/icons/cancel.png")));
		cancelImageView.setFitWidth(25.0);
		cancelImageView.setFitHeight(25.0);
		
		ImageView addImageView = new ImageView(new Image(getClass().getResourceAsStream("/resources/icons/ok.png")));
		addImageView.setFitWidth(25.0);
		addImageView.setFitHeight(25.0);
		
		cancel.setGraphic(cancelImageView);
		add.setGraphic(addImageView);
		
		bottom.setAlignment(Pos.CENTER_RIGHT);
		bottom.getChildren().addAll(cancel, add);
		bottom.setPadding(new Insets(0, 10, 10, 10));
		
//		HBox oldTop = (HBox) borderPane.getTop();
//		VBox top = new VBox(10);
//		top.setPadding(new Insets(10, 10, 0, 10));
//		top.getChildren().addAll(title, oldTop);
		
		borderPane.setTop(title);
		borderPane.setBottom(bottom);
		
		if (clients != null) {
			progressIndicator.setVisible(false);
			tableView.setItems(FXCollections.observableArrayList(clients));
		}
		else {
			ClientDAO dao = new ClientDAO();
			dao.setProgressIndicator(progressIndicator);
			dao.listAll(list -> {
				progressIndicator.setVisible(false);
				tableView.setItems(list);
			});
		}
		
		Stage stage = (Stage) tableView.getScene().getWindow();
		stage.setOnCloseRequest(event -> controller.clearClientInfo());
		
		cancel.setOnAction(ev -> stage.close());
		add.setOnAction(ev -> {
			Client client = tableView.getSelectionModel().getSelectedItem();
			controller.fillClientInfo(client);
			stage.close();
		});

		this.tableView.setOnMouseClicked(event -> {
			if (event.getClickCount() == 2) {
				Client client = tableView.getSelectionModel().getSelectedItem();
				controller.fillClientInfo(client);
				stage.close();
			}
		});
	}

	public void setRootViewController(RootViewController rootViewController) {
		this.rootViewController = rootViewController;
	}
}
