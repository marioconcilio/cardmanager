package com.marioconcilio.cardmanager.view.list.bill;

import com.marioconcilio.cardmanager.MainApp;
import com.marioconcilio.cardmanager.controller.BillController;
import com.marioconcilio.cardmanager.model.dao.BillDAO;
import com.marioconcilio.cardmanager.model.vo.Bill;
import com.marioconcilio.cardmanager.model.vo.Bill.BillStatus;
import com.marioconcilio.cardmanager.util.Formatter;
import com.marioconcilio.cardmanager.view.AbstractListBillViewController;
import com.marioconcilio.cardmanager.view.AlertFactory;
import com.marioconcilio.cardmanager.view.FileChooserFactory;
import com.marioconcilio.cardmanager.view.dialog.DialogComponents;
import com.marioconcilio.cardmanager.view.dialog.bill.DialogBillViewController;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.hibernate.HibernateException;
import org.hibernate.exception.ConstraintViolationException;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class ListBillViewController extends AbstractListBillViewController {
	
	@FXML
    private TextField filterField;
	
	@FXML
	private ProgressIndicator progressIndicator;
	
	@FXML
	private Label totalLabel;
	
	@FXML
	public void initialize() {
		super.initialize();
		/*
		BillRMIService service = new BillRMIService();
		try {
			service.checkRemessa();
			service.checkRetorno();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		*/
		
		/*
		 * total selected
		 */
		this.tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		this.tableView.getSelectionModel().selectedItemProperty().addListener((obs, oldValue, newValue) -> {
			BigDecimal totalSelected = tableView.getSelectionModel().getSelectedItems()
					.stream()
					.map(Bill::getValue)
					.reduce(BigDecimal.ZERO, BigDecimal::add);
			
			String text = String.format("Total Selecionado: %s", Formatter.formatCurrency(totalSelected));
			this.totalLabel.setText(text);
		});
		
		/*
		 * fetch
		 */
		final BillDAO dao = new BillDAO();
		dao.setProgressIndicator(progressIndicator);
		dao.listAll(list -> {
			// 1. Wrap the ObservableList in a FilteredList (initially display all data).
	        FilteredList<Bill> filteredData = new FilteredList<>(list, c -> true);

	        // 2. Set the filter Predicate whenever the filter changes.
	        filterField.textProperty().addListener((observable, oldValue, newValue) -> filteredData.setPredicate(bill -> {
                // If filter text is empty, display all.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();
                String number = Formatter.formatBillNumber(bill.getId());
                return bill.getClient().getName().toLowerCase().contains(lowerCaseFilter) ||
                        number.contains(lowerCaseFilter);
            }));

	        // 3. Wrap the FilteredList in a SortedList. 
	        SortedList<Bill> sortedData = new SortedList<>(filteredData);
	     
	        // 4. Bind the SortedList comparator to the TableView comparator.
	        sortedData.comparatorProperty().bind(tableView.comparatorProperty());
			
	        tableView.setItems(sortedData);
			progressIndicator.setVisible(false);
		});
	}

	public void updateBillStatus(BillStatus status) {
		final Bill bill = this.tableView.getSelectionModel().getSelectedItem();
		final String header = String.format("Deseja atualizar boleto para %s?", status);
		
		ButtonType button = AlertFactory.showConfirmation(header, bill.toString());
		if (button == ButtonType.OK){
			bill.setStatus(status);
			
			final BillDAO dao = new BillDAO();
			try {
				dao.update(bill);
			} 
			catch (Exception e) {
				AlertFactory.showError(e, "Não foi possível atualizar boleto.");
			}
		}
		
	}
	
	public void processRemessa() {
		List<File> list = FileChooserFactory.openRemessa();
		if (list == null || list.isEmpty()) {
			return;
		}
		
		BillController controller = new BillController();
		try {
			List<Bill> remessa = controller.processListRemessa(list);
			remessa.forEach(System.out::println);
			
			BillDAO dao = new BillDAO();
			int total = dao.saveAll(remessa);
			String msg = (total > 1)? total + " boletos adicionados." : total + " boleto adicionado.";
			AlertFactory.showInfo("Remessa processada", msg);
		}
		catch (ConstraintViolationException e) {
			AlertFactory.showInfo("Ops...", "Remessa já processada.");
		}
		catch (HibernateException e) {
			AlertFactory.showError(e, "Erro ao adicionar boletos.");
		}
		catch (IOException e) {
			AlertFactory.showError(e, "Arquivo não encontrado.");
		}
	}

	public void processRetorno() {
		List<File> list = FileChooserFactory.openRetorno();
		if (list == null || list.isEmpty()) {
			return;
		}

		final BillController controller = new BillController();
		try {
			final List<Bill> retorno = controller.processListRetorno(list);
			final BillDAO dao = new BillDAO();
			dao.updateAll(retorno);

			final DialogComponents dialog = MainApp.openDialog("view/dialog/bill/DialogBillView.fxml");
			final DialogBillViewController dialogController = (DialogBillViewController) dialog.getController();
			dialogController.setBillList(retorno);
			dialog.showAndWait();
		}
		catch (HibernateException ex) {
			AlertFactory.showError(ex, "Não foi possível atualizar boletos.");
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

}
