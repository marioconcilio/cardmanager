package com.marioconcilio.cardmanager.view.list.psmcard;

import com.marioconcilio.cardmanager.controller.PSMController;
import com.marioconcilio.cardmanager.model.dao.PSMCardDAO;
import com.marioconcilio.cardmanager.model.vo.PSMCard;
import com.marioconcilio.cardmanager.view.AlertFactory;
import com.marioconcilio.cardmanager.view.FileChooserFactory;
import com.marioconcilio.cardmanager.view.ListCardViewController;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;

import java.io.File;
import java.util.List;

public class ListPSMCardViewController extends ListCardViewController<PSMCard> {
	
	@FXML
	private TableColumn<PSMCard, Number> sharesColumn;

	@FXML
	@Override
	public void initialize() {
		super.initialize();
		this.sharesColumn.setCellValueFactory(cardData -> cardData.getValue().sharesProperty());
		populateTable();
	}

	@Override
	public void populateTable() {
		final PSMCardDAO dao = new PSMCardDAO();
		dao.setProgressIndicator(progressIndicator);
		dao.listAll(list -> {
			tableView.setItems(list);
			progressIndicator.setVisible(false);
		});
	}

	@Override
	public void processExtrato() {
		File file = FileChooserFactory.openPSMExtrato();
		if (file == null) {
			return;
		}
		
		PSMController controller = new PSMController();
		try {
			List<PSMCard> list = controller.processExtrato(file);
			list.forEach(System.out::println);
			
			PSMCardDAO dao = new PSMCardDAO();
			int total = dao.saveAll(list);
//			this.cardViewController.updateTable();
			
			AlertFactory.showInfo("Importado com sucesso", total + " entradas importadas.");
		} 
		catch (Exception e) {
			AlertFactory.showError(e, "Erro ao importar extrato.");	
		}
	}

	@Override
	public void setCardOk() {
		final PSMCard card = this.tableView.getSelectionModel().getSelectedItem();
		if (card.isOk())
			return;
		
		final ButtonType button = AlertFactory.showConfirmation("Deseja marcar como recebido?", card.toString());
		if (button == ButtonType.OK) {
		    card.setOk(true);
		    
		    final PSMCardDAO dao = new PSMCardDAO();
		    try {
				dao.update(card);
			}
		    catch (Exception e) {
		    	AlertFactory.showError(e, "Não foi possível atualizar cartão.");
			}
		    
		    populateTable();
		}
	}

	@Override
	public void deleteCard() {
		final PSMCard card = this.tableView.getSelectionModel().getSelectedItem();
		final ButtonType button = AlertFactory.showConfirmation("Remover entrada?", card.toString());
		if (button == ButtonType.OK) {
			populateTable();
		}
	}
}
