package com.marioconcilio.cardmanager.view;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import org.apache.commons.io.comparator.LastModifiedFileComparator;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

public class FileChooserFactory {
	
	private FileChooserFactory() {
		//
	}
	
	public static File openPSMExtrato() {
		return openFileChooser("Importar Extrato Moderninha", new ExtensionFilter("Arquivos XML", "*.XML", "*.xml"));
	}

	public static File openCieloExtrato() {
		return openFileChooser("Importar Extrato Cielo", new ExtensionFilter("Arquivos TXT", "*.TXT", "*.txt"));
	}
	
	public static List<File> openRemessa() {
		return openMultipleFileChooser("Processar Remessa", null);
	}
	
	public static List<File> openRetorno() {
		return openMultipleFileChooser("Processar Retorno", new ExtensionFilter("Arquivos RET", "*.RET", "*.ret"));
	}
	
	private static File openFileChooser(String title, ExtensionFilter filter) {
		final String userDir = System.getProperty("user.home") + "/Desktop";
		final FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle(title);
		fileChooser.setInitialDirectory(new File(userDir));
		
		if (filter != null) {
			fileChooser.getExtensionFilters().addAll(filter);
		}
		
		final File selectedFile = fileChooser.showOpenDialog(null);
		return selectedFile;
	}
	
	private static List<File> openMultipleFileChooser(String title, ExtensionFilter filter) {
		final String userDir = System.getProperty("user.home") + "/Desktop";
		final FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle(title);
		fileChooser.setInitialDirectory(new File(userDir));
		
		if (filter != null) {
			fileChooser.getExtensionFilters().addAll(filter);
		}
		
		final List<File> listSelected = fileChooser.showOpenMultipleDialog(null);
		return listSelected.stream()
				.sorted(LastModifiedFileComparator.LASTMODIFIED_COMPARATOR)
				.collect(Collectors.toList());
	}

}
