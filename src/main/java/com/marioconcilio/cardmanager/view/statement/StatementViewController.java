package com.marioconcilio.cardmanager.view.statement;

import com.marioconcilio.cardmanager.model.dao.CieloCardDAO;
import com.marioconcilio.cardmanager.model.vo.Card;
import com.marioconcilio.cardmanager.model.vo.CieloCard;
import com.marioconcilio.cardmanager.view.AlertFactory;
import com.marioconcilio.cardmanager.view.ListCardViewController;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.control.*;

public abstract class StatementViewController extends ListCardViewController {
	
	@FXML
	private TableColumn<CieloCard, String> descColumn;
	
	@FXML
	private ProgressIndicator progressIndicator;

	@Override
	@FXML
	protected void initialize() {
		super.initialize();
		
		CieloCardDAO dao = new CieloCardDAO();
		dao.setProgressIndicator(progressIndicator);
		dao.listAll(list -> {
			tableView.getItems().setAll(list);
			progressIndicator.setVisible(false);
//			cardTable.setVisible(true);
		});
		
//		this.cardTable.setVisible(false);
		this.tableView.setPlaceholder(new Label(""));
		this.descColumn.setCellValueFactory(cardData -> cardData.getValue().descriptionProperty());
		this.tableView.setOnMouseClicked(event -> {
			if (event.getClickCount() == 2)
				this.checkSelectedCard();
		});
		
		final PseudoClass greenRow = PseudoClass.getPseudoClass("green_row");
		this.tableView.setRowFactory(p -> new TableRow<Card>() {
			protected void updateItem(Card card, boolean empty) {
				super.updateItem(card, empty);
				if (card == null || empty) return;
				
				pseudoClassStateChanged(greenRow, card.isOk());
			}
		});
		
	}
	
	public void updateTable() {
		new CieloCardDAO().listAll(list -> {
//			this.tableView.setItems(list);
		});
	}
	
	public void checkSelectedCard() {
		final CieloCard card = (CieloCard) this.tableView.getSelectionModel().getSelectedItem();
		if (card.isOk())
			return;
		
		final ButtonType button = AlertFactory.showConfirmation("Deseja marcar como recebido?", card.toString());
		if (button == ButtonType.OK){
		    card.setOk(true);
		    
		    final CieloCardDAO dao = new CieloCardDAO();
		    try {
				dao.update(card);
			} 
		    catch (Exception e) {
		    	AlertFactory.showError(e, "Não foi possível atualizar cartão.");
			}
//		    updateTable();
		}
	}
}
