package com.marioconcilio.cardmanager.view.editpapel;

import com.marioconcilio.cardmanager.handler.LimitCharactersHandler;
import com.marioconcilio.cardmanager.handler.LimitCharactersHandler.LimitCharactersType;
import com.marioconcilio.cardmanager.model.dao.PapelDAO;
import com.marioconcilio.cardmanager.model.dao.PapelTipoDAO;
import com.marioconcilio.cardmanager.model.vo.Papel;
import com.marioconcilio.cardmanager.model.vo.PapelTipo;
import com.marioconcilio.cardmanager.model.vo.User;
import javafx.beans.binding.NumberBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.math.BigDecimal;
import java.util.Set;

public class EditPapelViewController {
	
	@FXML
	private ComboBox<PapelTipo> papelComboBox;
	
	@FXML
	private TextField grsTextField;
	
	@FXML
	private TextField larguraTextField;
	
	@FXML
	private TextField comprimentoTextField;
	
	@FXML
	private TextField flsTextField;
	
	@FXML
	private TextField pesoTextField;
	
	@FXML
	private TextField marcaTextField;
	
	@FXML
	private TextField qtdeTextField;
	
	@FXML
	private Label titleLabel;
	
	@FXML
	private GridPane formPane;
	
	private Papel papel;
	private User user;
	
	@FXML
	public void initialize() {
		/*
		 * setup
		 */
		this.larguraTextField.addEventFilter(KeyEvent.KEY_TYPED, new LimitCharactersHandler(10, LimitCharactersType.CURRENCY));
		this.comprimentoTextField.addEventFilter(KeyEvent.KEY_TYPED, new LimitCharactersHandler(10, LimitCharactersType.CURRENCY));
		this.flsTextField.addEventFilter(KeyEvent.KEY_TYPED, new LimitCharactersHandler(10, LimitCharactersType.CURRENCY));
		this.grsTextField.addEventFilter(KeyEvent.KEY_TYPED, new LimitCharactersHandler(10, LimitCharactersType.CURRENCY));
		this.qtdeTextField.addEventFilter(KeyEvent.KEY_TYPED, new LimitCharactersHandler(100, LimitCharactersType.CURRENCY));
		
		/*
		 * bindings for calculating peso
		 */
		DoubleProperty larg = new SimpleDoubleProperty();
		DoubleProperty comp = new SimpleDoubleProperty();
		DoubleProperty fls = new SimpleDoubleProperty();
		DoubleProperty grs = new SimpleDoubleProperty();
		
		this.larguraTextField.textProperty().addListener((obs, oldValue, newValue) -> {
			try {
				double val = Double.parseDouble(newValue.replaceAll(",", "."));
				larg.setValue(val);
			}
			catch (NumberFormatException ex) {}
		});
		
		this.comprimentoTextField.textProperty().addListener((obs, oldValue, newValue) -> {
			try {
				double val = Double.parseDouble(newValue.replaceAll(",", "."));
				comp.setValue(val);
			}
			catch (NumberFormatException ex) {}
		});
		
		this.flsTextField.textProperty().addListener((obs, oldValue, newValue) -> {
			try {
				double val = Double.parseDouble(newValue.replaceAll(",", "."));
				fls.setValue(val);
			}
			catch (NumberFormatException ex) {}
		});
		
		this.grsTextField.textProperty().addListener((obs, oldValue, newValue) -> {
			try {
				double val = Double.parseDouble(newValue.replaceAll(",", "."));
				grs.setValue(val);
			}
			catch (NumberFormatException ex) {}
		});
		
		NumberBinding peso = larg.multiply(comp)
				.multiply(fls)
				.multiply(grs)
				.divide(10000000);
		
		pesoTextField.textProperty().bind(peso.asString());
		
		Set<Node> nodes = formPane.lookupAll(".text-field");
		nodes.forEach(node -> {
			TextField tx = (TextField) node;
			tx.setOnKeyPressed(event -> {
				if (event.getCode() == KeyCode.ENTER)
					saveButtonAction(null);
			});
		});
		
		/*
		 * fetch
		 */
		PapelTipoDAO dao = new PapelTipoDAO();
		dao.listAll(list -> this.papelComboBox.getItems().addAll(list));
	}
	
	@FXML
	private void saveButtonAction(ActionEvent event) {
		if (!checkAll()) return;
		if (papel == null) {
			papel = new Papel();
		
			papel.setPapelTipo(this.papelComboBox.getSelectionModel().getSelectedItem());
			papel.setGrs(Integer.parseInt(this.grsTextField.getText()));
			papel.setLargura(Integer.parseInt(this.larguraTextField.getText()));
			papel.setComprimento(Integer.parseInt(this.comprimentoTextField.getText()));
			papel.setFls(Integer.parseInt(this.flsTextField.getText()));
			papel.setPeso(new BigDecimal(this.pesoTextField.getText()));
			papel.setMarca(this.marcaTextField.getText());
			papel.setQtde(Long.parseLong(this.qtdeTextField.getText()));
		
			PapelDAO dao = new PapelDAO();
			dao.save(papel);
		}
		else {
			papel.setPapelTipo(this.papelComboBox.getSelectionModel().getSelectedItem());
			papel.setGrs(Integer.parseInt(this.grsTextField.getText()));
			papel.setLargura(Integer.parseInt(this.larguraTextField.getText()));
			papel.setComprimento(Integer.parseInt(this.comprimentoTextField.getText()));
			papel.setFls(Integer.parseInt(this.flsTextField.getText()));
			papel.setPeso(new BigDecimal(this.pesoTextField.getText()));
			papel.setMarca(this.marcaTextField.getText());
			papel.setQtde(Long.parseLong(this.qtdeTextField.getText()));
			
			PapelDAO dao = new PapelDAO();
			dao.update(papel);
		}
		
		Stage stage = (Stage) qtdeTextField.getScene().getWindow();
		stage.close();
	}
	
	@FXML
	private void cancelButtonAction(ActionEvent event) {
		Button button = (Button) event.getSource();
		Stage stage = (Stage) button.getScene().getWindow();
		stage.close();
	}
	
	private boolean checkAll() {
		int i = 0;
		
		Set<Node> nodes = this.formPane.lookupAll(".text-field");
		for(Node node : nodes) {
			TextField tx = (TextField) node;
			
			// disregard marca text field, which can be empty
			if (tx.getId().equals("marcaTextField")) 
				continue;
			
			if (tx.getText().length() == 0) {
				tx.getStyleClass().add("validation_error");
				i++;
			}
			else {
				tx.getStyleClass().remove("validation_error");
			}
		}
		
		return i == 0;
	}
	
	public void setPapel(Papel papel) {
		this.papel = papel;
		if (papel == null) {
			return;
		}
		
		this.titleLabel.setText("Editando Papel");
		
		this.papelComboBox.getSelectionModel().select(papel.getPapelTipo());
		this.grsTextField.setText(Integer.toString(papel.getGrs()));
		this.larguraTextField.setText(Integer.toString(papel.getLargura()));
		this.comprimentoTextField.setText(Integer.toString(papel.getComprimento()));
		this.flsTextField.setText(Integer.toString(papel.getFls()));
//		this.pesoTextField.setText(papel.getPeso().toString());
		this.marcaTextField.setText(papel.getMarca());
		this.qtdeTextField.setText(Long.toString(papel.getQtde()));
	}
	
	public void setUser(User user) {
		this.user = user;
		
		if (user.getLevel() < 3) {
			qtdeTextField.setDisable(true);
			
			if (papel == null)
				qtdeTextField.setText("0");
		}
	}

}
