package com.marioconcilio.cardmanager.view.listorcamento;

import com.marioconcilio.cardmanager.model.dao.OrcamentoDAO;
import com.marioconcilio.cardmanager.model.vo.Orcamento;
import com.marioconcilio.cardmanager.util.Formatter;
import com.marioconcilio.cardmanager.view.root.RootViewController;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.math.BigDecimal;
import java.util.Date;

public class ListOrcamentoViewController {
	
	@FXML
	private TableView<Orcamento> tableView;
	
	@FXML
	private TableColumn<Orcamento, Date> dateColumn;
	
	@FXML
	private TableColumn<Orcamento, String> nameColumn;
	
	@FXML
	private TableColumn<Orcamento, BigDecimal> valueColumn;
	
	@FXML
	private ProgressIndicator progressIndicator;
	
	private RootViewController rootViewController;
	
	@FXML
	public void initialize() {
		dateColumn.setMinWidth(110.0);
		dateColumn.setMaxWidth(110.0);
		valueColumn.setMinWidth(180.0);
		valueColumn.setMaxWidth(180.0);
		
		dateColumn.setCellValueFactory(o -> o.getValue().dataProperty());
		nameColumn.setCellValueFactory(o -> o.getValue().getClient().nameProperty());
		valueColumn.setCellValueFactory(o -> o.getValue().totalProperty());
		
		dateColumn.setStyle("-fx-alignment: CENTER;");
		valueColumn.setStyle("-fx-alignment: CENTER-RIGHT;");
		
		dateColumn.setCellFactory(column -> {
			return new TableCell<Orcamento, Date>() {
				@Override
				protected void updateItem(Date item, boolean empty) {
					super.updateItem(item, empty);					
					if (item == null || empty) 
						setText(null);
					else 
						setText(Formatter.formatDate(item));
				}
			};
		});
		
		valueColumn.setCellFactory(column -> {
			return new TableCell<Orcamento, BigDecimal>() {
				@Override
				protected void updateItem(BigDecimal item, boolean empty) {
					super.updateItem(item, empty);
					if (item == null || empty)
						setText(null);
					else 
						setText(Formatter.formatCurrency(item));
				}
			};
		});
		
		tableView.setOnMouseClicked(event -> {
			if (event.getClickCount() == 2) {
				Orcamento orcamento = tableView.getSelectionModel().getSelectedItem();
				rootViewController.showOrcamentoView(orcamento);
			}
		});
		
		/*
		 * fetch
		 */
		OrcamentoDAO dao = new OrcamentoDAO();
		dao.setProgressIndicator(progressIndicator);
		dao.listAll(list -> {
			tableView.setItems(list);	
			progressIndicator.setVisible(false);
		});
	}
	
	public void setRootViewController(RootViewController rootViewController) {
		this.rootViewController = rootViewController;
	}

}
