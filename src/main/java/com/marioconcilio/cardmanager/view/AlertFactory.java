package com.marioconcilio.cardmanager.view;

import com.marioconcilio.cardmanager.util.Constants;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;

import java.util.Optional;

public final class AlertFactory {
	
	private AlertFactory() {
		//
	}
	
	/**
	 * Shows an alert view with confirmation and returns the button clicked by the user.
	 * @param message  a custom message to the user.
	 * @return the ButtonType clicked by the user.
	 */
	public static ButtonType showConfirmation(final String message) {
		return showAlert(AlertType.CONFIRMATION, "Confirmar", message);
	}
	
	/**
	 * Shows an alert view with confirmation and returns the button clicked by the user.
	 * @param title The title.
	 * @param message a custom message to the user.
	 * @return the ButtonType clicked by the user.
	 */
	public static ButtonType showConfirmation(final String title, final String message) {
		return showAlert(AlertType.CONFIRMATION, title, message);
	}
	
	/**
	 * Shows an alert view with an error message and the exception stack trace.
	 * @param ex the exception thrown.
	 * @param message a custom message to the user.
	 */
	public static void showError(final Exception ex, final String message) {
		showAlertException(ex, "Erro", message);
	}
	
	/**
	 * Shows an alert view with an error message and the exception stack trace.
	 * @param ex the exception thrown.
	 * @param title the custom title.
	 * @param message a custom message to the user.
	 */
	public static void showError(final Exception ex, final String title, final String message) {
		showAlertException(ex, title, message);
	}
	
	/**
	 * Shows an alert view with an information message.
	 * @param message a custom message to the user.
	 */
	public static void showInfo(final String message) {
		showAlert(AlertType.INFORMATION, "Info", message);
	}
	
	/**
	 * Shows an alert view with an information message.
	 * @param title a title.
	 * @param message a custom message to the user.
	 */
	public static void showInfo(final String title, final String message) {
		showAlert(AlertType.INFORMATION, title, message);
	}
	
	/**
	 * Shows an alert view with a warning message.
	 * @param message a custom message to the user.
	 */
	public static void showWarning(final String message) {
		showAlert(AlertType.WARNING, "Aviso", message);
	}
	
	/**
	 * Shows an alert view with a warning message.
	 * @param title a title.
	 * @param message a custom message to the user.
	 */
	public static void showWarning(final String title, final String message) {
		showAlert(AlertType.WARNING, title, message);
	}
	
	private static ButtonType showAlert(final AlertType type, final String title, final String message) {
		final Alert alert = new Alert(type);
		alert.setTitle(Constants.APP_NAME);
		alert.setHeaderText(title);
		alert.setContentText(message);
		alert.getDialogPane().getStylesheets().add(AlertFactory.class.getClassLoader().getResource("bootstrap3.css").toExternalForm());
		alert.getDialogPane().getStylesheets().add(AlertFactory.class.getClassLoader().getResource("gui.css").toExternalForm());
		alert.getDialogPane().getScene().getWindow().sizeToScene();
		alert.getDialogPane().getChildren().stream().filter(node -> node instanceof Label).forEach(node -> {
			final Label label = (Label) node;
			label.setMinHeight(Region.USE_PREF_SIZE);
			label.setMinWidth(Region.USE_PREF_SIZE);
		});
		
		final Optional<ButtonType> button = alert.showAndWait();
		return button.get();
	}
	
	private static ButtonType showAlertException(final Exception ex, final String title, final String message) {
		final AlertException alert = new AlertException(AlertType.ERROR, ex);
		alert.setTitle(Constants.APP_NAME);
		alert.setHeaderText(title);
		alert.setContentText(message);
		alert.getDialogPane().getStylesheets().add(AlertFactory.class.getClassLoader().getResource("bootstrap3.css").toExternalForm());
		alert.getDialogPane().getStylesheets().add(AlertFactory.class.getClassLoader().getResource("gui.css").toExternalForm());
		
		final Optional<ButtonType> button = alert.showAndWait();
		return button.get();
	}

}
