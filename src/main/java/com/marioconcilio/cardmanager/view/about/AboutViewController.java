package com.marioconcilio.cardmanager.view.about;

import com.marioconcilio.cardmanager.util.Constants;
import com.marioconcilio.cardmanager.view.dialog.DialogInterface;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class AboutViewController implements DialogInterface {
	
	@FXML
	private Label label;
	
	@FXML
	private Button okButton;
	
	private Stage dialogStage;
	
	@FXML
	private void initialize() {
		label.setText(Constants.APP_VERSION);
		this.okButton.setOnAction(event -> this.dialogStage.close());
	}

	@Override
	public void setStage(Stage stage) {
		this.dialogStage = stage;
	}
}
