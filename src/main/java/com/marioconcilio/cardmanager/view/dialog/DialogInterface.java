package com.marioconcilio.cardmanager.view.dialog;

import javafx.stage.Stage;

public interface DialogInterface {

	void setStage(Stage stage);
	
}
