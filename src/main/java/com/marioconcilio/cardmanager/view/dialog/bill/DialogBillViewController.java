package com.marioconcilio.cardmanager.view.dialog.bill;

import com.marioconcilio.cardmanager.model.vo.Bill;
import com.marioconcilio.cardmanager.util.Formatter;
import com.marioconcilio.cardmanager.view.AbstractListBillViewController;
import com.marioconcilio.cardmanager.view.dialog.DialogInterface;
import javafx.collections.FXCollections;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class DialogBillViewController extends AbstractListBillViewController implements DialogInterface {

    private Stage stage;

    @FXML
    public void initialize() {
		super.initialize();
    }

    @FXML
    private void okButtonAction() {
        stage.close();
    }

    @Override
    public void setStage(Stage stage) {
        stage.setResizable(false);
        stage.setMinWidth(800.0);
        stage.setMaxWidth(800.0);
        stage.setMinHeight(600.0);
        stage.setMaxHeight(600.0);
        this.stage = stage;
    }

    public void setBillList(List<Bill> list) {
        tableView.setItems(FXCollections.observableList(list));
    }
}
