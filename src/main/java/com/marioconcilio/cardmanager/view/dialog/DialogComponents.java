package com.marioconcilio.cardmanager.view.dialog;

import javafx.stage.Stage;

public class DialogComponents {
	
	private Stage stage;
	private DialogInterface controller;
	
	public DialogComponents(Stage stage, DialogInterface controller) {
		this.stage = stage;
		this.controller = controller;
	}
	
	public Stage getStage() {
		return stage;
	}
	
	public DialogInterface getController() {
		return controller;
	}

	public void showAndWait() {
		stage.showAndWait();
	}

}
