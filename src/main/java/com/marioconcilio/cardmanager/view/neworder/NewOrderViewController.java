package com.marioconcilio.cardmanager.view.neworder;

import com.marioconcilio.cardmanager.MainApp;
import com.marioconcilio.cardmanager.controller.CieloController;
import com.marioconcilio.cardmanager.model.vo.Card;
import com.marioconcilio.cardmanager.model.vo.CieloCard;
import com.marioconcilio.cardmanager.model.vo.Card.CieloCardType;
import com.marioconcilio.cardmanager.scene.control.CardTypeEditableCell;
import com.marioconcilio.cardmanager.scene.control.DateEditableCell;
import com.marioconcilio.cardmanager.scene.control.ValueEditableCell;
import com.marioconcilio.cardmanager.util.Constants;
import com.marioconcilio.cardmanager.util.Formatter;
import com.marioconcilio.cardmanager.view.AlertFactory;
import com.marioconcilio.cardmanager.view.ListCardViewController;
import com.marioconcilio.cardmanager.view.notfound.NotFoundViewController;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.StringConverter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class NewOrderViewController extends ListCardViewController {

	@FXML
	private DatePicker datePicker;

	@FXML
	private ComboBox<CieloCardType> typeComboBox;

	@FXML
	private TextField valueTextField;

	@FXML
	private Button addButton;

	@FXML
	private Button endButton;

	@FXML
	private Button removeButton;
	
	private MainApp mainApp;
	private ObservableList<Card> cardList;

	public NewOrderViewController() {
		this.cardList = FXCollections.observableArrayList();
	}

	@Override
	@FXML
	protected void initialize() {
		super.initialize();
		this.tableView.setItems(cardList);
		
		// bindings
		this.addButton.disableProperty().bind(
				Bindings.or(
						Bindings.or(Bindings.isNull(this.typeComboBox.getSelectionModel().selectedItemProperty()), 
								Bindings.isNull(this.datePicker.valueProperty())),
						Bindings.equal(0, Bindings.length(this.valueTextField.textProperty()))));
		
		this.removeButton.disableProperty().bind(
				Bindings.isNull(this.tableView.getSelectionModel().selectedItemProperty()));
		
		this.endButton.disableProperty().bind(
				Bindings.equal(0, Bindings.size(this.cardList)));

		this.valueTextField.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.ENTER)
				this.addOrder();
		});
		
//		this.valueTextField.setOnKeyTyped(event -> this.valueTextField.getStyleClass().remove("validation_error"));
		// this.cardTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> this.showCard(newValue));

//		this.valueColumn.setCellFactory(cell -> new ValueEditableCell());
//		this.typeColumn.setCellFactory(cell -> new CardTypeEditableCell());
//		this.dateColumn.setCellFactory(cell -> new DateEditableCell());
		this.tableView.setEditable(true);

		// combobox
		ObservableList<CieloCardType> list = FXCollections.observableArrayList(CieloCardType.values());
		this.typeComboBox.setItems(list);

		// datepicker
		this.datePicker.setConverter(new StringConverter<LocalDate>() {
			DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

			@Override
			public String toString(LocalDate date) {
				if (date != null)
					return dateFormatter.format(date);
				return "";
			}

			@Override
			public LocalDate fromString(String string) {
				if (string != null && !string.isEmpty())
					return LocalDate.parse(string, dateFormatter);
				return null;
			}
		});

		// textfield
		this.valueTextField.addEventFilter(KeyEvent.KEY_TYPED, e -> {
        	TextField tx = (TextField) e.getSource();
        	if (tx.getText().length() >= 10) {
        		e.consume();
        	}
        	
        	if (!e.getCharacter().matches("[0-9,.]")) {
        		e.consume();
        	}
        });
		
		/*
		DecimalFormat format = new DecimalFormat("#.0");
		// TODO: fix backspace not working on last number to delete
		this.valueTextField.setTextFormatter(new TextFormatter<>(c -> {
			ParsePosition parsePosition = new ParsePosition(0);
			Object object = format.parse(c.getControlNewText(), parsePosition);

			if (object == null || parsePosition.getIndex() < c.getControlNewText().length())
				return null;
			else
				return c;
		}));
		*/
	}

	@Override
	public void populateTable() {

	}

	@Override
	public void setCardOk() {

	}

	@Override
	public void deleteCard() {

	}

	@Override
	public void processExtrato() {

	}

	@FXML
	private void addOrder() {
		LocalDate localDate = this.datePicker.getValue();
		String value = this.valueTextField.getText().replace(",", ".");
		
		try {
			// creating and adding new card
			CieloCard card = new CieloCard();
			card.setCardType(this.typeComboBox.getValue());
			card.setDate(Formatter.toDate(localDate));
			card.setValue(new BigDecimal(value));
			
			this.cardList.add(card);
			this.valueTextField.requestFocus();
			this.valueTextField.getStyleClass().remove("validation_error");
		}
		catch (NumberFormatException ex) {
			this.valueTextField.getStyleClass().add("validation_error");
		}
		
	}

	@FXML
	public void removeOrder() {
		Card card = (Card) this.tableView.getSelectionModel().getSelectedItem();
		if (card == null)
			return;

		this.cardList.remove(card);
	}

	@FXML
	private void processOrders() {
		/*
		final CieloController controller = new CieloController();
		try {
			controller.execute(this.cardList, cardsNotFound -> {
				if (!cardsNotFound.isEmpty()) {
					showNotFoundDialog(cardsNotFound);
				}
				
				final int cardsFound = controller.getCardsFound();
				if (cardsFound > 0) {
					final Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle(Constants.APP_NAME);
					alert.setHeaderText("Atualizado com sucesso");
					alert.setContentText(cardsFound + " entradas atualizadas.");
					alert.showAndWait();
				}
			});
		} 
		catch (Exception e) {
			AlertFactory.showError(e, "Não foi possível atualizar extrato.");
		}
		*/
	}
	
	public void showNotFoundDialog(List<CieloCard> list) {
		/*
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/notfound/NotFoundView.fxml"));
			Pane pane = (Pane) loader.load();
			
			Stage dialogStage = new Stage();
		    dialogStage.setTitle(Constants.APP_NAME);
		    dialogStage.initModality(Modality.WINDOW_MODAL);
		    dialogStage.initOwner(mainApp.getPrimaryStage());
		    dialogStage.initStyle(StageStyle.UTILITY);
		    dialogStage.setResizable(false);
		    
		    Scene scene = new Scene(pane);
		    scene.getStylesheets().add(MainApp.class.getResource("/resources/gui.css").toExternalForm());
		    dialogStage.setScene(scene);
		    
		    NotFoundViewController controller = loader.getController();
		    controller.setCardList(list);
		    
		    dialogStage.showAndWait();
//		    controller.setStage(dialogStage);
		    
//		    return new DialogComponents(dialogStage, controller);
		}
		catch (Exception ex) {
			ex.printStackTrace();
//			return null;
		}
		
//		DialogComponents components = this.createDialog("view/notfound/NotFoundView.fxml");
//		NotFoundViewController vc = (NotFoundViewController) components.getController();
//		
//		vc.setCardList(list);
//		components.getStage().showAndWait();
*/
	}
	
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}
}
