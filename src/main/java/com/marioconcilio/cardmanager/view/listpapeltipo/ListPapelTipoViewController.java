package com.marioconcilio.cardmanager.view.listpapeltipo;

import com.marioconcilio.cardmanager.handler.LimitCharactersHandler;
import com.marioconcilio.cardmanager.handler.LimitCharactersHandler.LimitCharactersType;
import com.marioconcilio.cardmanager.model.dao.PapelTipoDAO;
import com.marioconcilio.cardmanager.model.vo.PapelTipo;
import com.marioconcilio.cardmanager.model.vo.User;
import com.marioconcilio.cardmanager.util.Formatter;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.Optional;

public class ListPapelTipoViewController {
	
	private static final Logger logger = LogManager.getLogger();
	
	@FXML
	private TableView<PapelTipo> tableView;

	@FXML
	private TableColumn<PapelTipo, String> tipoColumn;
	
	@FXML
	private TableColumn<PapelTipo, BigDecimal> precoColumn;
	
	@FXML
	private ProgressIndicator progressIndicator;
	
	private User user;
	
	@FXML
	public void initialize() {
		tipoColumn.setCellValueFactory(papelTipo -> papelTipo.getValue().descricaoProperty());
		precoColumn.setCellValueFactory(papelTipo -> papelTipo.getValue().precoKgProperty());
		
		precoColumn.setMinWidth(120.0);
		precoColumn.setMaxWidth(120.0);
		precoColumn.setStyle("-fx-alignment: CENTER-RIGHT;");
		precoColumn.setCellFactory(column -> {
			return new TableCell<PapelTipo, BigDecimal>() {
				@Override
				protected void updateItem(BigDecimal item, boolean empty) {
					super.updateItem(item, empty);
					if (item == null || empty)
						setText(null);
					else 
						setText(Formatter.formatCurrency(item));
				}
			};
		});
		
		/*
		 * editing
		 */
		tableView.setRowFactory(table -> {
			final TableRow<PapelTipo> row = new TableRow<>();
			final ContextMenu rowMenu = new ContextMenu();
			
			MenuItem editItem = new MenuItem("Editar");
			editItem.setOnAction(event -> {
				PapelTipo papelTipo = row.getItem();
				showDialog(papelTipo);
			});
			
			rowMenu.getItems().add(editItem);
			
			row.contextMenuProperty().bind(
					Bindings.when(Bindings.isNotNull(row.itemProperty()))
					.then(rowMenu)
					.otherwise((ContextMenu) null));
			
			return row;
		});
		
		tableView.setOnMouseClicked(event -> {
			if (event.getClickCount() == 2) {
				PapelTipo papelTipo = tableView.getSelectionModel().getSelectedItem();
				showDialog(papelTipo);
			}
		});
		
		/*
		 * fetch
		 */
		PapelTipoDAO dao = new PapelTipoDAO();
		dao.setProgressIndicator(progressIndicator);
		dao.listAll(list -> {
			tableView.setItems(list);	
			progressIndicator.setVisible(false);
		});
	}
	
	@FXML
	private void okButtonAction(ActionEvent event) {
		Stage scene = (Stage) tableView.getScene().getWindow();
		scene.close();
	}
	
	private void showDialog(PapelTipo papelTipo) {
		if (user.getLevel() < 2) return;
		
		Dialog<BigDecimal> dialog = new Dialog<>();
		dialog.setTitle("Alterar Preço do Kg");
		dialog.setHeaderText("Alterar Preço do Kg");
		dialog.setContentText(papelTipo.getDescricao());
		dialog.setGraphic(new ImageView(this.getClass().getResource("/resources/icons/paper48.png").toString()));
		
		// Set the button types.
		dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

		// Create labels and fields.
		GridPane grid = new GridPane();
		grid.setHgap(5);
		grid.setVgap(5);
		grid.setPadding(new Insets(20, 75, 10, 10));
		
		TextField precoTextField = new TextField();
		precoTextField.setPromptText("Preço Kg");
		
		precoTextField.addEventFilter(KeyEvent.KEY_TYPED, new LimitCharactersHandler(100, LimitCharactersType.CURRENCY));
		
		grid.add(new Label("Preço Kg"), 0, 0);
		grid.add(precoTextField, 1, 0);
		
		// Validate qtdeProperty field and enable/disable ok button
		Button okButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
		
		// Bindings
		DoubleProperty precoProperty = new SimpleDoubleProperty(papelTipo.getPrecoKg().doubleValue());

		precoTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			try {
				precoProperty.set(Double.parseDouble(newValue.replace("R$ ", "").replace(",", ".")));
			}
			catch (NumberFormatException ex) {
				precoProperty.set(0.0);
			}
		});
		
		precoTextField.focusedProperty().addListener((observable, lostFocus, gainedFocus) -> {
			// gained focus -> 1234,56
			if (gainedFocus) {
				try {
					String oldValue = precoTextField.getText();
					String newValue = Formatter.parseCurrency(oldValue).toString().replace(".", ",");
					precoTextField.setText(newValue);
				}
				catch (NumberFormatException ex) {}
			}
			
			// lost focus -> R$ 1234,56
			if (lostFocus) {
				try {
					String oldValue = precoTextField.getText().replaceAll(",", ".");
					String newValue = Formatter.formatCurrency(new BigDecimal(oldValue));
					precoTextField.setText(newValue);
					
					precoProperty.set(Double.parseDouble(oldValue));
				}
				catch (NumberFormatException ex) {
					precoProperty.set(0.0);
				}
			}
		});
		
		BooleanBinding validPreco = Bindings.greaterThan(precoProperty, 0.0);
		
		okButton.disableProperty().bind(Bindings.not(validPreco));
		
		VBox vbox = new VBox();
		vbox.setPadding(new Insets(15, 10, 10, 10));
		vbox.getChildren().addAll(new Label(papelTipo.getDescricao()), grid);
		
		dialog.getDialogPane().setContent(vbox);
		
		// After all set
		Platform.runLater(() -> {
			String preco = Formatter.formatCurrency(papelTipo.getPrecoKg());
			precoTextField.setText(preco);
		});
		
		// Convert the result to a qtdeProperty-precoUn-pair when ok button is clicked.
		dialog.setResultConverter(dialogButton -> {
		    if (dialogButton == ButtonType.OK) {
		    	BigDecimal preco = Formatter.parseCurrency(precoTextField.getText());
		        return preco;
		    }
		    
		    return null;
		});
		
		Optional<BigDecimal> result = dialog.showAndWait();

		result.ifPresent(precoKg -> {
			papelTipo.setPrecoKg(precoKg);
			
			PapelTipoDAO dao = new PapelTipoDAO();
			dao.update(papelTipo);
			dao.listAll(list -> {
				tableView.setItems(list);
			});
			
			logger.info("Altered {}. New PrecoKg: {}", papelTipo, Formatter.formatCurrency(precoKg));
		});
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
}
