package com.marioconcilio.cardmanager.view;

import com.marioconcilio.cardmanager.model.dao.UserDAO;
import com.marioconcilio.cardmanager.model.vo.User;
import com.marioconcilio.cardmanager.scene.transition.ShakeTransition;
import com.marioconcilio.cardmanager.util.Cipher;
import com.marioconcilio.cardmanager.view.wait.WaitStage;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.*;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.function.Consumer;

public class LoginDialog extends Dialog<Pair<String, String>> {
	
	protected static final Logger logger = LogManager.getLogger();
	
	private TextField username;
	private PasswordField password;
	private Node loginButton;
	private Consumer<User> onSuccess;

	public LoginDialog() {
		// Create the custom dialog.
		this.setTitle("Login");
		this.setHeaderText("Fazer Login");
		this.initModality(Modality.APPLICATION_MODAL);
		this.getDialogPane().getStylesheets().add(getClass().getClassLoader().getResource("bootstrap3.css").toExternalForm());
		this.getDialogPane().getStylesheets().add(getClass().getClassLoader().getResource("gui.css").toExternalForm());

		// Set the icon (must be included in the project).
		this.setGraphic(new ImageView(getClass().getClassLoader().getResource("icons/key-48.png").toExternalForm()));

		// Set the button types.
		ButtonType loginButtonType = new ButtonType("Login", ButtonData.OK_DONE);
		this.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

		// Create the username and password labels and fields.
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 75, 10, 10));

		username = new TextField();
		username.setPromptText("Login");
		password = new PasswordField();
		password.setPromptText("Senha");

		grid.add(new Label("Login"), 0, 0);
		grid.add(username, 1, 0);
		grid.add(new Label("Senha"), 0, 1);
		grid.add(password, 1, 1);

//		HBox hbox = new HBox(10);
//		final ProgressIndicator pi = new ProgressIndicator();
//		pi.setPrefSize(50.0, 50.0);
//		pi.setVisible(false);
//		hbox.getChildren().addAll(grid, pi);

		// Enable/Disable login button depending on whether a username was entered.
		loginButton = this.getDialogPane().lookupButton(loginButtonType);
		loginButton.setDisable(true);

		username.textProperty().addListener((observable, oldValue, newValue) -> {
			loginButton.setDisable(newValue.trim().isEmpty());
		});

		this.getDialogPane().setContent(grid);
		
		loginButton.addEventFilter(EventType.ROOT, ev -> {
			if (ev.getEventType().equals(ActionEvent.ACTION)) {
				ev.consume();
				
				WaitStage wait = new WaitStage(null, "Fazendo Login");
				getDialogPane().setEffect(new GaussianBlur());
				wait.show();
				
				String login = username.getText().toLowerCase();
				UserDAO dao = new UserDAO();
				dao.userFromLogin(login, user -> {
					getDialogPane().setEffect(null);
					wait.close();
					
					boolean isSuccessful = false;
					
					if (user != null) {
						String pass = Cipher.digestSHA512(password.getText().trim());
						if (pass.equals(user.getPassword())) {
							isSuccessful = true;
						}
					}
					
					if (isSuccessful) {
						onSuccess.accept(user);
						close();
					}
					else {
						shake();
					}
				});
				
			}
		});

		// Request focus on the username field by default.
		Platform.runLater(() -> username.requestFocus());
	}
	
	public void shake() {
		ShakeTransition anim = new ShakeTransition(this.getDialogPane(), e -> {
			username.requestFocus();
		});
		
		anim.playFromStart();
	}
	
	public void setOnSuccess(Consumer<User> onSuccess) {
		this.onSuccess = onSuccess;
	}
	
}
