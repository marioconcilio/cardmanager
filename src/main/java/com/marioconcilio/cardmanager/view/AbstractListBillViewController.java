package com.marioconcilio.cardmanager.view;

import com.marioconcilio.cardmanager.model.vo.Bill;
import com.marioconcilio.cardmanager.util.Formatter;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Mario Concilio.
 */
public abstract class AbstractListBillViewController {

    @FXML
    protected TableView<Bill> tableView;

    @FXML
    protected TableColumn<Bill, Number> numberColumn;

    @FXML
    protected TableColumn<Bill, String> nameColumn;

    @FXML
    protected TableColumn<Bill, BigDecimal> valueColumn;

    @FXML
    protected TableColumn<Bill, Date> dateColumn;

    @FXML
    protected TableColumn<Bill, Bill.BillStatus> statusColumn;

    protected void initialize() {
        /*
		 * cell value factory
		 */
        this.tableView.setPlaceholder(new Label(""));
        this.numberColumn.setCellValueFactory(billData -> billData.getValue().idProperty());
        this.valueColumn.setCellValueFactory(billData -> billData.getValue().valueProperty());
        this.dateColumn.setCellValueFactory(billData -> billData.getValue().dateProperty());
        this.statusColumn.setCellValueFactory(billData -> billData.getValue().statusProperty());
        if (this.nameColumn != null)
            this.nameColumn.setCellValueFactory(billData -> billData.getValue().getClient().nameProperty());

        /*
         * alignment
         */
        this.numberColumn.getStyleClass().add("row-align-left");
        this.valueColumn.getStyleClass().add("row-align-right");
        this.dateColumn.getStyleClass().add("row-align-center");
        this.statusColumn.getStyleClass().add("row-align-left");
        if (this.nameColumn != null)
            this.nameColumn.getStyleClass().add("row-align-left");

        /*
         * size
         */
        this.numberColumn.setMinWidth(100.0);
        this.numberColumn.setMaxWidth(100.0);
        this.numberColumn.setResizable(false);
        this.valueColumn.setMinWidth(150.0);
        this.valueColumn.setMaxWidth(150.0);
        this.valueColumn.setResizable(false);
        this.dateColumn.setMinWidth(120.0);
        this.dateColumn.setMaxWidth(120.0);
        this.dateColumn.setResizable(false);
        this.statusColumn.setMinWidth(170.0);
        this.statusColumn.setMaxWidth(170.0);
        this.statusColumn.setResizable(false);

        /*
		 * formatters
		 */
        this.dateColumn.setCellFactory(column -> new TableCell<Bill, Date>() {
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty)
                    setText(null);
                else
                    setText(Formatter.formatDate(item));
            }
        });

        this.valueColumn.setCellFactory(column -> new TableCell<Bill, BigDecimal>() {
            @Override
            protected void updateItem(BigDecimal item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty)
                    setText(null);
                else
                    setText(Formatter.formatCurrency(item));
            }
        });

        this.numberColumn.setCellFactory(column -> new TableCell<Bill, Number>() {
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty)
                    setText(null);
                else
                    setText(Formatter.formatBillNumber(item.longValue()));
            }
        });

        /*
         * bill number order
         */
        this.numberColumn.setComparator((n1, n2) -> {
            int num1 = n1.intValue();
            int num2 = n2.intValue();

            if (num1 / 1000 == 0) num1 *= 1000;
            if (num2 / 1000 == 0) num2 *= 1000;

            if (num1 > num2) return 1;
            if (num1 < num2) return -1;

            return 0;
        });

        /*
		 * color
		 */
        final PseudoClass greenRow = PseudoClass.getPseudoClass("green-row");
        final PseudoClass yellowRow = PseudoClass.getPseudoClass("yellow-row");
        final PseudoClass orangeRow = PseudoClass.getPseudoClass("orange-row");
        final PseudoClass redRow = PseudoClass.getPseudoClass("red-row");
        this.tableView.setRowFactory(p -> new TableRow<Bill>() {
            @Override
            protected void updateItem(Bill bill, boolean empty) {
                super.updateItem(bill, empty);
                if (bill == null || empty) return;

                pseudoClassStateChanged(greenRow, bill.isLiquidado());
                pseudoClassStateChanged(yellowRow, bill.isVencido());
                pseudoClassStateChanged(orangeRow, bill.isCartorio());
                pseudoClassStateChanged(redRow, bill.isProtestado());
            }
        });
    }

}
