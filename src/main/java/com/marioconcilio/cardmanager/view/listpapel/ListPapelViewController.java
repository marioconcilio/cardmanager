package com.marioconcilio.cardmanager.view.listpapel;

import com.marioconcilio.cardmanager.model.dao.PapelDAO;
import com.marioconcilio.cardmanager.model.vo.Papel;
import com.marioconcilio.cardmanager.report.EstoquePapelReport;
import com.marioconcilio.cardmanager.util.Formatter;
import com.marioconcilio.cardmanager.view.root.RootViewController;
import com.marioconcilio.cardmanager.view.wait.WaitStage;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.function.Consumer;

public class ListPapelViewController {
	
	@FXML
    private TextField filterField;
	
	@FXML
	private TableView<Papel> tableView;
	
	@FXML
	private TableColumn<Papel, String> papelColumn;
	
	@FXML
	private TableColumn<Papel, Number> grsColumn;
	
	@FXML
	private TableColumn<Papel, String> formatoColumn;
	
	@FXML
	private TableColumn<Papel, Number> flsColumn;
	
	@FXML
	private TableColumn<Papel, BigDecimal> pesoColumn;
	
	@FXML
	private TableColumn<Papel, String> marcaColunm;
	
	@FXML
	private TableColumn<Papel, String> precoColumn;
	
	@FXML
	private TableColumn<Papel, Number> qtdeColumn;
	
	@FXML
	private ProgressIndicator progressIndicator;
	
	@FXML
	private BorderPane pane;
	
	private RootViewController rootViewController;
	
	@FXML
	public void initialize() {
		/*
		 * setup
		 */
		grsColumn.setMinWidth(50.0);
		grsColumn.setMaxWidth(50.0);
		formatoColumn.setMinWidth(70.0);
		formatoColumn.setMaxWidth(70.0);
		flsColumn.setMinWidth(50.0);
		flsColumn.setMaxWidth(50.0);
		pesoColumn.setMinWidth(60.0);
		pesoColumn.setMaxWidth(60.0);
		precoColumn.setMinWidth(90.0);
		precoColumn.setMaxWidth(90.0);
		qtdeColumn.setMinWidth(60.0);
		qtdeColumn.setMaxWidth(60.0);
		
		this.tableView.setPlaceholder(new Label(""));
		this.papelColumn.setCellValueFactory(papel -> papel.getValue().getPapelTipo().tipoProperty());
		this.grsColumn.setCellValueFactory(papel -> papel.getValue().grsProperty());
		this.formatoColumn.setCellValueFactory(papel -> papel.getValue().formatoProperty());
		this.flsColumn.setCellValueFactory(papel -> papel.getValue().flsProperty());
		this.pesoColumn.setCellValueFactory(papel -> papel.getValue().pesoPropery());
		this.marcaColunm.setCellValueFactory(papel -> papel.getValue().marcaProperty());
		this.qtdeColumn.setCellValueFactory(papel -> papel.getValue().qtdeProperty());
		this.precoColumn.setCellValueFactory(papel -> {
			BigDecimal preco = papel.getValue().getPapelTipo().getPrecoKg();
			BigDecimal peso = papel.getValue().getPeso();
			BigDecimal precoFinal = preco.multiply(peso);
			String str = Formatter.formatCurrency(precoFinal);
			
			return new ReadOnlyObjectWrapper<String>(str);
		});
		
		this.tableView.setOnMouseClicked(event -> {
			if (event.getClickCount() == 2) {
				Papel papel = tableView.getSelectionModel().getSelectedItem();
				rootViewController.showEditPapelView(papel);
			}
		});
		
		this.grsColumn.setStyle("-fx-alignment: CENTER;");
		this.formatoColumn.setStyle("-fx-alignment: CENTER;");
		this.flsColumn.setStyle("-fx-alignment: CENTER;");
		this.pesoColumn.setStyle("-fx-alignment: CENTER;");
		this.qtdeColumn.setStyle("-fx-alignment: CENTER;");
		this.precoColumn.setStyle("-fx-alignment: CENTER-RIGHT;");
		
		/*
		 * color
		 */
		final PseudoClass redRow = PseudoClass.getPseudoClass("red_row");
		this.tableView.setRowFactory(p -> new TableRow<Papel>() {
			@Override
			protected void updateItem(Papel papel, boolean empty) {
				super.updateItem(papel, empty);
				if (papel == null || empty) return;
				
				pseudoClassStateChanged(redRow, (papel.getQtde() <= 0));
			}
		});
		
		/*
		 * fetch
		 */
		PapelDAO dao = new PapelDAO();
		dao.setProgressIndicator(progressIndicator);
		dao.listAll(list -> {
			// 1. Wrap the ObservableList in a FilteredList (initially display all data).
	        FilteredList<Papel> filteredData = new FilteredList<>(list, c -> true);

	        // 2. Set the filter Predicate whenever the filter changes.
	        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
	            filteredData.setPredicate(papel -> {
	                // If filter text is empty, display all persons.
	                if (newValue == null || newValue.isEmpty()) {
	                    return true;
	                }

	                // Compare first name and last name of every client with filter text.
	                String lowerCaseFilter = newValue.toLowerCase();

	                if (papel.getPapelTipo().getTipo().toLowerCase().contains(lowerCaseFilter)) {
	                    return true; // Filter matches first name.
	                } 
	                
	                return false; // Does not match.
	            });
	        });

	        // 3. Wrap the FilteredList in a SortedList. 
	        SortedList<Papel> sortedData = new SortedList<>(filteredData);
	     
	        // 4. Bind the SortedList comparator to the TableView comparator.
	        sortedData.comparatorProperty().bind(tableView.comparatorProperty());
			
	        tableView.setItems(sortedData);
//			tableView.setItems(list);
			progressIndicator.setVisible(false);
		});
	}
	
	@FXML
	public void printButtonAction(ActionEvent event) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Imprimir");
		alert.setHeaderText("Imprimir Estoque");
		alert.setContentText("Deseja imprimir o estoque de papel?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			Stage stage = rootViewController.getMainApp().getPrimaryStage();
			WaitStage wait = new WaitStage(stage, "Imprimindo Estoque");
			wait.show();
			
		    EstoquePapelReport report = new EstoquePapelReport();
		    report.generate(file -> {
		    	wait.close();
		    });
		}
	}
	
	public void getPapel(Consumer<Papel> consumer) {
		HBox bottom = new HBox(10);
		Button cancel = new Button("Cancelar");
		Button add = new Button("Adicionar");
		add.disableProperty().bind(Bindings.isNull(tableView.getSelectionModel().selectedItemProperty()));
		
		ImageView cancelImageView = new ImageView(new Image(getClass().getResourceAsStream("/resources/icons/cancel.png")));
		cancelImageView.setFitWidth(25.0);
		cancelImageView.setFitHeight(25.0);
		
		ImageView addImageView = new ImageView(new Image(getClass().getResourceAsStream("/resources/icons/add-cart-48.png")));
		addImageView.setFitWidth(25.0);
		addImageView.setFitHeight(25.0);
		
		cancel.setGraphic(cancelImageView);
		add.setGraphic(addImageView);
		
		bottom.setAlignment(Pos.CENTER_RIGHT);
		bottom.getChildren().addAll(cancel, add);
		bottom.setPadding(new Insets(0, 10, 10, 10));
		
		Label title = new Label("Escolher Papel");
		title.setFont(new Font("Open Sans", 24));
		
//		HBox top = new HBox();
//		top.setPadding(new Insets(10, 10, 0, 10));
//		top.getChildren().add(title);
		
		HBox oldTop = (HBox) pane.getTop();
		VBox top = new VBox(10);
		top.setPadding(new Insets(10, 10, 0, 10));
		top.getChildren().addAll(title, oldTop);
		
		pane.setTop(top);
		pane.setBottom(bottom);
		
		Stage stage = (Stage) tableView.getScene().getWindow();
		cancel.setOnAction(ev -> stage.close());
		
		tableView.setOnMouseClicked(ev -> {
			if (ev.getClickCount() == 2) {
				Papel papel = tableView.getSelectionModel().getSelectedItem();
				consumer.accept(papel);
			}
		});
		
		add.setOnAction(ev -> {
			Papel papel = tableView.getSelectionModel().getSelectedItem();
			consumer.accept(papel);
		});
	}

	public void setRootViewController(RootViewController rootViewController) {
		this.rootViewController = rootViewController;
	}

}
