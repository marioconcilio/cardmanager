package com.marioconcilio.cardmanager.view.editclient;

import com.marioconcilio.cardmanager.handler.LimitCharactersHandler;
import com.marioconcilio.cardmanager.handler.LimitCharactersHandler.LimitCharactersType;
import com.marioconcilio.cardmanager.model.dao.ClientDAO;
import com.marioconcilio.cardmanager.model.vo.Client;
import com.marioconcilio.cardmanager.util.Formatter;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.math.BigDecimal;
import java.util.Set;

public class EditClientViewController {
	
	@FXML
	private Button saveButton;
	
	@FXML
	private TextField nameTextField;
	
	@FXML
	private TextField addressTextField;
	
	@FXML
	private TextField cityTextField;
	
	@FXML
	private TextField cepTextField;
	
	@FXML
	private TextField emailTextField;
	
	@FXML
	private TextField creditLimitTextField;
	
	@FXML
	private ComboBox<String> stateComboBox;
	
	@FXML
	private GridPane formPane;
	
	private Client client;
	
	@FXML
	public void initialize() {
		nameTextField.addEventFilter(KeyEvent.KEY_TYPED, new LimitCharactersHandler(255));
		addressTextField.addEventFilter(KeyEvent.KEY_TYPED, new LimitCharactersHandler(255));
		cityTextField.addEventFilter(KeyEvent.KEY_TYPED, new LimitCharactersHandler(255));
		emailTextField.addEventFilter(KeyEvent.KEY_TYPED, new LimitCharactersHandler(255));
		cepTextField.addEventFilter(KeyEvent.KEY_TYPED, new LimitCharactersHandler(9, LimitCharactersType.CEP));
		creditLimitTextField.addEventFilter(KeyEvent.KEY_TYPED, new LimitCharactersHandler(10, LimitCharactersType.CURRENCY));
		
		DoubleProperty precoProperty = new SimpleDoubleProperty();
		creditLimitTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			try {
				precoProperty.set(Double.parseDouble(newValue.replace("R$ ", "").replace(",", ".")));
			}
			catch (NumberFormatException ex) {
				precoProperty.set(0.0);
			}
			
			System.out.println(precoProperty.get());
		});
		
		creditLimitTextField.focusedProperty().addListener((observable, lostFocus, gainedFocus) -> {
			// gained focus -> 1234,56
			if (gainedFocus) {
				try {
					String oldValue = creditLimitTextField.getText();
					String newValue = Formatter.parseCurrency(oldValue).toString().replace(".", ",");
					creditLimitTextField.setText(newValue);
				}
				catch (NumberFormatException ex) {}
			}
			
			// lost focus -> R$ 1234,56
			if (lostFocus) {
				try {
					String oldValue = creditLimitTextField.getText().replaceAll(",", ".");
					String newValue = Formatter.formatCurrency(new BigDecimal(oldValue));
					creditLimitTextField.setText(newValue);
					
					precoProperty.set(Double.parseDouble(oldValue));
				}
				catch (NumberFormatException ex) {
					precoProperty.set(0.0);
				}
			}
		});
		
		BooleanBinding validCredit = Bindings.greaterThan(precoProperty, 0.0);
		saveButton.disableProperty().bind(Bindings.not(validCredit));
		
		stateComboBox.setItems(FXCollections.observableArrayList("AC","AL","AP","AM","BA","CE","DF","ES","GO","MA","MT","MS","MG","PA","PB","PR","PE","PI","RJ","RN","RS","RO","RR","SC","SP","SE","TO"));
	}
	
	public void setClient(Client client) {
		this.client = client;
		
		this.nameTextField.setText(client.getName());
		this.addressTextField.setText(client.getAddress());
		this.cityTextField.setText(client.getCity());
		this.cepTextField.setText(Formatter.formatCep(client.getCep()));
		this.creditLimitTextField.setText(Formatter.formatCurrency(client.getCreditLimit()));
		
		String email = client.getEmail();
		if (email != null) 
			this.emailTextField.setText(email);
		
		this.stateComboBox.getSelectionModel().select(client.getState());
	}
	
	@FXML
	private void saveButtonAction(ActionEvent event) {
		if (checkAll()) {
			ClientDAO dao = new ClientDAO();
			client.setName(this.nameTextField.getText());
			client.setAddress(this.addressTextField.getText());
			client.setCity(this.cityTextField.getText());
			client.setState(this.stateComboBox.getSelectionModel().getSelectedItem());
			client.setCep(Formatter.parseCep(this.cepTextField.getText()));
			client.setEmail(this.emailTextField.getText());
			client.setCreditLimit(Formatter.parseCurrencyToInt(this.creditLimitTextField.getText()));
			
			dao.update(client);
			
			Button button = (Button) event.getSource();
			Stage stage = (Stage) button.getScene().getWindow();
			stage.close();
		}
	}

	@FXML
	private void cancelButtonAction(ActionEvent event) {
		Button button = (Button) event.getSource();
		Stage stage = (Stage) button.getScene().getWindow();
		stage.close();
	}
	
	private boolean checkAll() {
		int i = 0;
		
		Set<Node> nodes = this.formPane.lookupAll(".text-field");
		for(Node node: nodes) {
			TextField tx = (TextField) node;
			
			// disregard email text field, which can be empty
			if (tx.getId().equals("emailTextField")) 
				continue;
			
			if (tx.getText().length() == 0) {
				tx.getStyleClass().add("validation_error");
				i++;
			}
			else {
				tx.getStyleClass().remove("validation_error");
			}
		}
		
		return i == 0;
	}
	
}
