package com.marioconcilio.cardmanager.view.notfound;

import com.marioconcilio.cardmanager.model.vo.Card;
import com.marioconcilio.cardmanager.view.dialog.DialogInterface;
import com.marioconcilio.cardmanager.view.ListCardViewController;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.util.List;

public class NotFoundViewController extends ListCardViewController implements DialogInterface {
	
	@FXML
	private Button okButton;
	
	private Stage dialogStage;
	
	@Override
	@FXML
	protected void initialize() {
		super.initialize();
		this.okButton.setOnAction(event -> this.dialogStage.close());
	}

	@Override
	public void populateTable() {

	}

	@Override
	public void setCardOk() {

	}

	@Override
	public void deleteCard() {

	}

	@Override
	public void processExtrato() {

	}

	public void setCardList(List<Card> list) {
		this.tableView.setItems(FXCollections.observableList(list));
	}
	
	@Override
	public void setStage(Stage stage) {
		this.dialogStage = stage;
	}

}
