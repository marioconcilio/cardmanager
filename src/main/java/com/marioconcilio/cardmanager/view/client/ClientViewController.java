package com.marioconcilio.cardmanager.view.client;

import com.marioconcilio.cardmanager.model.vo.Bill;
import com.marioconcilio.cardmanager.model.vo.Bill.BillStatus;
import com.marioconcilio.cardmanager.model.vo.Client;
import com.marioconcilio.cardmanager.model.vo.User;
import com.marioconcilio.cardmanager.util.Formatter;
import com.marioconcilio.cardmanager.view.AbstractListBillViewController;
import com.marioconcilio.cardmanager.view.dialog.DialogInterface;
import com.marioconcilio.cardmanager.view.root.RootViewController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class ClientViewController extends AbstractListBillViewController implements DialogInterface {
	
	@FXML
	private Button editButton;
	
	@FXML
	private Label nameLabel;
	
	@FXML
	private Label cnpjLabel; 
	
	@FXML
	private Label addressLabel; 
	
	@FXML
	private Label cityLabel;
	
	@FXML
	private Label cepLabel;
	
	@FXML
	private Label emailLabel;
	
	@FXML
	private Label creditLimitLabel;
	
	@FXML
	private Label abertoLabel;
	
	@FXML
	private Label pagoLabel;
	
	@FXML
	private Label vencidoLabel;
	
	@FXML
	private Label cartorioLabel;
	
	@FXML
	private Label bxLabel;

	private Client client;
	private Stage stage;
	private RootViewController rootViewController;

	@FXML
	public void initialize() {
		super.initialize();
	}
	
	public void setClient(Client client) {
		this.client = client;
		
		this.nameLabel.setText(client.getName());
		this.cnpjLabel.setText(Formatter.formatCnpj(client.getId()));
		this.addressLabel.setText(client.getAddress());
		this.cepLabel.setText(Formatter.formatCep(client.getCep()));
		this.creditLimitLabel.setText(String.join(" ", "Limite de Crédito:", Formatter.formatCurrency(client.getCreditLimit())));
		this.cityLabel.setText(String.join("/", client.getCity(), client.getState()));
		
		if (client.getEmail() == null || client.getEmail().length() == 0) 
			this.emailLabel.setText("Nenhum email cadastrado.");
		else
			this.emailLabel.setText(client.getEmail());
		
		List<Bill> clientBills = client.getBills();
		ObservableList<Bill> bills = FXCollections.observableArrayList(clientBills);
		this.tableView.setItems(bills);
		
		BigDecimal aberto = clientBills.stream()
				.filter(b -> b.getStatus() == BillStatus.ABERTO && b.getDate().after(new Date()))
				.map(Bill::getValue)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		
		this.abertoLabel.setText(Formatter.formatCurrency(aberto));
		
		BigDecimal pago = clientBills.stream()
				.filter(b -> b.getStatus() == BillStatus.PAGO)
				.map(Bill::getValue)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		
		this.pagoLabel.setText(Formatter.formatCurrency(pago));
		
		BigDecimal vencido = clientBills.stream()
				.filter(b -> b.getStatus() == BillStatus.ABERTO && b.getDate().before(new Date()))
				.map(Bill::getValue)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		
		this.vencidoLabel.setText(Formatter.formatCurrency(vencido));
		
		BigDecimal cartorio = clientBills.stream()
				.filter(b -> b.getStatus() == BillStatus.CARTORIO)
				.map(Bill::getValue)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		
		this.cartorioLabel.setText(Formatter.formatCurrency(cartorio));
		
		BigDecimal bx = clientBills.stream()
				.filter(b -> b.getStatus() == BillStatus.PROTESTADO)
				.map(Bill::getValue)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		
		this.bxLabel.setText(Formatter.formatCurrency(bx));
	}
	
	@FXML
	private void editButtonAction() {
		rootViewController.openEditClient(this.client);
	}
	
	@FXML
	private void okButtonAction(ActionEvent event) {
		Button button = (Button) event.getSource();
		Stage stage = (Stage) button.getScene().getWindow();
		stage.close();
	}
	
	public void setRootViewController(RootViewController rootViewController) {
		this.rootViewController = rootViewController;
	}
	
	public void setUser(User user) {
		if (user.getLevel() < 2)
			editButton.setDisable(true);
	}

	@Override
	public void setStage(Stage stage) {
		this.stage = stage;
	}
}
