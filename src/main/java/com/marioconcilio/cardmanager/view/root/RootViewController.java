package com.marioconcilio.cardmanager.view.root;

import com.marioconcilio.cardmanager.MainApp;
import com.marioconcilio.cardmanager.model.vo.Bill.BillStatus;
import com.marioconcilio.cardmanager.model.vo.Client;
import com.marioconcilio.cardmanager.model.vo.Orcamento;
import com.marioconcilio.cardmanager.model.vo.Papel;
import com.marioconcilio.cardmanager.view.ListCardViewController;
import com.marioconcilio.cardmanager.view.client.ClientViewController;
import com.marioconcilio.cardmanager.view.editclient.EditClientViewController;
import com.marioconcilio.cardmanager.view.editpapel.EditPapelViewController;
import com.marioconcilio.cardmanager.view.list.bill.ListBillViewController;
import com.marioconcilio.cardmanager.view.list.client.ListClientViewController;
import com.marioconcilio.cardmanager.view.list.product.ListProductViewController;
import com.marioconcilio.cardmanager.view.listorcamento.ListOrcamentoViewController;
import com.marioconcilio.cardmanager.view.listpapel.ListPapelViewController;
import com.marioconcilio.cardmanager.view.listpapeltipo.ListPapelTipoViewController;
import com.marioconcilio.cardmanager.view.neworder.NewOrderViewController;
import com.marioconcilio.cardmanager.view.orcamento.OrcamentoViewController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class RootViewController {
	
	private static final int ESTOQUE_PANE 	= 0;
	private static final int PAPEL_PANE		= 1;
	private static final int BILL_PANE 		= 2;
	private static final int CARD_PANE 		= 3;
	private static final int PSMCARD_PANE	= 4;
	private static final int CADASTRO_PANE	= 5;
	
	@FXML
	private TabPane tabPane;
	
	@FXML
	private Tab cielo_cardTab;
	
	@FXML
	private Tab psm_cardTab;

    /********************************************************************
     * Cielo
     ********************************************************************/
    @FXML
    private Button cielo_okButton;

    @FXML
    private Button cielo_deleteButton;

	@FXML
    private Button cielo_setupRatesButton;

    /********************************************************************
     * PagSeguro
     ********************************************************************/
    @FXML
    private Button psm_okButton;

    @FXML
    private Button psm_deleteButton;

    /********************************************************************
     * Boletos
     ********************************************************************/
	@FXML
	private Button remessaButton;
	
	@FXML
	private Button retornoButton;

	@FXML
	private Button pagoButton;
	
	@FXML
	private Button cartorioButton;
	
	@FXML
	private Button bxButton;

    /********************************************************************
     * Controllers
     ********************************************************************/
	private MainApp mainApp;
	private ListBillViewController listBillViewController;
	private ListCardViewController<?> listCardViewController;

	@FXML
	public void initialize() {
		this.tabPane.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
			switch (newValue.intValue()) {
			case ESTOQUE_PANE:
				stock_listQuimicoAction();
				break;
				
			case PAPEL_PANE:
				showListPapel();
				break;
				
			case BILL_PANE:
				bill_listAction();
				break;
			
			case CARD_PANE:
				cielo_listCardAction();
				break;
				
			case PSMCARD_PANE:
				psm_listCardAction();
				break;
				
			case CADASTRO_PANE:
				listClientAction();
				break;
			}
		});

		/*
		// buttons by level of access
        int level = mainApp.getUser().getLevel();
        switch(level) {
            case 1:
                pagoButton.setDisable(true);
                cartorioButton.setDisable(true);
                bxButton.setDisable(true);
                cielo_okButton.setDisable(true);
                cielo_deleteButton.setDisable(true);
                cielo_setupRatesButton.setDisable(true);
                psm_okButton.setDisable(true);
                psm_deleteButton.setDisable(true);

            case 2:
                remessaButton.setDisable(true);
                retornoButton.setDisable(true);
                break;

            default:
                break;
        }
        */
	}
	
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
		
		if (mainApp.getUser().getLevel() < 2) {
//			cardTab.setDisable(true);
		}
	}
	
	/********************************************************************
	 * Cadastro
	 ********************************************************************/

	@FXML
	public void listClientAction() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/list/client/ListClientView.fxml"));
			mainApp.getRootLayout().setCenter(loader.load());
			
			ListClientViewController controller = loader.getController();
			controller.setRootViewController(this);
			controller.listAllClients();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void openClient(Client client) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/client/ClientView.fxml"));
			Pane pane = loader.load();
			
			Stage dialogStage = new Stage();
		    dialogStage.setTitle(client.getName());
		    dialogStage.initModality(Modality.NONE);
		    dialogStage.initOwner(mainApp.getPrimaryStage());
		    dialogStage.initStyle(StageStyle.DECORATED);
		    dialogStage.setResizable(false);
		    
		    Scene scene = new Scene(pane);
		    dialogStage.setScene(scene);
		    
		    ClientViewController controller = loader.getController();
		    controller.setRootViewController(this);
		    controller.setClient(client);
		    controller.setUser(mainApp.getUser());
		    
		    dialogStage.show();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void openEditClient(Client client) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/editclient/EditClientView.fxml"));
			Pane pane = loader.load();
			
			Stage dialogStage = new Stage();
		    dialogStage.setTitle(String.join(" ", "Editando", client.getName()));
		    dialogStage.initModality(Modality.APPLICATION_MODAL);
		    dialogStage.initOwner(mainApp.getPrimaryStage());
		    dialogStage.initStyle(StageStyle.DECORATED);
		    dialogStage.setResizable(false);
		    
		    Scene scene = new Scene(pane);
		    dialogStage.setScene(scene);
		    
		    EditClientViewController controller = loader.getController();
		    controller.setClient(client);
		    
		    dialogStage.show();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	/********************************************************************
	 * Boletos
	 ********************************************************************/
	
	@FXML
	public void bill_listAction() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/list/bill/ListBillView.fxml"));
			VBox pane = loader.load();
			mainApp.getRootLayout().setCenter(pane);
			listBillViewController = loader.getController();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@FXML
	public void bill_openRemessaAction() {
		listBillViewController.processRemessa();
	}
	
	@FXML
	public void bill_openRetornoAction() {
		listBillViewController.processRetorno();
	}
	
	@FXML
	public void bill_setPagoAction() {
		listBillViewController.updateBillStatus(BillStatus.PAGO);
	}
	
	@FXML
	public void bill_setCartorioAction() {
		listBillViewController.updateBillStatus(BillStatus.CARTORIO);
	}
	
	@FXML
	public void bill_setProtestoAction() {
		listBillViewController.updateBillStatus(BillStatus.PROTESTADO);
	}
	
	/********************************************************************
	 * Cielo
	 ********************************************************************/
	
	@FXML
	public void cielo_listCardAction() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/list/cielocard/ListCieloCardView.fxml"));
			StackPane pane = loader.load();
			listCardViewController = loader.getController();
			mainApp.getRootLayout().setCenter(pane);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@FXML
	public void showNewPurchaseOrders() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/neworder/NewOrderView.fxml"));
//			AnchorPane pane = (AnchorPane) loader.load();
			
			mainApp.getRootLayout().setCenter(loader.load());
			
			NewOrderViewController controller = loader.getController();
			controller.setMainApp(mainApp);
//			this.newOrderViewController = loader.getController();
//			this.newOrderViewController.setMainApp(this);		
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	/********************************************************************
	 * PagSeguro
	 ********************************************************************/
	
	@FXML
	public void psm_listCardAction() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/list/psmcard/ListPSMCardView.fxml"));
			StackPane pane = loader.load();
			listCardViewController = loader.getController();
			mainApp.getRootLayout().setCenter(pane);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@FXML
	public void psm_searchCardAction() {
		System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());
	}

    /********************************************************************
     * Card
     ********************************************************************/

    @FXML
    public void card_openExtratoAction() {
        listCardViewController.processExtrato();
    }

    @FXML
    public void card_setCardOkAction() {
        listCardViewController.setCardOk();
    }

    @FXML
    public void card_deleteCardAction() {
        listCardViewController.deleteCard();
    }
	
	/*
	 * Estoque
	 */
	@FXML
	public void orcamentoButtonAction() {
		showOrcamentoView(null);
	}
	
	@FXML
	public void listOrcamentoButtonAction() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/listorcamento/ListOrcamentoView.fxml"));
			mainApp.getRootLayout().setCenter(loader.load());
			
			ListOrcamentoViewController controller = loader.getController();
			controller.setRootViewController(this);
//			controller.setMainApp(mainApp);
			
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@FXML
	public void showListPapel() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/listpapel/ListPapelView.fxml"));
			mainApp.getRootLayout().setCenter(loader.load());
			
			ListPapelViewController controller = loader.getController();
			controller.setRootViewController(this);
//			controller.setMainApp(mainApp);
			
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void showOrcamentoView(Orcamento orcamento) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/orcamento/OrcamentoView.fxml"));
			Pane pane = loader.load();
			
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Novo Orçamento");
		    dialogStage.initModality(Modality.NONE);
		    dialogStage.initOwner(mainApp.getPrimaryStage());
		    dialogStage.initStyle(StageStyle.DECORATED);
		    dialogStage.setResizable(false);
		    dialogStage.setOnCloseRequest(ev -> showListPapel());
		    
		    Scene scene = new Scene(pane);
//		    scene.getStylesheets().add(MainApp.class.getResource("/resources/gui.css").toExternalForm());
		    dialogStage.setScene(scene);

		    OrcamentoViewController controller = loader.getController();
		    controller.setMainApp(mainApp);
		    
		    if (orcamento != null) 
		    	controller.setOrcamento(orcamento);
		    
		    dialogStage.setOnCloseRequest(ev -> controller.cancelButtonAction(null));
		    dialogStage.show();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@FXML
	public void showListPapelTipo() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/listpapeltipo/ListPapelTipoView.fxml"));
			Pane pane = loader.load();
			
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Tabela Preço Kg");
		    dialogStage.initModality(Modality.NONE);
		    dialogStage.initOwner(mainApp.getPrimaryStage());
		    dialogStage.initStyle(StageStyle.DECORATED);
		    dialogStage.setResizable(false);
		    dialogStage.setOnCloseRequest(ev -> showListPapel());
		    
		    Scene scene = new Scene(pane, 390.0, 600.0);
		    dialogStage.setScene(scene);
		    
		    ListPapelTipoViewController controller = loader.getController();
		    controller.setUser(mainApp.getUser());
		    
		    dialogStage.show();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@FXML
	public void newPapelButtonAction() {
		showEditPapelView(null);
	}
	
	public void showEditPapelView(Papel papel) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/editpapel/EditPapelView.fxml"));
			Pane pane = loader.load();
			
			Stage dialogStage = new Stage();
		    dialogStage.initModality(Modality.NONE);
		    dialogStage.initOwner(mainApp.getPrimaryStage());
		    dialogStage.initStyle(StageStyle.DECORATED);
		    dialogStage.setResizable(false);
		    dialogStage.setOnCloseRequest(ev -> showListPapel());
		    
		    Scene scene = new Scene(pane);
//		    scene.getStylesheets().add(MainApp.class.getResource("/resources/gui.css").toExternalForm());
		    dialogStage.setScene(scene);
		    
		    EditPapelViewController controller = loader.getController();
	    	controller.setPapel(papel);
		    controller.setUser(mainApp.getUser());
	    	
		    if (papel != null)
		    	dialogStage.setTitle(papel.toString());
		    else 
		    	dialogStage.setTitle("Novo Papel");
		    
		    dialogStage.show();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	/********************************************************************
	 * Estoque
	 ********************************************************************/
	
	private ListProductViewController listProduct() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(MainApp.class.getResource("view/list/product/ListProductView.fxml"));
		mainApp.getRootLayout().setCenter(loader.load());

		return loader.getController();
	}
	
	@FXML
	public void stock_listQuimicoAction() {
		try {
			listProduct().listQuimico();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void stock_listTintaAction() {
		try {
			listProduct().listTinta();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void stock_listChapaAction() {
		try {
			listProduct().listChapa();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void stock_listEnvelopeAction() {
		try {
			listProduct().listEnvelope();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void stock_listBlanquetaAction() {
		try {
			listProduct().listBlanqueta();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void stock_listDiversosAction() {
		try {
			listProduct().listDiversos();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Getters
	 */
	public MainApp getMainApp() {
		return this.mainApp;
	}
	
}
