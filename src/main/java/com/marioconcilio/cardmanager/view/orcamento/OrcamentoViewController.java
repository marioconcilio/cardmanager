package com.marioconcilio.cardmanager.view.orcamento;

import com.marioconcilio.cardmanager.MainApp;
import com.marioconcilio.cardmanager.handler.LimitCharactersHandler;
import com.marioconcilio.cardmanager.handler.LimitCharactersHandler.LimitCharactersType;
import com.marioconcilio.cardmanager.model.dao.ClientDAO;
import com.marioconcilio.cardmanager.model.dao.OrcamentoDAO;
import com.marioconcilio.cardmanager.model.dao.OrcamentoPapelDAO;
import com.marioconcilio.cardmanager.model.dao.PapelDAO;
import com.marioconcilio.cardmanager.model.vo.Client;
import com.marioconcilio.cardmanager.model.vo.Orcamento;
import com.marioconcilio.cardmanager.model.vo.OrcamentoPapel;
import com.marioconcilio.cardmanager.model.vo.Papel;
import com.marioconcilio.cardmanager.report.OrcamentoReport;
import com.marioconcilio.cardmanager.util.Constants;
import com.marioconcilio.cardmanager.util.Formatter;
import com.marioconcilio.cardmanager.view.AlertFactory;
import com.marioconcilio.cardmanager.view.list.client.ListClientViewController;
import com.marioconcilio.cardmanager.view.listpapel.ListPapelViewController;
import com.marioconcilio.cardmanager.view.wait.WaitStage;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class OrcamentoViewController {
	
	protected static final Logger logger = LogManager.getLogger();
	
	@FXML
	private Label totalLabel;
	
	@FXML
	private Label dateLabel;
	
	@FXML
	private TextField cnpjTextField;
	
	@FXML
	private TextField nameTextField;
	
	@FXML
	private TextField addressTextField;
	
	@FXML
	private TextField cityTextField;
	
	@FXML
	private TextField cepTextField;
	
	@FXML
	private ComboBox<String> stateComboBox;
	
	@FXML
	private TableView<OrcamentoPapel> tableView;
	
	@FXML
	private TableColumn<OrcamentoPapel, Number> qtdeColumn;
	
	@FXML
	private TableColumn<OrcamentoPapel, Papel> produtoColumn;
	
	@FXML
	private TableColumn<OrcamentoPapel, BigDecimal> precoUnColumn;
	
	@FXML
	private TableColumn<OrcamentoPapel, BigDecimal> subtotalColumn;
	
	@FXML
	private Button saveButton;
	
	@FXML
	private Button cancelButton;
	
	@FXML
	private Button addPapelButton;
	
	@FXML
	private Button cnpjButton;
	
	@FXML
	private Button nomeButton;
	
	@FXML
	private Button listClientButton;
	
	@FXML
	private GridPane gridPane;
	
	private MainApp mainApp;
	private Client client;
	private Stage listPapelStage;
	private Orcamento orcamento;
	
	@FXML
	public void initialize() {
		logger.info("New Orcamento");
		
		/*
		 * Extractor
		 * This will cause the list to fire update notifications 
		 * (thereby labeling it as invalid) 
		 * when any of the specified properties belonging to the elements change.
		 */
		ObservableList<OrcamentoPapel> list = FXCollections.observableArrayList(op -> new Observable[] {op.subtotalProperty()});
		tableView.setItems(list);
		
		tableView.setPlaceholder(new Label(""));
		
		qtdeColumn.setCellValueFactory(op -> op.getValue().qtdeProperty());
		produtoColumn.setCellValueFactory(op -> op.getValue().papelProperty());
		precoUnColumn.setCellValueFactory(op -> op.getValue().precoUnProperty());
		subtotalColumn.setCellValueFactory(op -> op.getValue().subtotalProperty());
		
		precoUnColumn.setCellFactory(column -> {
			return new TableCell<OrcamentoPapel, BigDecimal>() {
				@Override
				protected void updateItem(BigDecimal item, boolean empty) {
					super.updateItem(item, empty);
					
					if (item == null || empty)
						setText(null);
					else 
						setText(Formatter.formatCurrency(item));
				}
			};
		});
		
		subtotalColumn.setCellFactory(column -> {
			return new TableCell<OrcamentoPapel, BigDecimal>() {
				@Override
				protected void updateItem(BigDecimal item, boolean empty) {
					super.updateItem(item, empty);
					
					if (item == null || empty)
						setText(null);
					else 
						setText(Formatter.formatCurrency(item));
				}
			};
		});
		
		qtdeColumn.setStyle("-fx-alignment: CENTER;");
		precoUnColumn.setStyle("-fx-alignment: CENTER-RIGHT;");
		subtotalColumn.setStyle("-fx-alignment: CENTER-RIGHT;");
		
		qtdeColumn.setMinWidth(60.0);
		qtdeColumn.setMaxWidth(60.0);
		
		precoUnColumn.setMinWidth(110.0);
		precoUnColumn.setMaxWidth(110.0);
		
		subtotalColumn.setMinWidth(110.0);
		subtotalColumn.setMaxWidth(110.0);
		
		tableView.setRowFactory(table -> {
			final TableRow<OrcamentoPapel> row = new TableRow<>();
			final ContextMenu rowMenu = new ContextMenu();
			
			MenuItem editItem = new MenuItem("Editar");
			editItem.setOnAction(event -> {
				OrcamentoPapel op = row.getItem();
				createDialogPane("Editar Papel", op, OrcamentoType.EDIT);
			});
			
			MenuItem deleteItem = new MenuItem("Remover");
			deleteItem.setOnAction(event -> {
				OrcamentoPapel op = row.getItem();
				Papel papel = op.getPapel();
				long q = papel.getQtde() + op.getQtde();
				papel.setQtde(q);
				
				PapelDAO dao = new PapelDAO();
				dao.update(papel);
				
				table.getItems().remove(row.getItem());
			});
			
			rowMenu.getItems().addAll(editItem, deleteItem);
			
			row.contextMenuProperty().bind(
					Bindings.when(Bindings.isNotNull(row.itemProperty()))
					.then(rowMenu)
					.otherwise((ContextMenu) null));
			
			return row;
		});
		
		ObjectBinding<BigDecimal> total = Bindings.createObjectBinding(() -> {
			BigDecimal t = tableView.getItems().stream()
					.map(op -> op.getPrecoUn().multiply(new BigDecimal(op.getQtde())))
					.reduce(BigDecimal.ZERO, BigDecimal::add);
			
			return t;
		}, 
		tableView.getItems());
		
		StringBinding totalFormatted = Bindings.createStringBinding(() -> Formatter.formatCurrency(total.get()), total);
		totalLabel.textProperty().bind(totalFormatted);
		
		saveButton.disableProperty().bind(Bindings.isEmpty(tableView.getItems()));
		
		cnpjTextField.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.ENTER)
				cnpjSearchButtonAction(null);
		});
		
		nameTextField.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.ENTER)
				nameSearchButtonAction(null);
		});
		
		dateLabel.setText(Formatter.formatDate(new Date()));
		stateComboBox.setItems(FXCollections.observableArrayList("AC","AL","AP","AM","BA","CE","DF","ES","GO","MA","MT","MS","MG","PA","PB","PR","PE","PI","RJ","RN","RS","RO","RR","SC","SP","SE","TO"));
	}
	
	@FXML
	private void cnpjSearchButtonAction(ActionEvent event) {
		String cnpj = cnpjTextField.getText();
		if (cnpj.length() == 0) return;
		
		ClientDAO dao = new ClientDAO();
		Client client = dao.getClientFromCnpj(Long.parseLong(cnpj));
		fillClientInfo(client);
	}
	
	@FXML
	private void nameSearchButtonAction(ActionEvent event) {
		String name = nameTextField.getText();
		if (name.length() == 0) return;
		
		ClientDAO dao = new ClientDAO();
		List<Client> clients = dao.fetchClient(name);
		
		switch (clients.size()) {
			case 0:
				return;
			case 1:
				fillClientInfo(clients.get(0));
				break;
			default:
				openListClient(clients);
		}
		
	}
	
	@FXML
	private void addPapelButtonAction(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/listpapel/ListPapelView.fxml"));
			BorderPane pane = (BorderPane) loader.load();
			
			Stage dialogStage = new Stage();
		    dialogStage.initModality(Modality.NONE);
		    dialogStage.initOwner(mainApp.getPrimaryStage());
		    dialogStage.initStyle(StageStyle.DECORATED);
		    dialogStage.setResizable(true);
		    this.listPapelStage = dialogStage;
		    
		    Scene scene = new Scene(pane, 700, 600);
		    scene.getStylesheets().add(MainApp.class.getResource("/resources/gui.css").toExternalForm());
		    dialogStage.setScene(scene);
		    
		    ListPapelViewController controller = loader.getController();
		    
		    controller.getPapel(papel -> {
		    	ObservableList<OrcamentoPapel> list = tableView.getItems();
				
				if (papel.getQtde() == 0) return;
				
				for (OrcamentoPapel op : list) {
					Papel p = op.getPapel();
					
					if (papel.toString().equals(p.toString())) {
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle(Constants.APP_NAME);
						alert.setHeaderText("Papel já incluso no pedido");
						alert.setContentText("Para alterar a quantidade e/ou desconto, clique com o botão direito em seguida Editar.");
						alert.showAndWait();
						
						return;
					}
				}

				addPapel(papel);
		    });
		    
		    dialogStage.show();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@FXML
	private void listClientButtonAction(ActionEvent event) {
		openListClient(null);
	}
	
	@FXML
	public void cancelButtonAction(ActionEvent event) {
		logger.info("Cancelling Orcamento. Returning Papel");
		
		ObservableList<OrcamentoPapel> list = tableView.getItems();
		if (!list.isEmpty()) {
			PapelDAO dao = new PapelDAO();
			
			list.forEach(op -> {
				Papel papel = op.getPapel();
				long q = papel.getQtde() + op.getQtde();
				papel.setQtde(q);
				dao.update(papel);
			});
		}
		
		logger.info("Orcamento cancelled");
		Stage scene = (Stage) tableView.getScene().getWindow();
		scene.close();
	}
	
	@FXML
	private void saveButtonAction(ActionEvent event) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmar Orçamento");
		alert.setHeaderText("Confirmar Orçamento");
		alert.setContentText("Are you ok with this?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
		    saveOrcamento();
		}

	}
	
	private void saveOrcamento() {
		// if is new client
		if (client == null) {
			client = new Client();
			client.setId(Formatter.parseCnpj(cnpjTextField.getText()));
			client.setName(nameTextField.getText());
			client.setAddress(addressTextField.getText());
			client.setCity(cityTextField.getText());
			client.setState(stateComboBox.getSelectionModel().getSelectedItem());
			client.setCep(Formatter.parseCep(cepTextField.getText()));
			client.setCreditLimit(2000);

			final ClientDAO dao = new ClientDAO();
			try {
				dao.save(client);
			} 
			catch (Exception e) {
				AlertFactory.showError(e, "Não foi possível adicionar novo cliente.");
			}
		}

		orcamento = new Orcamento();
		orcamento.setData(new Date());
		orcamento.setClient(client);
		orcamento.setTotal(Formatter.parseCurrency(totalLabel.getText()));
		orcamento.setOrcamentoPapels(tableView.getItems());
		new OrcamentoDAO().save(orcamento);

		WaitStage wait = new WaitStage(mainApp.getPrimaryStage(), "Gerando Orçamento");
		wait.show();

		Stage s = (Stage) tableView.getScene().getWindow();
		s.close();

		OrcamentoReport report = new OrcamentoReport(orcamento);
		report.generate(file -> {
			wait.close();
		});
	}
	
	private void openListClient(List<Client> clients) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/listclient/ListClientView.fxml"));
			Pane pane = (Pane) loader.load();
			
			Stage dialogStage = new Stage();
		    dialogStage.initModality(Modality.NONE);
		    dialogStage.initOwner(mainApp.getPrimaryStage());
		    dialogStage.initStyle(StageStyle.DECORATED);
		    dialogStage.setResizable(true);
		    
		    Scene scene = new Scene(pane, 850, 600);
		    scene.getStylesheets().add(MainApp.class.getResource("/resources/gui.css").toExternalForm());
		    dialogStage.setScene(scene);
		    
		    ListClientViewController controller = loader.getController();
		    controller.setClients(clients, this);		    
		    dialogStage.show();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void fillClientInfo(Client client) {
		this.client = client;
		if (client == null) return;
		
		cnpjTextField.setText(Formatter.formatCnpj(client.getId()));
		nameTextField.setText(client.getName());
		addressTextField.setText(client.getAddress());
		cityTextField.setText(client.getCity());
		stateComboBox.getSelectionModel().select(client.getState());
		cepTextField.setText(Formatter.formatCep(client.getCep()));
	}
	
	public void clearClientInfo() {
		this.client = null;
		
		cnpjTextField.setText("");
		nameTextField.setText("");
		addressTextField.setText("");
		cityTextField.setText("");
		stateComboBox.getSelectionModel().select("");
		cepTextField.setText("");
	}
	
	private void addPapel(Papel papel) {
		BigDecimal preco = papel.getPeso().multiply(papel.getPapelTipo().getPrecoKg());
		OrcamentoPapel op = new OrcamentoPapel();
		op.setPapel(papel);
		op.setPrecoUn(preco);
		createDialogPane("Adicionar Papel", op, OrcamentoType.ADD);
	}
	
	private void createDialogPane(String title, OrcamentoPapel op, OrcamentoType type) {
		Dialog<Pair<String, String>> dialog = new Dialog<>();
		dialog.setTitle(title);
		dialog.setHeaderText(title);
		dialog.setGraphic(new ImageView(this.getClass().getResource("/resources/icons/paper48.png").toString()));
		
		// Set the button types.
		dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

		// Create labels and fields.
		GridPane grid = new GridPane();
		grid.setHgap(5);
		grid.setVgap(5);
		grid.setPadding(new Insets(20, 75, 10, 10));
		
		TextField qtdeTextField = new TextField();
		qtdeTextField.setPromptText("Quantidade");
		TextField precoUnTextField = new TextField();
		precoUnTextField.setPromptText("Preço Unitário");
		
		qtdeTextField.addEventFilter(KeyEvent.KEY_TYPED, new LimitCharactersHandler(100, LimitCharactersType.ONLY_NUMBERS));
		precoUnTextField.addEventFilter(KeyEvent.KEY_TYPED, new LimitCharactersHandler(100, LimitCharactersType.CURRENCY));
		
		grid.add(new Label("Quantidade"), 0, 0);
		grid.add(qtdeTextField, 1, 0);
		grid.add(new Label("Preço"), 0, 1);
		grid.add(precoUnTextField, 1, 1);
		
		// Validate qtdeProperty field and enable/disable ok button
		Node okButton = dialog.getDialogPane().lookupButton(ButtonType.OK);
//		okButton.setDisable(true);
		
		// Bindings
		LongProperty qtdeProperty = new SimpleLongProperty(op.getQtde());
		DoubleProperty precoProperty = new SimpleDoubleProperty(op.getPrecoUn().doubleValue());
		
		qtdeTextField.textProperty().addListener((observalbe, oldValue, newValue) -> {
//			okButton.setDisable(newValue.trim().isEmpty());
			
			try {
				qtdeProperty.set(Long.parseLong(newValue));
			}
			catch (NumberFormatException ex) {
				qtdeProperty.set(0L);
			}
		});
		
		precoUnTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			try {
				precoProperty.set(Double.parseDouble(newValue.replace("R$ ", "").replace(",", ".")));
			}
			catch (NumberFormatException ex) {
				precoProperty.set(0.0);
			}
		});
		
		precoUnTextField.focusedProperty().addListener((observable, lostFocus, gainedFocus) -> {
			// gained focus -> 1234,56
			if (gainedFocus) {
				try {
					String oldValue = precoUnTextField.getText();
					String newValue = Formatter.parseCurrency(oldValue).toString().replace(".", ",");
					precoUnTextField.setText(newValue);
				}
				catch (NumberFormatException ex) {}
			}
			
			// lost focus -> R$ 1234,56
			if (lostFocus) {
				try {
					String oldValue = precoUnTextField.getText().replaceAll(",", ".");
					String newValue = Formatter.formatCurrency(new BigDecimal(oldValue));
					precoUnTextField.setText(newValue);
					
					precoProperty.set(Double.parseDouble(oldValue));
				}
				catch (NumberFormatException ex) {
					precoProperty.set(0.0);
				}
			}
		});
		
		BooleanBinding validQtde = Bindings.greaterThan(qtdeProperty, 0)
				.and(Bindings.lessThanOrEqual(qtdeProperty, op.getPapel().getQtde() + op.getQtde()));
		
		double minPreco = op.getPapel().getPeso()
				.multiply(op.getPapel().getPapelTipo().getPrecoKg())
				.multiply(new BigDecimal("0.85"))
				.doubleValue();
		
		BooleanBinding validPreco = Bindings.greaterThanOrEqual(precoProperty, minPreco);
		
		okButton.disableProperty().bind(Bindings.not(validPreco.and(validQtde)));
		
		VBox vbox = new VBox();
		vbox.setPadding(new Insets(15, 10, 10, 10));
		vbox.getChildren().addAll(new Label(op.getPapel().toString()), grid);
		
		dialog.getDialogPane().setContent(vbox);
		
		// After all set
		Platform.runLater(() -> {
			qtdeTextField.requestFocus();
			
			if (op.getQtde() > 0)
				qtdeTextField.setText(Long.toString(op.getQtde()));
			
			String preco = Formatter.formatCurrency(op.getPrecoUn());
			precoUnTextField.setText(preco);
		});
		
		// Convert the result to a qtdeProperty-precoUn-pair when ok button is clicked.
		dialog.setResultConverter(dialogButton -> {
		    if (dialogButton == ButtonType.OK) {
		        return new Pair<>(qtdeTextField.getText(), precoUnTextField.getText());
		    }
		    
		    return null;
		});
		
		Optional<Pair<String, String>> result = dialog.showAndWait();

		result.ifPresent(qtdePrecoUn -> {
			BigDecimal preco = Formatter.parseCurrency(qtdePrecoUn.getValue());
			op.setPrecoUn(preco);
			
			if (type == OrcamentoType.ADD) {
				op.setQtde(Long.parseLong(qtdePrecoUn.getKey()));
				
				Papel papel = op.getPapel();
				long q = papel.getQtde() - op.getQtde();
				papel.setQtde(q);
				
				PapelDAO dao = new PapelDAO();
				dao.update(papel);
				
				tableView.getItems().add(op);
			}
			else {
				Papel papel = op.getPapel();
				long newQtde = Long.parseLong(qtdePrecoUn.getKey());
				long q = op.getQtde() + papel.getQtde() - newQtde;
				papel.setQtde(q);
				
				PapelDAO dao = new PapelDAO();
				dao.update(papel);
				
				op.setQtde(newQtde);
			}
			
			logger.info("Added {}pct {}", op.getQtde(), op.getPapel());
			listPapelStage.close();
		});
	}

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}
	
	public void setOrcamento(Orcamento orcamento) {
		fillClientInfo(orcamento.getClient());
		
		OrcamentoPapelDAO dao = new OrcamentoPapelDAO();
		dao.listAllFromOrcamento(orcamento, list -> {
			tableView.setItems(list);
		});
		
		/*
		 * reformat view
		 */
		ImageView printView = new ImageView(new Image(getClass().getResourceAsStream("/resources/icons/print-48.png")));
		printView.setFitWidth(25.0);
		printView.setFitHeight(25.0);
		
		ImageView okView = new ImageView(new Image(getClass().getResourceAsStream("/resources/icons/ok.png")));
		okView.setFitWidth(25.0);
		okView.setFitHeight(25.0);
		
		addPapelButton.setText("Reimprimir");
		addPapelButton.setGraphic(printView);
		saveButton.setText("OK");
		saveButton.setGraphic(okView);
		tableView.setDisable(true);
		tableView.setStyle("-fx-opacity: 1.0;");
		stateComboBox.setDisable(true);
		stateComboBox.setStyle("-fx-opacity: 1.0;");
		
		Set<Node> nodes = gridPane.lookupAll(".text-field");
		for (Node n : nodes) {
			n.setDisable(true);
			n.setStyle("-fx-opacity: 1.0;");
		}
		
		cnpjButton.setVisible(false);
		nomeButton.setVisible(false);
		listClientButton.setVisible(false);
		cancelButton.setVisible(false);
		
		dateLabel.setText(Formatter.formatDate(orcamento.getData()));
		totalLabel.textProperty().unbind();
		totalLabel.setText(Formatter.formatCurrency(orcamento.getTotal()));
		
		saveButton.disableProperty().unbind();
		saveButton.setDisable(false);
		saveButton.setOnAction(ev -> {
			Stage scene = (Stage) tableView.getScene().getWindow();
			scene.close();
		});
		
		addPapelButton.setOnAction(ev -> {
			Parent parent = tableView.getScene().getRoot();
			WaitStage wait = new WaitStage(mainApp.getPrimaryStage(), "Reimprimindo Orçamento", parent);
			wait.show();
			
			OrcamentoReport report = new OrcamentoReport(orcamento);
			report.generate(file -> {
				wait.close();
			});
		});
		
	}
	
	enum OrcamentoType {
		ADD,
		EDIT
	}
}
