package com.marioconcilio.cardmanager.handler;

import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

public class LimitCharactersHandler implements EventHandler<KeyEvent> {
	
	public enum LimitCharactersType {
		ACCEPT_ALL("."),
		ONLY_NUMBERS("[0-9]"),
		ONLY_NON_NUMBERS("[^0-9]"),
		CEP("[0-9-]"),
		CURRENCY("[0-9,.]");
		
		private String regex;
		
		LimitCharactersType(String regex) {
			this.regex = regex;
		}
		
		public String getRegex() {
			return this.regex;
		}
	}
	
	private int maxChar;
	private LimitCharactersType type;
	
	public LimitCharactersHandler(int maxChar) {
		this.maxChar = maxChar;
		this.type = LimitCharactersType.ACCEPT_ALL;
	}
	
	public LimitCharactersHandler(int maxChar, LimitCharactersType type) {
		this.maxChar = maxChar;
		this.type = type;
	}

	@Override
	public void handle(KeyEvent event) {
		TextField tx = (TextField) event.getSource();
		if (tx.getText().length() >= this.maxChar) {
    		event.consume();
    	}
		
		if (!event.getCharacter().matches(this.type.getRegex())) {
			event.consume();
		}
	}

}
