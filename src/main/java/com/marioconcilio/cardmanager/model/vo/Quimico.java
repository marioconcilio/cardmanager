package com.marioconcilio.cardmanager.model.vo;

import com.marioconcilio.cardmanager.model.dao.DolarDAO;
import com.marioconcilio.cardmanager.util.Formatter;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

@Entity
@Table(name="quimico", schema="public")
public class Quimico extends AbstractProduct {
	
	private final StringProperty nome = new SimpleStringProperty(this, "nome");
	private final StringProperty marca = new SimpleStringProperty(this, "marca");
	private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<>(this, "preco");
	
	/*
	 * Integer ID
	 */
	@Override
	@Id
	@Column(name="id_quimico")
	public long getId() {
		return this.id.get();
	}
	
	/*
	 * String Nome
	 */
	@Column(name="nome")
	public String getNome() {
		return this.nome.get();
	}
	
	public void setNome(String nome) {
		this.nome.set(nome);
	}
	
	public StringProperty nomeProperty() {
		return this.nome;
	}
	
	/*
	 * String Marca
	 */
	@Column(name="marca")
	public String getMarca() {
		return this.marca.get();
	}
	
	public void setMarca(String marca) {
		this.marca.set(marca);
	}
	
	public StringProperty marcaProperty() {
		return this.marca;
	}

	/*
	 * BigDecimal Preco
	 */
	@Column(name="preco")
	public BigDecimal getPreco() {
		return this.preco.get();
	}
	
	public void setPreco(BigDecimal preco) {
		this.preco.set(preco);
	}
	
	public ObjectProperty<BigDecimal> precoProperty() {
		return this.preco;
	}
	
	/*
	 * String Un
	 */
	@Column(name="un")
	public String getUn() {
		return this.unProperty.get();
	}
	
	public void setUn(String un) {
		this.unProperty.set(un);
	}
	
	@Override
	public StringProperty unProperty() {
		return this.unProperty;
	}
	
	@Override
	public BigDecimal precoUn() {
		if (this.precoUn == null) {
			if (getCurrency().equals(Currency.USD)) {
				Dolar dolar = new DolarDAO().get();
				this.precoUn = getPreco().multiply(dolar.getValue());
			}
			else {
				this.precoUn = getPreco();
			}
			
			this.precoUn.setScale(2, RoundingMode.HALF_UP);
		}

		return this.precoUn;
	}
	
	@Override
    public StringProperty descriptionProperty() {
		if (this.descProperty.get() == null) {
			this.descProperty.set(getNome());
		}
		
		return this.descProperty;
	}
	
	@Override
    public StringProperty detailProperty() {
		if (this.detailProperty.get() == null) {
			this.detailProperty.set(getMarca());
		}
		
		return this.detailProperty;
	}
	
	@Override
	public String toString() {
		return String.format("%-40s %-25s %-12s %-15s", 
				getNome(), 
				getMarca(),
				Formatter.formatCurrency(precoUn()),
				getUn());
	}
	
	@Override
    public boolean equals(Object o) {
    	if (o == this) return true;
    	if (!(o instanceof Quimico)) {
            return false;
        }
    	
    	Quimico q = (Quimico) o;
    	return Objects.equals(getNome(), q.getNome()) &&
    			Objects.equals(getMarca(), q.getMarca()) &&
    			Objects.equals(getUn(), q.getUn());
    }
    
    @Override
    public int hashCode() {
    	return Objects.hash(getNome(), getMarca(), getUn());
    }

}
