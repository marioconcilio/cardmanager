package com.marioconcilio.cardmanager.model.vo;

import com.marioconcilio.cardmanager.util.Formatter;
import javafx.beans.property.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name="psm_card", schema="public")
public class PSMCard extends AbstractVO implements Card {

	private static final long serialVersionUID = 1L;

	private final ObjectProperty<UUID> uuid = new SimpleObjectProperty<>(this, "uuid");
	private final StringProperty description = new SimpleStringProperty(this, "description");
	private final IntegerProperty shares = new SimpleIntegerProperty(this, "shares");
	private final ObjectProperty<BigDecimal> value = new SimpleObjectProperty<>(this, "value");
	private final ObjectProperty<Date> date = new SimpleObjectProperty<>(this, "date");
	private final ObjectProperty<CardType> cardType = new SimpleObjectProperty<>(this, "cardType");
	private final BooleanProperty ok = new SimpleBooleanProperty(this, "ok");
	
	public PSMCard() {
		
	}
	
	public PSMCard(Date date, PSMCardType type, BigDecimal value) {
		this.date.set(date);
		this.cardType.set(type);
		this.value.set(value);
	}
	
	/*
	 * Integer ID
	 */
	
	@Override
	@Transient
	public long getId() {
		return this.id.get();
	}
	
	/*
	 * UUID
	 */
	@Id
	@Column(name="uuid_psmcard")
	@org.hibernate.annotations.Type(type="pg-uuid")
	public UUID getUUID() {
		return this.uuid.get();
	}
	
	public void setUUID(UUID uuid) {
		this.uuid.set(uuid);
	}
	
	public ObjectProperty<UUID> uuidProperty() {
		return this.uuid;
	}
	
	/*
	 * String Description
	 */
	@Column(name="description")
	public String getDescription() {
		return this.description.get();
	}
	
	public void setDescription(String description) {
		this.description.set(description);
	}
	
	public StringProperty descriptionProperty() {
		return this.description;
	}
	
	/*
	 * Integer Shares
	 */
	@Column(name="shares")
	public Integer getShares() {
		return this.shares.get();
	}
	
	public void setShares(Integer shares) {
		this.shares.set(shares);
	}
	
	public IntegerProperty sharesProperty() {
		return this.shares;
	}
	
	/*
	 * BigDecimal Value
	 */
	@Column(name="value")
	public BigDecimal getValue() {
		return this.value.get();
	}
	
	public void setValue(BigDecimal value) {
		this.value.set(value);
	}
	
	public ObjectProperty<BigDecimal> valueProperty() {
		return this.value;
	}
	
	/*
	 * Date date
	 */
	@Column(name="date")
	public Date getDate() {
		return this.date.get();
	}
	
	public void setDate(Date date) {
		this.date.set(date);
//		this.date.set(Formatter.parseDate(date));
	}
	
	public ObjectProperty<Date> dateProperty() {
		return this.date;
	}
	
	/*
	 * PSMCardType cardType
	 */
	@Enumerated(EnumType.STRING)
	public PSMCardType getCardType() {
		return (PSMCardType) this.cardType.get();
	}
	
	public void setCardType(PSMCardType type) {
		this.cardType.set(type);
	}
	
	public ObjectProperty<CardType> cardTypeProperty() {
		return this.cardType;
	}
	
	/*
	 * Boolean OK
	 */
	@Column(name="is_ok")
	public boolean isOk() {
		return this.ok.get();
	}
	
	public void setOk(boolean ok) {
		this.ok.set(ok);
	}
	
	public BooleanProperty okProperty() {
		return this.ok;
	}
	
	@Override
	public String toString() {
		final String str = String.format("%s %-20s %-12s %s",
				Formatter.formatDate(this.getDate()),
				this.getCardType(),
				Formatter.formatCurrency(this.getValue()),
				(this.getCardType() == PSMCardType.CREDITO_PARCELADO)? "Parcelas: " + this.getShares() : "");
		
		return str.toString();
	}

}
