package com.marioconcilio.cardmanager.model.vo;

import com.marioconcilio.cardmanager.model.dao.DolarDAO;
import com.marioconcilio.cardmanager.util.Formatter;
import javafx.beans.property.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

/**
 * String nome<br>
 * String tipo<br>
 * Integer larg<br>
 * Integer comp<br>
 * Integer grs<br>
 * BigDecimal precoMilheiro<br>
 * @author Mario Concilio
 */
@Entity
@Table(name="envelope", schema="public")
public class Envelope extends AbstractProduct {
	
	private final StringProperty nome = new SimpleStringProperty(this, "nome");
	private final StringProperty tipo = new SimpleStringProperty(this, "tipo");
	private final IntegerProperty larg = new SimpleIntegerProperty(this, "larg");
	private final IntegerProperty comp = new SimpleIntegerProperty(this, "comp");
	private final IntegerProperty grs = new SimpleIntegerProperty(this, "grs");
	private final ObjectProperty<BigDecimal> precoMilheiro = new SimpleObjectProperty<>(this, "precoMilheiro");
	
	private Integer un;

	/*
	 * Integer ID
	 */
	@Override
	@Id
	@Column(name="id_envelope")
	public long getId() {
		return this.id.get();
	}
	
	/*
	 * String Nome
	 */
	@Column(name="nome")
	public String getNome() {
		return this.nome.get();
	}
	
	public void setNome(String nome) {
		this.nome.set(nome);
	}
	
	public StringProperty nomeProperty() {
		return this.nome;
	}
	
	/*
	 * String Tipo
	 */
	@Column(name="tipo")
	public String getTipo() {
		return this.tipo.get();
	}
	
	public void setTipo(String tipo) {
		this.tipo.set(tipo);
	}
	
	public StringProperty tipoProperty() {
		return this.tipo;
	}
	
	/*
	 * BigDecimal Preco
	 */
	@Column(name="preco_milheiro")
	public BigDecimal getPrecoMilheiro() {
		return this.precoMilheiro.get();
	}
	
	public void setPrecoMilheiro(BigDecimal precoMilheiro) {
		this.precoMilheiro.set(precoMilheiro);
	}
	
	public ObjectProperty<BigDecimal> precoMilheiroProperty() {
		return this.precoMilheiro;
	}
	
	/*
	 * Integer Larg
	 */
	@Column(name="larg")
	public Integer getLarg() {
		return this.larg.get();
	}
	
	public void setLarg(Integer larg) {
		this.larg.set(larg);
	}
	
	public IntegerProperty largProperty() {
		return this.larg;
	}
	
	/*
	 * Integer Comp
	 */
	@Column(name="comp")
	public Integer getComp() {
		return this.comp.get();
	}
	
	public void setComp(Integer comp) {
		this.comp.set(comp);
	}
	
	public IntegerProperty compProperty() {
		return this.comp;
	}
	
	/*
	 * Integer Grs
	 */
	@Column(name="grs")
	public Integer getGrs() {
		return this.grs.get();
	}
	
	public void setGrs(Integer grs) {
		this.grs.set(grs);
	}
	
	public IntegerProperty grsProperty() {
		return this.grs;
	}
	
	/*
	 * Integer Un
	 */
	@Column(name="un")
	public Integer getUn() {
		return this.un;
	}
	
	public void setUn(Integer un) {
		this.un = un;
	}
	
	@Override
	public StringProperty unProperty() {
		if (this.unProperty.get() == null) {
			this.unProperty.set(String.format("Cx c/ %d", this.un));
		}
		
		return this.unProperty;
	}
    
    @Override
    public BigDecimal precoUn() {
    	if (this.precoUn == null) {
    		final BigDecimal preco;
    		if (getCurrency().equals(Currency.USD)) {
    			Dolar dolar = new DolarDAO().get();
    			preco = getPrecoMilheiro().multiply(dolar.getValue());
    		}
    		else {
    			preco = getPrecoMilheiro();
    		}

    		this.precoUn = preco.multiply(BigDecimal.valueOf(getUn()))
    				.divide(BigDecimal.valueOf(1000L))
    				.setScale(1, RoundingMode.HALF_UP);
    	}

    	return this.precoUn;
    }
    
    @Override
    public StringProperty descriptionProperty() {
		if (this.descProperty.get() == null) {
			this.descProperty.set(String.format("%03dx%03d %dg",
					getLarg(),
					getComp(),
					getGrs()));
		}
		
		return this.descProperty;
	}
	
	@Override
    public StringProperty detailProperty() {
		if (this.detailProperty.get() == null) {
			if (getNome() == null) {
				this.detailProperty.set(getTipo());
			}
			else {
				this.detailProperty.set(String.join(" ", getNome(), getTipo()));
			}
		}
		
		return this.detailProperty;
	}
	
	@Override
	public String toString() {
		if (getNome() != null) {
			return String.format("%-20s %03dx%03d %dg\t %-20s %-12s Cx c/ %d",
					getNome(),
					getLarg(),
					getComp(),
					getGrs(),
					getTipo(), 
					Formatter.formatCurrency(precoUn()), 
					getUn());
		}
		
		return String.format("%03dx%03d %dg\t %-20s %-12s Cx c/ %d",
				getLarg(),
				getComp(),
				getGrs(),
				getTipo(), 
				Formatter.formatCurrency(precoUn()), 
				getUn());
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
    	if (!(o instanceof Envelope)) {
            return false;
        }
    	
    	Envelope e = (Envelope) o;
    	return getLarg() == e.getLarg() &&
    			getComp() == e.getComp() &&
    			getGrs() == e.getGrs() &&
    			Objects.equals(getTipo(), e.getTipo());
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(getLarg(), getComp(), getGrs(), getTipo());
	}
	
}
