package com.marioconcilio.cardmanager.model.vo;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Embeddable class for compound PK of Orcamento_Papel
 * <li>Long id_orcamento
 * <li>Long id_papel
 * 
 * @author Mario Concilio
 */
@Embeddable
public class OrcamentoPapelId implements Serializable {

	private static final long serialVersionUID = -302031559564307252L;
	private long idOrcamento;
	private long idPapel;

	/**
	 * Class to maintain id_orcamento and id_papel as primary key of Orcamento_Papel
	 */
	public OrcamentoPapelId() {
		
	}

	/**
	 * Class to maintain id_orcamento and id_papel as primary key of Orcamento_Papel
	 * @param idOrcamento
	 * @param idPapel
	 */
	public OrcamentoPapelId(long idOrcamento, long idPapel) {
		this.idOrcamento = idOrcamento;
		this.idPapel = idPapel;
	}
	
	public OrcamentoPapelId(Orcamento orcamento, Papel papel) {
		this.idOrcamento = orcamento.getId();
		this.idPapel = papel.getId();
	}

	@Column(name="id_orcamento", nullable=false)
	public long getIdOrcamento() {
		return this.idOrcamento;
	}

	public void setIdOrcamento(long idOrcamento) {
		this.idOrcamento = idOrcamento;
	}

	@Column(name="id_papel", nullable=false)
	public long getIdPapel() {
		return this.idPapel;
	}

	public void setIdPapel(long idPapel) {
		this.idPapel = idPapel;
	}

	@Override
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof OrcamentoPapelId))
			return false;
		OrcamentoPapelId castOther = (OrcamentoPapelId) other;

		return (this.getIdOrcamento() == castOther.getIdOrcamento())
				&& (this.getIdPapel() == castOther.getIdPapel());
	}

	@Override
	public int hashCode() {
		int result = 17;

		result = 37 * result + (int) this.getIdOrcamento();
		result = 37 * result + (int) this.getIdPapel();
		return result;
	}

}
