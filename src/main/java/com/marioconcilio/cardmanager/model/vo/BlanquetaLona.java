package com.marioconcilio.cardmanager.model.vo;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name="blanqueta_lona", schema="public")
public class BlanquetaLona extends AbstractVO {
	
	private final IntegerProperty lonas = new SimpleIntegerProperty(this, "lonas");
	private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<>(this, "preco");
	
	public BlanquetaLona() {
		
	}
	
	public BlanquetaLona(Integer lonas, BigDecimal preco) {
		this.lonas.set(lonas);
		this.preco.set(preco);
	}

	/*
	 * Integer ID
	 */
	@Override
	@Id
	@Column(name="id_blanqueta_lona")
	public long getId() {
		return this.id.get();
	}
	
	/*
	 * Integer Lonas
	 */
	@Column(name="lonas")
	public Integer getLonas() {
		return this.lonas.get();
	}
	
	public void setLonas(Integer lonas) {
		this.lonas.set(lonas);
	}
	
	public IntegerProperty lonasProperty() {
		return this.lonas;
	}
	
	/*
	 * BigDecimal Preco
	 */
	@Column(name="preco")
	public BigDecimal getPreco() {
		return this.preco.get();
	}
	
	public void setPreco(BigDecimal preco) {
		this.preco.set(preco);
	}
	
	public ObjectProperty<BigDecimal> precoProperty() {
		return this.preco;
	}
	
	@Override
	public String toString() {
		return String.format("%d lonas", getLonas());
	}
	
	@Override
    public boolean equals(Object o) {
    	if (o == this) return true;
    	if (!(o instanceof BlanquetaLona)) {
            return false;
        }
    	
    	BlanquetaLona bl = (BlanquetaLona) o;
    	return getLonas() == bl.getLonas();
    }
    
    @Override
    public int hashCode() {
    	return Objects.hash(getLonas());
    }

}
