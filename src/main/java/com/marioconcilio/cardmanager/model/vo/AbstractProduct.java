package com.marioconcilio.cardmanager.model.vo;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.math.BigDecimal;

@MappedSuperclass
public abstract class AbstractProduct extends AbstractVO implements Product {
	
	protected final StringProperty descProperty = new SimpleStringProperty(this, "descProperty");
	protected final StringProperty detailProperty = new SimpleStringProperty(this, "detailProperty");
	protected final StringProperty unProperty = new SimpleStringProperty(this, "unProperty");
	protected final IntegerProperty qtdeProperty = new SimpleIntegerProperty(this, "qtdeProperty");
	
	protected Currency currency;
	protected BigDecimal precoUn;
	
	/*
	 * Currency currency
	 */
	@Enumerated(EnumType.STRING)
	@Column(name="moeda")
	public Currency getCurrency() {
		return this.currency;
	}
	
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	/*
	 * Integer Qtde
	 */
	@Column(name="qtde")
	public Integer getQtde() {
		return this.qtdeProperty.get();
	}
	
	public void setQtde(Integer qtde) {
		this.qtdeProperty.set(qtde);
	}
	
	public IntegerProperty qtdeProperty() {
		return this.qtdeProperty;
	}

}
