package com.marioconcilio.cardmanager.model.vo;

import com.marioconcilio.cardmanager.model.dao.DolarDAO;
import com.marioconcilio.cardmanager.util.Formatter;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Objects;

@Entity
@Table(name="tinta", schema="public")
public class Tinta extends AbstractProduct {
	
	private final StringProperty nome = new SimpleStringProperty(this, "nome");
	private final StringProperty marca = new SimpleStringProperty(this, "marca");
	private final ObjectProperty<BigDecimal> precoKg = new SimpleObjectProperty<>(this, "preco_kg");
	private BigDecimal un;

	/*
	 * Integer ID
	 */
	@Override
	@Id
	@Column(name="id_tinta")
	public long getId() {
		return this.id.get();
	}

	/*
	 * String Nome
	 */
	@Column(name="nome")
	public String getNome() {
		return this.nome.get();
	}
	
	public void setNome(String nome) {
		this.nome.set(nome);
	}
	
	public StringProperty nomeProperty() {
		return this.nome;
	}
	
	/*
	 * String Marca
	 */
	@Column(name="marca")
	public String getMarca() {
		return this.marca.get();
	}
	
	public void setMarca(String marca) {
		this.marca.set(marca);
	}
	
	public StringProperty marcaProperty() {
		return this.marca;
	}

	/*
	 * BigDecimal Preco
	 */
	@Column(name="preco_kg")
	public BigDecimal getPrecoKg() {
		return this.precoKg.get();
	}
	
	public void setPrecoKg(BigDecimal precoKg) {
		this.precoKg.set(precoKg);
	}
	
	public ObjectProperty<BigDecimal> precoKgProperty() {
		return this.precoKg;
	}
	
	/*
	 * BigDecimal Un
	 */
	@Column(name="un")
	public BigDecimal getUn() {
		return this.un;
	}
	
	public void setUn(BigDecimal un) {
		this.un = un;
	}
	
	@Override
	public StringProperty unProperty() {
		if (this.unProperty.get() == null) {
			this.unProperty.set(new DecimalFormat("#0.0kg").format(this.un));
		}
		
		return this.unProperty;
	}
	
	@Override
	public BigDecimal precoUn() {
		if (this.precoUn == null) {
			final BigDecimal kg;
			if (getCurrency().equals(Currency.USD)) {
				Dolar dolar = new DolarDAO().get();
				kg = getPrecoKg().multiply(dolar.getValue());
			}
			else {
				kg = getPrecoKg();
			}

			this.precoUn = kg.multiply(getUn())
					.setScale(2, RoundingMode.HALF_UP);
		}

		return this.precoUn;
	}
	
	@Override
    public StringProperty descriptionProperty() {
		if (this.descProperty.get() == null) {
			this.descProperty.set(getNome());
		}
		
		return this.descProperty;
	}
	
	@Override
    public StringProperty detailProperty() {
		if (this.detailProperty.get() == null) {
			this.detailProperty.set(getMarca());
		}
		
		return this.detailProperty;
	}
	
	@Override
	public String toString() {
		return String.format("%-30s %-10s %-12s %-4skg", 
				getNome(), 
				getMarca(),
				Formatter.formatCurrency(precoUn()),
				getUn());
	}
	
	@Override
    public boolean equals(Object o) {
    	if (o == this) return true;
    	if (!(o instanceof Tinta)) {
            return false;
        }
    	
    	Tinta tinta = (Tinta) o;
    	return Objects.equals(getNome(), tinta.getNome()) &&
    			Objects.equals(getMarca(), tinta.getMarca());
    }
    
    @Override
    public int hashCode() {
    	return Objects.hash(getNome(), getMarca());
    }
	
}
