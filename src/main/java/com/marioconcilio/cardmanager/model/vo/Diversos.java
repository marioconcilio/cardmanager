package com.marioconcilio.cardmanager.model.vo;

import com.marioconcilio.cardmanager.model.dao.DolarDAO;
import com.marioconcilio.cardmanager.util.Formatter;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

@Entity
@Table(name="diversos", schema="public")
public class Diversos extends AbstractProduct {
	
	private final StringProperty nome = new SimpleStringProperty(this, "nome");
	private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<>(this, "preco");
	private final StringProperty un = new SimpleStringProperty(this, "un");

	/*
	 * Integer ID
	 */
	@Override
	@Id
	@Column(name="id_diversos")
	public long getId() {
		return this.id.get();
	}
	
	/*
	 * String Nome
	 */
	@Column(name="nome")
	public String getNome() {
		return this.nome.get();
	}
	
	public void setNome(String nome) {
		this.nome.set(nome);
	}
	
	public StringProperty nomeProperty() {
		return this.nome;
	}

	/*
	 * BigDecimal Preco
	 */
	@Column(name="preco")
	public BigDecimal getPreco() {
		return this.preco.get();
	}
	
	public void setPreco(BigDecimal preco) {
		this.preco.set(preco);
	}
	
	public ObjectProperty<BigDecimal> precoProperty() {
		return this.preco;
	}
	
	/*
	 * String Un
	 */
	@Column(name="un")
	public String getUn() {
		return this.un.get();
	}
	
	public void setUn(String un) {
		this.un.set(un);
	}
	
	public StringProperty unProperty() {
		return this.un;
	}
	
	@Override
	public BigDecimal precoUn() {
		if (this.precoUn == null) {
			if (getCurrency().equals(Currency.USD)) {
				Dolar dolar = new DolarDAO().get();
				this.precoUn = getPreco().multiply(dolar.getValue());
			}
			else {
				this.precoUn = getPreco();
			}
			
			this.precoUn.setScale(2, RoundingMode.HALF_UP);
		}
		
		return this.precoUn;
	}
	
	@Override
    public StringProperty descriptionProperty() {
		if (this.descProperty.get() == null) {
			this.descProperty.set(getNome());
		}
		
		return this.descProperty;
	}
	
	@Override
    public StringProperty detailProperty() {
		if (this.detailProperty.get() == null) {
			this.detailProperty.set("");
		}
		
		return this.detailProperty;
	}
	
	@Override
	public String toString() {
		return String.format("%-40s %-12s %-15s",
				getNome(), 
				Formatter.formatCurrency(precoUn()), 
				getUn());
	}
	
	@Override
    public boolean equals(Object o) {
    	if (o == this) return true;
    	if (!(o instanceof Diversos)) {
            return false;
        }
    	
    	Diversos d = (Diversos) o;
    	return Objects.equals(getNome(), d.getNome()) &&
    			Objects.equals(getUn(), d.getUn());
    }
    
    @Override
    public int hashCode() {
    	return Objects.hash(getNome(), getUn());
    }
	
}
