package com.marioconcilio.cardmanager.model.vo;

import com.marioconcilio.cardmanager.util.DateUtils;
import com.marioconcilio.cardmanager.util.Formatter;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

/**
 * long number<br>
 * Client client<br>
 * BigDecimal value<br>
 * Date date<br>
 * BillStatus status<br>
 * @author Mario Concilio
 */
@Entity
@Table(name="bill", schema="public")
public class Bill extends AbstractVO {
	
	public enum BillStatus {
		ABERTO("Em Aberto", "ab"),
		PAGO("Pago", "pg"),
		CARTORIO("Em Cartório", "ca"),
		PROTESTADO("Protestado", "bx"),
		OUTRO("Outro", "ou");
		
		private String status;
		private String opt;
		
		BillStatus(String status, String opt) {
			this.status = status;
			this.opt = opt;
		}
		
		public String getOpt() {
			return opt;
		}
		
		@Override
		public String toString() {
			return this.status;
		}
	}

	private final ObjectProperty<Client> client = new SimpleObjectProperty<>(this, "client");
	private final ObjectProperty<BigDecimal> value = new SimpleObjectProperty<>(this, "value");
	private final ObjectProperty<Date> date = new SimpleObjectProperty<>(this, "date");
	private final ObjectProperty<BillStatus> status = new SimpleObjectProperty<>(this, "status");
	private final BooleanProperty ok = new SimpleBooleanProperty(this, "ok");
	
	public Bill() {
		
	}
	
	public Bill(long number) {
		this.id.set(number);
	}
	
	/*
	 * Integer ID
	 */

	@Override
	@Id
	@Column(name="id_bill")
	public long getId() {
		return this.id.get();
	}

	/*
	 * Client client
	 */
	@ManyToOne
//	@Cascade({CascadeType.ALL})
	@JoinColumn(name="id_client")
	public Client getClient() {
		return this.client.get();
	}
	
	public void setClient(Client client) {
		this.client.set(client);
	}
	
	public ObjectProperty<Client> clientProperty() {
		return this.client;
	}
	
	/*
	 * BigDecimal Value
	 */
	@Column(name="value")
	public BigDecimal getValue() {
		return this.value.get();
	}
	
	public void setValue(BigDecimal value) {
		this.value.set(value);
	}
	
	public ObjectProperty<BigDecimal> valueProperty() {
		return this.value;
	}
	
	/*
	 * Date date
	 */
	@Column(name="date")
	public Date getDate() {
		return this.date.get();
	}
	
	public void setDate(Date date) {
		this.date.set(date);
	}
	
	public ObjectProperty<Date> dateProperty() {
		return this.date;
	}

	/*
	 * BillStatus status
	 */
	@Enumerated(EnumType.STRING)
	public BillStatus getStatus() {
		return this.status.get();
	}
	
	public void setStatus(BillStatus status) {
		this.status.set(status);
	}
	
	public ObjectProperty<BillStatus> statusProperty() {
		return this.status;
	}
	
	/*
	 * Boolean OK
	 */
	@Column(name="is_ok")
	public boolean isOk() {
		return this.ok.get();
	}
	
	public void setOk(boolean ok) {
		this.ok.set(ok);
	}
	
	public BooleanProperty okProperty() {
		return this.ok;
	}
	
	@Transient
	public boolean isAberto() {
		return getStatus().equals(BillStatus.ABERTO);
	}
	
	@Transient
	public boolean isLiquidado() {
		return getStatus().equals(BillStatus.PAGO);
	}
	
	@Transient
	public boolean isCartorio() {
		return getStatus().equals(BillStatus.CARTORIO);
	}
	
	@Transient
	public boolean isProtestado() {
		return getStatus().equals(BillStatus.PROTESTADO);
	}
	
	@Transient
	public boolean isOutro() {
		return getStatus().equals(BillStatus.OUTRO);
	}
	
	@Transient
	public boolean isVencido() {
		if (getStatus().equals(BillStatus.ABERTO)) {
			final LocalDate today = LocalDate.now();
			final LocalDate dueTo = DateUtils.asLocalDate(getDate());
			return dueTo.isBefore(today);
		}
		
		return false;
	}

	@Override
    public boolean equals(Object o) {
	    if (o == this) return true;
	    if (!(o instanceof Bill)) return false;

	    Bill bill = (Bill) o;
	    return bill.getId() == this.getId();
    }

    @Override
    public int hashCode() {
	    return Objects.hash(this.getId());
    }
	
	@Override
	public String toString() {
		return String.format("%-8s  %-40s %-12s %s  %s",
				Formatter.formatBillNumber(getId()),
				getClient().getName(),
				Formatter.formatCurrency(getValue()),
				Formatter.formatDate(getDate()),
				getStatus().toString());
	}
	
}
