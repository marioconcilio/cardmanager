package com.marioconcilio.cardmanager.model.vo;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * Abstract superclass that implements Externalizable methods
 * <li>Integer id

 * @author Mario Concilio
 */
public abstract class AbstractVO implements Externalizable {
	
	protected final LongProperty id = new SimpleLongProperty(this, "id");

	public abstract long getId();
	
	public void setId(long id) {
		this.id.set(id);
	}
	
	public LongProperty idProperty() {
		return id;
	}
	
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeLong(getId());
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		this.setId(in.readLong());
	}

}
