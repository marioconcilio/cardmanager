package com.marioconcilio.cardmanager.model.vo;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * <li> String tipo
 * <li> BigDecimal precoKg
 * <li> String descricao
 * @author Mario Concilio
 *
 */
@Entity
@Table(name="papel_tipo", schema="public")
@SequenceGenerator(name="papel_tipo_id_seq", sequenceName="papel_tipo_id_seq")
public class PapelTipo extends AbstractVO {
	
	private final StringProperty tipo = new SimpleStringProperty(this, "tipo");
	private final ObjectProperty<BigDecimal> precoKg = new SimpleObjectProperty<>(this, "precoKg");
	private final StringProperty descricao = new SimpleStringProperty(this, "descricao");

    /*
     * Long ID
     */ 
    @Override
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="papel_tipo_id_seq")
    @Column(name="id_papel_tipo", nullable=false)
    public long getId() {
        return this.id.get();
    }

    /*
     * String tipo
     */ 
    @Column(name="tipo", nullable=false)
    public String getTipo() {
        return this.tipo.get();
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    public StringProperty tipoProperty() {
        return this.tipo;
    }
    
    /*
     * BigDecimal preco_kg
     */ 
    @Column(name="preco_kg", nullable=false)
    public BigDecimal getPrecoKg() {
        return this.precoKg.get();
    }
    
    public void setPrecoKg(BigDecimal precoKg) {
        this.precoKg.set(precoKg);
    }
    
    public ObjectProperty<BigDecimal> precoKgProperty() {
        return this.precoKg;
    }
    
    /*
     * String descricao
     */  
    @Column(name="descricao", nullable=false)
    public String getDescricao() {
        return this.descricao.get();
    }
    
    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }
    
    public StringProperty descricaoProperty() {
        return this.descricao;
    }
    
    /*
     * List<Papel> papeis
     *
    private List<Papel> papeis = new ArrayList<Papel>();
    
    @OneToMany(mappedBy="papelTipo", fetch=FetchType.EAGER)
    public List<Papel> getPapeis() {
        return this.papeis;
    }
    
    public void setPapeis(List<Papel> papeis) {
        this.papeis = papeis;
    }
    */
    
    @Override
    public String toString() {
    	return getDescricao();
    }
    
}
