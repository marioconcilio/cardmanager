package com.marioconcilio.cardmanager.model.vo;

import javax.persistence.*;

@Entity
@Table(name="settings", schema="public")
@SequenceGenerator(name="settings_id_seq", sequenceName="settings_id_seq")
public class Settings extends AbstractVO {
	
	private static final long serialVersionUID = 1L;

	private String rateDebit;
	private String rateCreditOnDemand;
	private String rateCreditOnTerm;

	@Override
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="settings_id_seq")
	@Column(name="id_settings")
	public long getId() {
		return this.id.get();
	}

	@Column(name="rate_debit")
	public String getRateDebit() {
		return rateDebit;
	}

	public void setRateDebit(String rateDebit) {
		this.rateDebit = rateDebit;
	}

	@Column(name="rate_demand")
	public String getRateCreditOnDemand() {
		return rateCreditOnDemand;
	}

	public void setRateCreditOnDemand(String rateCreditOnDemand) {
		this.rateCreditOnDemand = rateCreditOnDemand;
	}

	@Column(name="rate_term")
	public String getRateCreditOnTerm() {
		return rateCreditOnTerm;
	}

	public void setRateCreditOnTerm(String rateCreditOnTerm) {
		this.rateCreditOnTerm = rateCreditOnTerm;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("rate debit: ")
		.append(this.getRateDebit())
		.append('\t')
		.append("rate credit on demand: ")
		.append(this.getRateCreditOnDemand())
		.append('\t')
		.append("rate credit on term: ")
		.append(this.getRateCreditOnTerm());
		
		return str.toString();
	}

}
