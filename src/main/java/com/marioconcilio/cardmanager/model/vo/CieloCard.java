package com.marioconcilio.cardmanager.model.vo;

import com.marioconcilio.cardmanager.util.Formatter;
import javafx.beans.property.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name="card", schema="public")
@SequenceGenerator(name="card_id_seq", sequenceName="card_id_seq")
public class CieloCard extends AbstractVO implements Card {

	private static final long serialVersionUID = 1L;

	private final StringProperty description = new SimpleStringProperty(this, "description");
	private final ObjectProperty<BigDecimal> value = new SimpleObjectProperty<>(this, "value");
	private final ObjectProperty<Date> date = new SimpleObjectProperty<>(this, "date");
	private final ObjectProperty<CardType> cardType = new SimpleObjectProperty<>(this, "cardType");
	private final BooleanProperty ok = new SimpleBooleanProperty(this, "ok");

	/*
	 * Integer ID
	 */
	@Override
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="card_id_seq")
	@Column(name="id_card")
	public long getId() {
		return this.id.get();
	}
	
	/*
	 * String Description
	 */
	@Column(name="description")
	public String getDescription() {
		return this.description.get();
	}
	
	public void setDescription(String description) {
		this.description.set(description);
	}
	
	public StringProperty descriptionProperty() {
		return this.description;
	}
	
	/*
	 * BigDecimal Value
	 */
	@Column(name="value")
	public BigDecimal getValue() {
		return this.value.get();
	}
	
	public void setValue(BigDecimal value) {
		this.value.set(value);
	}
	
	public ObjectProperty<BigDecimal> valueProperty() {
		return this.value;
	}
	
	/*
	 * Date date
	 */
	@Column(name="date")
	public Date getDate() {
		return this.date.get();
	}
	
	public void setDate(Date date) {
		this.date.set(date);
	}
	
	public ObjectProperty<Date> dateProperty() {
		return this.date;
	}
	
	/*
	 * CieloCardType cardType
	 */
	@Enumerated(EnumType.STRING)
	public CieloCardType getCardType() {
		return (CieloCardType) this.cardType.get();
	}
	
	public void setCardType(CieloCardType type) {
		this.cardType.set(type);
	}
	
	public ObjectProperty<CardType> cardTypeProperty() {
		return this.cardType;
	}
	
	/*
	 * Boolean OK
	 */
	@Column(name="is_ok")
	public boolean isOk() {
		return this.ok.get();
	}
	
	public void setOk(boolean ok) {
		this.ok.set(ok);
	}
	
	public BooleanProperty okProperty() {
		return this.ok;
	}
	
	@Override
	public String toString() {
		final String str = String.format("%s  %-20s  %s", 
				Formatter.formatDate(this.getDate()),
				this.getCardType(),
				Formatter.formatCurrency(this.getValue()));
		
		return str;
	}

}
