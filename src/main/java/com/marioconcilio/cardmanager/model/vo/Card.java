package com.marioconcilio.cardmanager.model.vo;

import javafx.beans.property.ObjectProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public interface Card extends Serializable {
	
	interface CardType {}
	
	enum PSMCardType implements CardType {
		CREDITO_AVISTA("Crédito A Vista"),
		CREDITO_PARCELADO("Crédito Parcelado"),
		DEBITO("Débito"),
		OTHER("Outro");
		
		private String type;
		
		PSMCardType(String type) {
			this.type = type;
		}
		
		@Override
		public String toString() {
			return this.type;
		}
	}
	
	enum CieloCardType implements CardType {
		CREDITO_MASTER("Crédito MasterCard"), 
		CREDITO_VISA("Crédito Visa"), 
		CREDITO_ELO("Crédito Elo"),
		CREDITO_AMEX("Crédito Amex"),
		DEBITO_MASTER("Débito MasterCard"),
		DEBITO_VISA("Débito Visa Electron"),
		DEBITO_ELO("Débito Elo"),
		OTHER("Outro");
		
		private String type;
		
		CieloCardType(String type) {
			this.type = type;
		}
		
		@Override
		public String toString() {
			return this.type;
		}
	}
	
	ObjectProperty<Date> dateProperty();
	ObjectProperty<BigDecimal> valueProperty();
	ObjectProperty<CardType> cardTypeProperty();
	boolean isOk();

}
