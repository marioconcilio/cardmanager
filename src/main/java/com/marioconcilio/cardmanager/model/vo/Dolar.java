package com.marioconcilio.cardmanager.model.vo;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name="dolar", schema="public")
public class Dolar extends AbstractVO {
	
	private final ObjectProperty<BigDecimal> value = new SimpleObjectProperty<>(this, "value");
	
	public Dolar() {
		
	}
	
	public Dolar(int dolar) {
		this.setValue(BigDecimal.valueOf(dolar));
	}
	
	public Dolar(double dolar) {
		this.setValue(BigDecimal.valueOf(dolar));
	}

	@Override
	@Id
	@Column(name="id_dolar")
	public long getId() {
		return this.id.get();
	}
	
	/*
	 * BigDecimal Value
	 */
	@Column(name="value")
	public BigDecimal getValue() {
		return this.value.get();
	}
	
	public void setValue(BigDecimal value) {
		this.value.set(value);
	}
	
	public ObjectProperty<BigDecimal> valueProperty() {
		return this.value;
	}

}
