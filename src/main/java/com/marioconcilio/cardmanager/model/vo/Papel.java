package com.marioconcilio.cardmanager.model.vo;

import javafx.beans.property.*;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * <li>PapelTipo
 * <li>Integer grs
 * <li>Integer largura
 * <li>Integer comprimento
 * <li>String formato {@code transient}
 * <li>Integer fls
 * <li>BigDecimal peso
 * <li>String marca {@code nullable}
 * <li>Long qtdeProperty
 * 
 * @author Mario Concilio
 */
@Entity
@Table(name="papel", schema="public")
@SequenceGenerator(name="papel_id_seq", sequenceName="papel_id_seq", allocationSize=1)
public class Papel extends AbstractVO {
	
	private final ObjectProperty<PapelTipo> papelTipo = new SimpleObjectProperty<PapelTipo>(this, "papelTipo");
	private final IntegerProperty grs = new SimpleIntegerProperty(this, "grs");
	private final IntegerProperty largura = new SimpleIntegerProperty(this, "largura");
	private final IntegerProperty comprimento = new SimpleIntegerProperty(this, "comprimento");
	private final StringProperty formato = new SimpleStringProperty(this, "formato"); // transient
	private final IntegerProperty fls = new SimpleIntegerProperty(this, "fls");
	private final ObjectProperty<BigDecimal> peso = new SimpleObjectProperty<BigDecimal>(this, "peso");
	private final StringProperty marca = new SimpleStringProperty(this, "marca");
	private final LongProperty qtde = new SimpleLongProperty(this, "qtdeProperty");
//	private List<OrcamentoPapel> orcamentoPapels = new ArrayList<OrcamentoPapel>(0);
	
    /*
     * Long ID
     */ 
    @Override
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="papel_id_seq")
    @Column(name="id_papel", nullable=false)
    public long getId() {
        return this.id.get();
    }
    
    /*
     * PapelTipo papel_tipo
     */
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_papel_tipo", nullable=false)
    public PapelTipo getPapelTipo() {
        return this.papelTipo.get();
    }
    
    public void setPapelTipo(PapelTipo papelTipo) {
        this.papelTipo.set(papelTipo);
    }
    
    public ObjectProperty<PapelTipo> papelTipoProperty() {
        return this.papelTipo;
    }
    
    /*
     * Integer grs
     */
    @Column(name="grs", nullable=false)
    public int getGrs() {
        return this.grs.get();
    }
    
    public void setGrs(int grs) {
        this.grs.set(grs);
    }
    
    public IntegerProperty grsProperty() {
        return this.grs;
    }
    
    /*
     * Integer largura
     */
    @Column(name="largura", nullable=false)
    public int getLargura() {
        return this.largura.get();
    }
    
    public void setLargura(int largura) {
        this.largura.set(largura);
    }
    
    public IntegerProperty larguraProperty() {
        return this.largura;
    }
    
    /*
     * Integer comprimento
     */
    @Column(name="comprimento", nullable=false)
    public int getComprimento() {
        return this.comprimento.get();
    }
    
    public void setComprimento(int comprimento) {
        this.comprimento.set(comprimento);
    }
    
    public IntegerProperty comprimentoProperty() {
        return this.comprimento;
    }
    
    /*
     * Transient String formato
     */
    @Transient
    public String getFormato() {
    	return String.join("x", Integer.toString(getLargura()), Integer.toString(getComprimento()));
    }
    
    public StringProperty formatoProperty() {
    	String str = getFormato();
    	
    	if (formato.get() == null)
    		formato.set(str);
    	
    	return formato;
    }
    
    /*
     * Integer fls
     */
    @Column(name="fls", nullable=false)
    public int getFls() {
        return this.fls.get();
    }
    
    public void setFls(int fls) {
        this.fls.set(fls);
    }
    
    public IntegerProperty flsProperty() {
        return this.fls;
    }
    
    /*
     * Float peso
     */
    @Column(name="peso", nullable=false)
    public BigDecimal getPeso() {
        return this.peso.get();
    }
    
    public void setPeso(BigDecimal peso) {
        this.peso.set(peso);
    }
    
    public ObjectProperty<BigDecimal> pesoPropery() {
        return this.peso;
    }
    
    /*
     * String marca
     */
    @Column(name="marca", nullable=true)
    public String getMarca() {
        return this.marca.get();
    }
    
    public void setMarca(String marca) {
        this.marca.set(marca);
    }
    
    public StringProperty marcaProperty() {
        return this.marca;
    }
    
    /*
     * Long qtdeProperty
     */
    @Column(name="qtdeProperty", nullable=false)
    public long getQtde() {
        return this.qtde.get();
    }
    
    public void setQtde(long qtde) {
        this.qtde.set(qtde);
    }
    
    public LongProperty qtdeProperty() {
        return this.qtde;
    }
    
    /*
    @OneToMany(fetch=FetchType.LAZY, mappedBy="papel")
	public List<OrcamentoPapel> getOrcamentoPapels() {
		return this.orcamentoPapels;
	}

	public void setOrcamentoPapels(List<OrcamentoPapel> orcamentoPapels) {
		this.orcamentoPapels = orcamentoPapels;
	}
	*/
    
    @Override
    public String toString() {
    	return String.join(" ",
    			getPapelTipo().getTipo(), 
    			Integer.toString(getGrs()) + "g",
    			getFormato(), 
    			Integer.toString(getFls()) + "fls",
    			getMarca());
    }
    
}
