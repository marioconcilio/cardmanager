package com.marioconcilio.cardmanager.model.vo;

import javax.persistence.*;

/**
 * <li>String login
 * <li>String password
 * <li>int level
 * @author Mario Concilio
 *
 */
@Entity
@Table(name="usuario", schema="public")
@SequenceGenerator(name="user_id_seq", sequenceName="user_id_seq")
public class User extends AbstractVO {
	
	private String login;
	private String password;
	private int level;

	/*
	 * Long ID
	 */
	@Override
	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="user_id_seq")
    @Column(name="id_usuario", nullable=false)
	public long getId() {
		return this.id.get();
	}
	
	/*
	 * String Login
	 */
	@Column(name="login", nullable=false)
	public String getLogin() {
		return this.login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	/*
	 * String password
	 */
	@Column(name="password", nullable=false)
	public String getPassword() {
		return this.password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	/*
	 * int level
	 */
	@Column(name="level", nullable=false)
	public int getLevel() {
		return this.level;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	@Override
	public String toString() {
		return getLogin();
	}
	
}
