package com.marioconcilio.cardmanager.model.vo;

import com.marioconcilio.cardmanager.util.Formatter;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <li>Client
 * <li>Date data
 * <li>BigDecimal total
 * 
 * @author Mario Concilio
 */
@Entity
@Table(name="orcamento")
@SequenceGenerator(name="orcamento_id_seq", sequenceName="orcamento_id_seq", allocationSize=1)
public class Orcamento extends AbstractVO {

	private List<OrcamentoPapel> orcamentoPapels = new ArrayList<OrcamentoPapel>(0);
	private final ObjectProperty<Client> client = new SimpleObjectProperty<Client>(this, "client");
	private final ObjectProperty<Date> data = new SimpleObjectProperty<Date>(this, "data");
	private final ObjectProperty<BigDecimal> total = new SimpleObjectProperty<BigDecimal>(this, "total");

	public Orcamento() {
		
	}

	/**
	 * Orçamento
	 * @param client The client who bought
	 * @param data When it was realized
	 * @param total The total amount
	 */
	public Orcamento(Client client, Date data, BigDecimal total) {
		this.client.set(client);
		this.data.set(data);
		this.total.set(total);
	}

	@Override
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="orcamento_id_seq")
	@Column(name="id_orcamento", nullable=false)
	public long getId() {
		return this.id.get();
	}
	
	/*
	 * Client client
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_client", nullable=false)
	public Client getClient() {
		return this.client.get();
	}

	public void setClient(Client client) {
		this.client.set(client);
	}
	
	public ObjectProperty<Client> clientProperty() {
		return this.client;
	}

	/*
	 * Date data
	 */
	@Temporal(TemporalType.DATE)
	@Column(name="data", nullable=false, length=13)
	public Date getData() {
		return this.data.get();
	}

	public void setData(Date data) {
		this.data.set(data);
	}
	
	public ObjectProperty<Date> dataProperty() {
		return this.data;
	}

	/*
	 * BigDecimal total
	 */
	@Column(name="total", nullable=false, precision=8, scale=2)
	public BigDecimal getTotal() {
		return this.total.get();
	}

	public void setTotal(BigDecimal total) {
		this.total.set(total);
	}
	
	public ObjectProperty<BigDecimal> totalProperty() {
		return this.total;
	}

	/*
	 * List orcamento_papel
	 */
	@OneToMany(fetch=FetchType.EAGER, mappedBy="orcamento")
	public List<OrcamentoPapel> getOrcamentoPapels() {
		return this.orcamentoPapels;
	}

	public void setOrcamentoPapels(List<OrcamentoPapel> orcamentoPapels) {
		this.orcamentoPapels = orcamentoPapels;
	}
	
	@Override
	public String toString() {
		return String.join(" ", 
				Formatter.formatDate(getData()),
				getClient().getName(),
				Formatter.formatCurrency(getTotal()));
	}

}
