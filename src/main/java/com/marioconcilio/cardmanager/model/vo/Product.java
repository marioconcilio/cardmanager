package com.marioconcilio.cardmanager.model.vo;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public interface Product extends Serializable {
	
	public enum Currency {
		BRL("R$"),
		USD("US$");
		
		private String symbol;
		
		Currency(String symbol) {
			this.symbol = symbol;
		}
		
		@Override
		public String toString() {
			return this.symbol;
		}
	};
	
	public BigDecimal precoUn();
	
	public StringProperty descriptionProperty();
	public StringProperty detailProperty();
	public StringProperty unProperty();
	 
	public Currency getCurrency();
	public void setCurrency(Currency currency);
	
	public Integer getQtde();
	public void setQtde(Integer qtde);
	public IntegerProperty qtdeProperty();

}
