package com.marioconcilio.cardmanager.model.vo;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name="chapa_marca", schema="public")
public class ChapaMarca extends AbstractVO {
	
	private final StringProperty marca = new SimpleStringProperty(this, "marca");
	private final StringProperty esp = new SimpleStringProperty(this, "esp");
	private final ObjectProperty<BigDecimal> preco = new SimpleObjectProperty<>(this, "preco");
	
	public ChapaMarca() {
		
	}
	
	public ChapaMarca(String marca, String esp, BigDecimal preco) {
		this.marca.set(marca);
		this.esp.set(esp);
		this.preco.set(preco);
	}

	/*
	 * Integer ID
	 */
	@Override
	@Id
	@Column(name="id_chapa_marca")
	public long getId() {
		return this.id.get();
	}
	
	/*
	 * String Nome
	 */
	@Column(name="marca")
	public String getMarca() {
		return this.marca.get();
	}
	
	public void setMarca(String marca) {
		this.marca.set(marca);
	}
	
	public StringProperty marcaProperty() {
		return this.marca;
	}
	
	/*
	 * String Esp
	 */
	@Column(name="esp")
	public String getEsp() {
		return this.esp.get();
	}
	
	public void setEsp(String esp) {
		this.esp.set(esp);
	}
	
	public StringProperty espProperty() {
		return this.esp;
	}
	
	/*
	 * BigDecimal Preco
	 */
	@Column(name="preco")
	public BigDecimal getPreco() {
		return this.preco.get();
	}
	
	public void setPreco(BigDecimal preco) {
		this.preco.set(preco);
	}
	
	public ObjectProperty<BigDecimal> precoProperty() {
		return this.preco;
	}
	
	@Override
	public String toString() {
		return String.format("%-8s", getMarca());
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof ChapaMarca)) {
            return false;
        }
		
		ChapaMarca marca = (ChapaMarca) o;
		return Objects.equals(getMarca(), marca.getMarca()) &&
				Objects.equals(getEsp(), marca.getEsp());
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(getMarca(), getEsp());
	}

}
