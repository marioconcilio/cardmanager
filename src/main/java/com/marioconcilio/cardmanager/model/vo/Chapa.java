package com.marioconcilio.cardmanager.model.vo;

import com.marioconcilio.cardmanager.model.dao.DolarDAO;
import com.marioconcilio.cardmanager.util.Formatter;
import javafx.beans.property.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

@Entity
@Table(name="chapa", schema="public")
public class Chapa extends AbstractProduct {
	
	private final IntegerProperty larg = new SimpleIntegerProperty(this, "larg");
	private final IntegerProperty comp = new SimpleIntegerProperty(this, "comp");
	private final ObjectProperty<ChapaMarca> chapaMarca = new SimpleObjectProperty<ChapaMarca>(this, "chapaMarca");

	/*
	 * Integer ID
	 */
	@Override
	@Id
	@Column(name="id_chapa")
	public long getId() {
		return this.id.get();
	}
	
	/*
	 * Integer Larg
	 */
	@Column(name="larg")
	public Integer getLarg() {
		return this.larg.get();
	}
	
	public void setLarg(Integer larg) {
		this.larg.set(larg);
	}
	
	public IntegerProperty largProperty() {
		return this.larg;
	}
	
	/*
	 * Integer Comp
	 */
	@Column(name="comp")
	public Integer getComp() {
		return this.comp.get();
	}
	
	public void setComp(Integer comp) {
		this.comp.set(comp);
	}
	
	public IntegerProperty compProperty() {
		return this.comp;
	}
	
	/*
     * ChapaMarca chapaMarca
     */
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_chapa_marca", nullable=false)
    public ChapaMarca getChapaMarca() {
        return this.chapaMarca.get();
    }
    
    public void setChapaMarca(ChapaMarca chapaMarca) {
        this.chapaMarca.set(chapaMarca);
    }
    
    public ObjectProperty<ChapaMarca> chapaMarcaProperty() {
        return this.chapaMarca;
    }
    
    public BigDecimal precoAvulso() {
    	final ChapaMarca marca = getChapaMarca();
    	if (marca.getMarca().equals("Chinesa")) {
    		return precoUn().multiply(BigDecimal.valueOf(1.27));
    	}
    	
    	return null;
    }
    
    @Override
    public StringProperty unProperty() {
    	if (this.unProperty.get() == null) {
    		this.unProperty.set("Un");
    	}
    	
    	return this.unProperty;
    }
    
    @Override
    public BigDecimal precoUn() {
    	if (this.precoUn == null) {
    		final BigDecimal preco;
    		if (getCurrency().equals(Currency.USD)) {
    			Dolar dolar = new DolarDAO().get();
    			preco = getChapaMarca().getPreco().multiply(dolar.getValue());
    		}
    		else {
    			preco = getChapaMarca().getPreco();
    		}

    		this.precoUn = preco.multiply(BigDecimal.valueOf(getLarg()))
    				.multiply(BigDecimal.valueOf(getComp()))
    				.divide(BigDecimal.valueOf(1000000L))
    				.setScale(1, RoundingMode.HALF_UP);
    	}

    	return this.precoUn;
    }
    
    @Override
    public StringProperty descriptionProperty() {
    	if (this.descProperty.get() == null) {
    		this.descProperty.set(String.format("%dx%d",
    				getLarg(),
    				getComp()));
    	}
    	
    	return this.descProperty;
    }
    
    @Override
    public StringProperty detailProperty() {
    	if (this.detailProperty.get() == null) {
    		this.detailProperty.set(getChapaMarca().getMarca());
    	}
    	
    	return this.detailProperty;
    }
    
    @Override
    public String toString() {
    	String avulso;
    	if (precoAvulso() != null) {
    		avulso = String.format("Avulso  %12s", Formatter.formatCurrency(precoAvulso()));
    	}
    	else {
    		avulso = "";
    	}
    	
    	return String.format("%dx%d %s %12s %s",
				getLarg(),
				getComp(),
				getChapaMarca(),
				Formatter.formatCurrency(precoUn()),
				avulso);
    }
    
    @Override
    public boolean equals(Object o) {
    	if (o == this) return true;
    	if (!(o instanceof Chapa)) {
            return false;
        }
    	
    	Chapa chapa = (Chapa) o;
    	return getLarg() == chapa.getLarg() &&
    			getComp() == chapa.getComp() &&
    			Objects.equals(getChapaMarca(), chapa.getChapaMarca());
    }
    
    @Override
    public int hashCode() {
    	return Objects.hash(getLarg(), getComp(), getChapaMarca());
    }
    
}
