package com.marioconcilio.cardmanager.model.vo;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * <li>String cnpj
 * <li>String name
 * <li>String address
 * <li>String city
 * <li>String state
 * <li>Integer cep
 * <li>Integer credit_limit
 * <li>List Bill
 * <li>List Orcamento
 * 
 * @author Mario Concilio
 */
@Entity
@Table(name="client", schema="public")
public class Client extends AbstractVO {
	
	private final StringProperty name = new SimpleStringProperty(this, "name");
	private final StringProperty email = new SimpleStringProperty(this, "email");
	private final StringProperty address = new SimpleStringProperty(this, "address");
	private final StringProperty city = new SimpleStringProperty(this, "city");
	private final StringProperty state = new SimpleStringProperty(this, "state");
	private final IntegerProperty cep = new SimpleIntegerProperty(this, "cep");
	private final IntegerProperty creditLimit = new SimpleIntegerProperty(this, "credit_limit");
	private List<Bill> bills = new ArrayList<>(0);
	private List<Orcamento> orcamentos = new ArrayList<>(0);

	public Client() {

    }

    public Client(long cnpj) {
	    setId(cnpj);
    }

    public Client(long cnpj, String name) {
	    setId(cnpj);
	    setName(name);
    }
	
	/*
	 * Integer ID
	 */
	@Override
	@Id
	@Column(name="id_client")
	public long getId() {
		return this.id.get();
	}
	
	/*
	 * String Name
	 */
	@Column(name="name", nullable=false)
	public String getName() {
		return this.name.get();
	}
	
	public void setName(String name) {
		this.name.set(name);
	}
	
	public StringProperty nameProperty() {
		return this.name;
	}
	
	/*
	 * String Email
	 */
	@Column(name="email")
	public String getEmail() {
		return this.email.get();
	}
	
	public void setEmail(String email) {
		this.email.set(email);
	}
	
	public StringProperty emailProperty() {
		return this.email;
	}
	
	/*
	 * String Address
	 */
	@Column(name="address")
	public String getAddress() {
		return this.address.get();
	}
	
	public void setAddress(String address) {
		this.address.set(address);
	}
	
	public StringProperty addressProperty() {
		return this.address;
	}
	
	/*
     * String City
     */    
    @Column(name="city")
    public String getCity() {
        return this.city.get();
    }
    
    public void setCity(String city) {
        this.city.set(city);
    }
    
    public StringProperty cityProperty() {
        return this.city;
    }
    
    /*
     * String State
     */
    @Column(name="state")
    public String getState() {
        return this.state.get();
    }
    
    public void setState(String state) {
        this.state.set(state);
    }
    
    public StringProperty stateProperty() {
        return this.state;
    }

    /*
     * Integer Cep
     */
    @Column(name="cep")
    public Integer getCep() {
        return this.cep.get();
    }
    
    public void setCep(Integer cep) {
    	if (cep == null) return;
        this.cep.set(cep);
    }
    
    public IntegerProperty cepProperty() {
        return this.cep;
    }
	
	/*
     * Integer credit_limit
     */
    @Column(name="credit_limit", nullable=false)
    public int getCreditLimit() {
        return this.creditLimit.get();
    }
    
    public void setCreditLimit(int creditLimit) {
        this.creditLimit.set(creditLimit);
    }
    
    public IntegerProperty creditLimitProperty() {
        return this.creditLimit;
    }
	
	/*
	 * List<Bill> bills
	 */
	@OneToMany(mappedBy="client", fetch=FetchType.EAGER)
//	@Cascade({CascadeType.ALL})
	public List<Bill> getBills() {
		return this.bills;
	}
	
	public void setBills(List<Bill> bills) {
		this.bills = bills;
	}

	/*
	 * List<Orcamento> orcamentos
	 */
	@OneToMany(mappedBy="client", fetch=FetchType.LAZY)
	public List<Orcamento> getOrcamentos() {
		return this.orcamentos;
	}

	public void setOrcamentos(List<Orcamento> orcamentos) {
		this.orcamentos = orcamentos;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Client)) return false;

		Client client = (Client) o;
		return client.getId() == this.getId();
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getId());
	}

	@Override
	public String toString() {
		return getName();
	}
	
}
