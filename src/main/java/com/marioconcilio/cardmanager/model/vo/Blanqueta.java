package com.marioconcilio.cardmanager.model.vo;

import com.marioconcilio.cardmanager.model.dao.DolarDAO;
import com.marioconcilio.cardmanager.util.Formatter;
import javafx.beans.property.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

@Entity
@Table(name="blanqueta", schema="public")
public class Blanqueta extends AbstractProduct {
	
	private final StringProperty nome = new SimpleStringProperty(this, "nome");
	private final IntegerProperty larg = new SimpleIntegerProperty(this, "larg");
	private final IntegerProperty comp = new SimpleIntegerProperty(this, "comp");
	private final ObjectProperty<BlanquetaLona> blanquetaLona = new SimpleObjectProperty<>(this, "blanquetaLona");

	/*
	 * Integer ID
	 */
	@Override
	@Id
	@Column(name="id_blanqueta")
	public long getId() {
		return this.id.get();
	}
	
	/*
	 * String Nome
	 */
	@Column(name="nome")
	public String getNome() {
		return this.nome.get();
	}
	
	public void setNome(String nome) {
		this.nome.set(nome);
	}
	
	public StringProperty nomeProperty() {
		return this.nome;
	}
	
	/*
	 * Integer Larg
	 */
	@Column(name="larg")
	public Integer getLarg() {
		return this.larg.get();
	}
	
	public void setLarg(Integer larg) {
		this.larg.set(larg);
	}
	
	public IntegerProperty largProperty() {
		return this.larg;
	}
	
	/*
	 * Integer Comp
	 */
	@Column(name="comp")
	public Integer getComp() {
		return this.comp.get();
	}
	
	public void setComp(Integer comp) {
		this.comp.set(comp);
	}
	
	public IntegerProperty compProperty() {
		return this.comp;
	}
	
	/*
     * BlanquetaLona blanquetaLona
     */
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_blanqueta_lona", nullable=false)
    public BlanquetaLona getBlanquetaLona() {
        return this.blanquetaLona.get();
    }
    
    public void setBlanquetaLona(BlanquetaLona blanquetaLona) {
        this.blanquetaLona.set(blanquetaLona);
    }
    
    public ObjectProperty<BlanquetaLona> blanquetaLonaProperty() {
        return this.blanquetaLona;
    }
    
    @Override
    public StringProperty unProperty() {
    	if (this.unProperty.get() == null) {
    		this.unProperty.set("Un");
    	}
    	
    	return this.unProperty;
    }
    
    @Override
    public BigDecimal precoUn() {
    	if (this.precoUn == null) {
    		final BlanquetaLona lona = getBlanquetaLona();
    		final BigDecimal preco;
    		if (getCurrency().equals(Currency.USD)) {
    			Dolar dolar = new DolarDAO().get();
    			preco = lona.getPreco().multiply(dolar.getValue());
    		}
    		else {
    			preco = lona.getPreco();
    		}

    		this.precoUn = preco.multiply(BigDecimal.valueOf(getLarg()))
    				.multiply(BigDecimal.valueOf(getComp()))
    				.divide(BigDecimal.valueOf(1000000L))
    				.setScale(1, RoundingMode.HALF_UP);
    	}
		
		return this.precoUn;
    }
    
    @Override
    public StringProperty descriptionProperty() {
    	if (this.descProperty.get() == null) {
    		this.descProperty.set(String.format("%dx%d %s",
    				getLarg(),
    				getComp(),
    				getBlanquetaLona()));
    	}
    	
    	return this.descProperty;
    }
    
    @Override
    public StringProperty detailProperty() {
    	if (this.detailProperty.get() == null) {
    		this.detailProperty.set(getNome());
    	}
    	
    	return this.detailProperty;
    }
    
    @Override
	public String toString() {		
    	return String.format("%-15s %dx%d %12s", 
    			getNome(), 
    			getLarg(),
    			getComp(),
    			Formatter.formatCurrency(precoUn()));
	}
    
    @Override
    public boolean equals(Object o) {
    	if (o == this) return true;
    	if (!(o instanceof Blanqueta)) {
            return false;
        }
    	
    	Blanqueta b = (Blanqueta) o;
    	return getLarg() == b.getLarg() &&
    			getComp() == b.getComp() &&
    			Objects.equals(getBlanquetaLona(), b.getBlanquetaLona());
    }
    
    @Override
    public int hashCode() {
    	return Objects.hash(getLarg(), getComp(), getBlanquetaLona());
    }

}
