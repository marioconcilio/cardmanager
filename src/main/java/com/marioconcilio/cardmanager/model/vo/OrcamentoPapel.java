package com.marioconcilio.cardmanager.model.vo;

import javafx.beans.binding.Bindings;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.*;
import java.io.*;
import java.math.BigDecimal;

/**
 * <li>Orçamento
 * <li>Papel
 * <li>Long qtdeProperty
 * <li>BigDecimal precoUn
 * 
 * @author Mario Concilio
 */
@Entity
@Table(name="orcamento_papel")
public class OrcamentoPapel implements Serializable, Externalizable {

	private OrcamentoPapelId id;
	private final ObjectProperty<Orcamento> orcamento = new SimpleObjectProperty<Orcamento>(this, "orcamento");
	private final ObjectProperty<Papel> papel = new SimpleObjectProperty<Papel>(this, "papel");
	private final LongProperty qtde = new SimpleLongProperty(this, "qtdeProperty");
	private final ObjectProperty<BigDecimal> precoUn = new SimpleObjectProperty<BigDecimal>(this, "precoUn");
	
	/*
	 * @transient
	 * used only to bind to view controller's table
	 */
	private final ObjectProperty<BigDecimal> subtotal = new SimpleObjectProperty<BigDecimal>(this, "subtotal");

	public OrcamentoPapel() {
		// subtotal binding
		subtotal.bind(Bindings.createObjectBinding(() ->
			precoUn.get().multiply(new BigDecimal(qtde.get())),
			precoUn, qtde));
	}

	/**
	 * 
	 * @param orcamento
	 * @param papel
	 * @param qtdeProperty
	 * @param subtotal
	 */
	public OrcamentoPapel(Orcamento orcamento,
			Papel papel, 
			long qtde, 
			BigDecimal precoUn) {
		this();
		
		this.id = new OrcamentoPapelId(orcamento.getId(), papel.getId());
		this.orcamento.set(orcamento);
		this.papel.set(papel);
		this.qtde.set(qtde);
		this.precoUn.set(precoUn);
	}

//	@Override
	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name="idOrcamento", column=@Column(name="id_orcamento", nullable=false)),
			@AttributeOverride(name="idPapel", column=@Column(name="id_papel", nullable=false)) })
	public OrcamentoPapelId getId() {
		return this.id;
	}

	public void setId(OrcamentoPapelId id) {
		this.id = id;
	}

	/*
	 * Orcamento orcamento
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_orcamento", nullable=false, insertable=false, updatable=false)
	public Orcamento getOrcamento() {
		return this.orcamento.get();
	}

	public void setOrcamento(Orcamento orcamento) {
		this.orcamento.set(orcamento);
	}
	
	public ObjectProperty<Orcamento> orcamentoProperty() {
		return this.orcamento;
	}
	
	/*
	 * Papel papel
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_papel", nullable=false, insertable=false, updatable=false)
	public Papel getPapel() {
		return this.papel.get();
	}

	public void setPapel(Papel papel) {
		this.papel.set(papel);
	}
	
	public ObjectProperty<Papel> papelProperty() {
		return this.papel;
	}
	
	/*
	 * Long qtdeProperty
	 */		
	@Column(name="qtdeProperty", nullable=false)
	public long getQtde() {
		return this.qtde.get();
	}

	public void setQtde(long qtde) {
		this.qtde.set(qtde);
	}
	
	public LongProperty qtdeProperty() {
		return this.qtde;
	}

	/*
	 * BigDecimal precoUn	
	 */		
	@Column(name="preco_un", nullable=false, precision=8, scale=2)
	public BigDecimal getPrecoUn() {
		return this.precoUn.get();
	}

	public void setPrecoUn(BigDecimal precoUn) {
		this.precoUn.set(precoUn);
	}
	
	public ObjectProperty<BigDecimal> precoUnProperty() {
		return this.precoUn;
	}
	
	/*
	 * Transient
	 * BigDecimal subtotal
	 */
	@Transient
	public BigDecimal getSubtotal() {
		return this.subtotal.get();
	}
	
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal.set(subtotal);
	}
	
	public ObjectProperty<BigDecimal> subtotalProperty() {
		return this.subtotal;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeObject(getId());
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		setId((OrcamentoPapelId) in.readObject());
	}
	
	@Override
	public String toString() {
		return String.join(" ",	Long.toString(getQtde()),
				getPapel().toString(),
				getPrecoUn().toString());
	}

}
