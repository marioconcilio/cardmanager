package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Tinta;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.ObservableList;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.function.Consumer;

public class TintaDAO extends AbstractDAO<Tinta> {

	@Override
	public void delete(Tinta entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listAll(Consumer<ObservableList<Tinta>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery("from Tinta order by nome, marca");
		task.setConsumer(consumer);
		task.start();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tinta> list() {
		Session session = HibernateUtil.getSession();
		Criteria crit = session.createCriteria(Tinta.class)
				.addOrder(Order.asc("marca"))
				.addOrder(Order.asc("nome"));
		
		final List<Tinta> list = crit.list();
		session.close();
		
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tinta> fetch(String str) {
		Session session = HibernateUtil.getSession();
		Criteria crit = session.createCriteria(Tinta.class)
				.add(Restrictions.ilike("nome", str, MatchMode.ANYWHERE))
				.addOrder(Order.asc("nome"))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		List<Tinta> list = crit.list();
		session.close();
		
		return list;
	}

}
