package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Card.CieloCardType;
import com.marioconcilio.cardmanager.model.vo.CieloCard;
import com.marioconcilio.cardmanager.util.Formatter;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.ObservableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;

import javax.swing.*;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

public class CieloCardDAO extends AbstractDAO<CieloCard> {
	
	private static final Logger logger = LogManager.getLogger(CieloCardDAO.class);
	
	public void updateAllOK(List<CieloCard> list) throws Exception {
		Session session = HibernateUtil.getSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			
			int i = 0;
			for (CieloCard card : list) {
				card.setOk(true);
				session.update(card);
				
				// JDBC batch size in hibernate.cfg.xml
				if(++i % 20 == 0) {
					session.flush();
					session.clear();
				}
			}
			
			tx.commit();
			logger.info("Batch updated CieloCard");
		}
		catch (ConstraintViolationException ex) {
			tx.rollback();
			
			for (CieloCard card : list) {
				update(card);
			}
		}
		catch (HibernateException ex) {
			tx.rollback();
			
			logger.error("Error on batch updating CieloCard", ex);	
			throw new Exception(ex);
		}
		finally {
			session.close();
		}
	}

	@Override
	public void delete(CieloCard entity) {
		Session session = HibernateUtil.getSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			session.delete(entity);
			tx.commit();
			JOptionPane.showMessageDialog(null, "Entrada excluída com sucesso.",
					"Sucesso", 
					JOptionPane.PLAIN_MESSAGE);
			
			System.out.println("DELETE card: success");
		}
		catch(HibernateException ex) {
			tx.rollback();
			JOptionPane.showMessageDialog(null, "Erro ao excluír entrada no extrato.", 
					"Erro", 
					JOptionPane.ERROR_MESSAGE);
			
			System.out.println("DELETE card: error");
		}
		finally {
			session.close();
		}
	}

	@Override
	public void listAll(Consumer<ObservableList<CieloCard>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery("from CieloCard order by date desc");
		task.setConsumer(consumer);
		task.start();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CieloCard> list() {
		Session session = HibernateUtil.getSession();
		List<CieloCard> list = session.createQuery("from CieloCard order by date asc")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		session.close();
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<CieloCard> listCardsFrom(Date date, CieloCardType type, BigDecimal value) {
		// 7 days later
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 7);
		Date sevenDays = calendar.getTime();
		
		System.out.println("query:");
		System.out.println("\tfrom: " + Formatter.formatDate(date)); 
		System.out.println("\tto: " + Formatter.formatDate(sevenDays));
		System.out.println("\tvalue: " + value);
		System.out.println("\ttype: " + type);
		
		Session session = HibernateUtil.getSession();
		Criteria crit = session.createCriteria(CieloCard.class)
				.add(Restrictions.ge("date", date))		// greater or equal than date
				.add(Restrictions.le("date", sevenDays))	// less or equal than date + 7
				.add(Restrictions.le("value", value))		// less or equal than value
				.add(Restrictions.eq("cardType", type))	// equal card type
				.add(Restrictions.eq("ok", false))		// not equal OK
				.addOrder(Order.desc("date"));				// order by date desc
		
		return crit.list();
	}

	@Override
	public List<CieloCard> fetch(String str) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
