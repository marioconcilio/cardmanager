package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Settings;
import com.marioconcilio.cardmanager.util.Constants;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import com.marioconcilio.cardmanager.view.AlertException;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.function.Consumer;

public class SettingsDAO extends AbstractDAO<Settings> {

	@Override
	public void save(Settings entity) {
		Session session = HibernateUtil.getSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			session.update(entity);
			tx.commit();
			
			System.out.println("SAVE settings: success");
			
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle(Constants.APP_NAME);
			alert.setHeaderText("Sucesso");
			alert.setContentText("Configurações salvas com sucesso.");
			alert.showAndWait();
		}
		catch(HibernateException ex) {
			tx.rollback();
			
			System.out.println("SAVE settings: error");
			
			AlertException alert = new AlertException(AlertType.ERROR, ex);
			alert.setTitle(Constants.APP_NAME);
			alert.setHeaderText("Erro");
			alert.setContentText("Não foi possível salvar as configurações.");
			alert.showAndWait();
		}
		finally {
			session.close();
		}
	}

	@Override
	public void update(Settings entity) {
		Session session = HibernateUtil.getSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			session.update(entity);
			tx.commit();
			
			System.out.println("UPDATE settings: success");
			
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle(Constants.APP_NAME);
			alert.setHeaderText("Atualizado com sucesso");
			alert.setContentText("Configurações atualizadas.");
			alert.showAndWait();
		}
		catch(HibernateException ex) {
			tx.rollback();
			
			System.out.println("UPDATE settings: error");
			
			AlertException alert = new AlertException(AlertType.ERROR, ex);
			alert.setTitle(Constants.APP_NAME);
			alert.setHeaderText("Erro");
			alert.setContentText("Não foi possível atualizar as configurações.");
			alert.showAndWait();
		}
		finally {
			session.close();
		}
	}

	@Override
	public void delete(Settings entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listAll(Consumer<ObservableList<Settings>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery("from Settings");
		task.setConsumer(consumer);
		task.start();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Settings> list() {
		Session session = HibernateUtil.getSession();
		List<Settings> list = session.createQuery("from Settings")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		session.close();
		
		return list;
	}

	@Override
	public List<Settings> fetch(String str) {
		// TODO Auto-generated method stub
		return null;
	}

}
