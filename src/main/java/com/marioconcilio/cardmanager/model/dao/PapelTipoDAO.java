package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.PapelTipo;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.ObservableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.function.Consumer;

public class PapelTipoDAO extends AbstractDAO<PapelTipo> {
	
	private static final Logger logger = LogManager.getLogger();

	@Override
	public void save(PapelTipo entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(PapelTipo entity) {
		Session session = HibernateUtil.getSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();

			session.update(entity);
			tx.commit();
			
			logger.info("Updating {}", entity.toString());
		}
		catch(HibernateException ex) {
			tx.rollback();
			ex.printStackTrace();
		}
		finally {
			session.close();
		}
	}

	@Override
	public void delete(PapelTipo entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listAll(Consumer<ObservableList<PapelTipo>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery("from PapelTipo order by descricao asc");
		task.setConsumer(consumer);
		task.start();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PapelTipo> list() {
		Session session = HibernateUtil.getSession();
		List<PapelTipo> list = session.createQuery("from PapelTipo order by descricao asc")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		session.close();
		
		return list;
	}

	@Override
	public List<PapelTipo> fetch(String str) {
		// TODO Auto-generated method stub
		return null;
	}

}
