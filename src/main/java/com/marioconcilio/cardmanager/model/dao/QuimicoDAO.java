package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Quimico;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.ObservableList;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.function.Consumer;

public class QuimicoDAO extends AbstractDAO<Quimico> {

	@Override
	public void delete(Quimico entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listAll(Consumer<ObservableList<Quimico>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery("from Quimico order by nome, marca");
		task.setConsumer(consumer);
		task.start();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Quimico> list() {
		Session session = HibernateUtil.getSession();
		List<Quimico> list = session.createQuery("from Quimico order by nome, marca")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		session.close();

		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Quimico> fetch(String str) {
		Session session = HibernateUtil.getSession();
		Criteria crit = session.createCriteria(Quimico.class)
				.add(Restrictions.ilike("nome", str, MatchMode.ANYWHERE))
				.addOrder(Order.asc("nome"))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		List<Quimico> list = crit.list();
		session.close();
		
		return list;
	}

}
