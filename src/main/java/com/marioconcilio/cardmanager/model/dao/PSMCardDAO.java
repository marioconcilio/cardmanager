package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Card.PSMCardType;
import com.marioconcilio.cardmanager.model.vo.PSMCard;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.ObservableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

public class PSMCardDAO extends AbstractDAO<PSMCard> {
	
	private static final Logger logger = LogManager.getLogger(PSMCardDAO.class);
	
	public void updateAllOK(List<PSMCard> list) throws Exception {
		final Session session = HibernateUtil.getSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			
			int i = 0;
			for (PSMCard card : list) {
				card.setOk(true);
				session.update(card);
				
				// JDBC batch size in hibernate.cfg.xml
				if (i++ % 20 == 0) {
					session.flush();
					session.clear();
				}
			}
			
			tx.commit();
			logger.info("Batch updated PSMCard");
		}
		catch (ConstraintViolationException ex) {
			tx.rollback();
			
			for (PSMCard card : list) {
				update(card);
			}
		}
		catch (HibernateException ex) {
			tx.rollback();
			
			logger.error("Error on batch updating PSMCard", ex);	
			throw new Exception(ex);
		}
		finally {
			session.close();
		}
	}

	@Override
	public void delete(PSMCard entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listAll(Consumer<ObservableList<PSMCard>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery("from PSMCard order by date desc, value desc, cardType desc");
		task.setConsumer(consumer);
		task.start();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PSMCard> list() {
		Session session = HibernateUtil.getSession();
		List<PSMCard> list = session.createQuery("from PSMCard order by date asc")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		session.close();
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<PSMCard> listCardsFrom(Date date, PSMCardType type) {
		Session session = HibernateUtil.getSession();
		Criteria crit = session.createCriteria(PSMCard.class)
				.add(Restrictions.eq("date", date))
				.add(Restrictions.eq("cardType", type))
				.add(Restrictions.eq("ok", false));
		
		final List<PSMCard> list = crit.list();
		session.close();
		
		return list;
	}
	
	public PSMCard fetchCard(Date date, PSMCardType type, BigDecimal value) {
		Session session = HibernateUtil.getSession();
		Criteria crit = session.createCriteria(PSMCard.class)
				.add(Restrictions.eq("date", date))
				.add(Restrictions.eq("cardType", type))
				.add(Restrictions.eq("value", value))
				.add(Restrictions.eq("ok", false));
		
		final PSMCard card = (PSMCard) crit.uniqueResult();
		session.close();
		
		return card;
	}
	
	public PSMCard fetchCard(PSMCard card) {
		return fetchCard(card.getDate(), card.getCardType(), card.getValue());
	}
	
	@SuppressWarnings("unchecked")
	public List<PSMCard> fetchCards(BigDecimal value) {
		Session session = HibernateUtil.getSession();
		Criteria crit = session.createCriteria(PSMCard.class)
				.add(Restrictions.eq("value", value));
		
		final List<PSMCard> list = crit.list();
		session.close();
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<PSMCard> fetchCards(Date date) {
		Session session = HibernateUtil.getSession();
		Criteria crit = session.createCriteria(PSMCard.class)
				.add(Restrictions.eq("date", date));
		
		final List<PSMCard> list = crit.list();
		session.close();
		
		return list;
	}

	@Override
	public List<PSMCard> fetch(String str) {
		// TODO Auto-generated method stub
		return null;
	}

}
