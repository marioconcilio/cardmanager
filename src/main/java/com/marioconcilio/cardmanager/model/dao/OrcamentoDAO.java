package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Orcamento;
import com.marioconcilio.cardmanager.model.vo.OrcamentoPapelId;
import com.marioconcilio.cardmanager.model.vo.Papel;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.ObservableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.function.Consumer;

public class OrcamentoDAO extends AbstractDAO<Orcamento> {
	
	private static final Logger logger = LogManager.getLogger();
	
	@Override
	public void save(Orcamento entity) {
		Session session = HibernateUtil.getSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			session.save(entity);
			
			entity.getOrcamentoPapels().forEach(op -> {
				Papel papel = (Papel) session.load(Papel.class, new Long(op.getPapel().getId()));
				op.setPapel(papel);
				
				OrcamentoPapelId id = new OrcamentoPapelId(entity, papel);
				op.setOrcamento(entity);
				op.setId(id);
				
				session.save(op);
			});
			
			tx.commit();

			logger.info("Saving {}", entity);
		}
		catch(HibernateException ex) {
			tx.rollback();
			ex.printStackTrace();
		}
		finally {
			session.close();
		}
	}

	@Override
	public void update(Orcamento entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Orcamento entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listAll(Consumer<ObservableList<Orcamento>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery("from Orcamento order by data desc");
		task.setConsumer(consumer);
		task.start();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Orcamento> list() {
		Session session = HibernateUtil.getSession();
		List<Orcamento> list = session.createQuery("from Orcamento order by data desc")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		session.close();
		
		return list;
	}

	@Override
	public List<Orcamento> fetch(String str) {
		// TODO Auto-generated method stub
		return null;
	}

}
