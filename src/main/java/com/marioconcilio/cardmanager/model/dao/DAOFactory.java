package com.marioconcilio.cardmanager.model.dao;

public class DAOFactory {
	
	public enum DAO {
		BILL,
		CARD,
		CLIENT,
		ORCAMENTO,
		ORCAMENTO_PAPEL,
		PAPEL,
		PAPEL_TIPO,
		PSM_CARD,
		SETTINGS,
		USER,
	};
		
	private DAOFactory() {
		//
	}
	
	public static AbstractDAO<?> getInstance(DAO dao) {
		switch(dao) {
		case BILL:
			return new BillDAO();
			
		case CARD:
			return new CieloCardDAO();	
			
		case CLIENT:
			return new ClientDAO();
			
		case ORCAMENTO:
			return new OrcamentoDAO();
			
		case ORCAMENTO_PAPEL:
			return new OrcamentoPapelDAO();
			
		case PAPEL:
			return new PapelDAO();
			
		case PAPEL_TIPO:
			return new PapelTipoDAO();
			
		case PSM_CARD:
			return new PSMCardDAO();
			
		case SETTINGS:
			return new SettingsDAO();
			
		case USER:
			return new UserDAO();
			
		default:
			throw new IllegalArgumentException("no such table in database");
		}
	}

}
