package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Blanqueta;
import com.marioconcilio.cardmanager.util.Formatter;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.ObservableList;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class BlanquetaDAO extends AbstractDAO<Blanqueta> {

	@Override
	public void delete(Blanqueta entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listAll(Consumer<ObservableList<Blanqueta>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery("from Blanqueta as b "
				+ "inner join fetch b.blanquetaLona "
				+ "order by nome asc, larg asc, comp asc");
		task.setConsumer(consumer);
		task.start();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Blanqueta> list() {
		Session session = HibernateUtil.getSession();
		List<Blanqueta> list = session.createQuery("from Blanqueta as b "
				+ "inner join fetch b.blanquetaLona "
				+ "order by nome asc, larg asc, comp asc")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		session.close();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Blanqueta> fetch(String str) {
		if (str.length() == 0) {
			return new ArrayList<>();
		}
		
		Criterion criterion;
		
		// str contains numbers, fetch by larg and comp
		if (Character.isDigit(str.charAt(0))) {
			final String[] split = str.split("(?i)x");
			
			// if only contains one number, search LARG OR COMP
			if (split.length == 1) {
				int larg = Formatter.parseInt(split[0]);
				
				criterion = Restrictions.or(
						Restrictions.eq("larg", larg),
						Restrictions.eq("comp", larg));
			}
			// else, search for LARG AND COMP
			else { // lengh > 1
				int larg = Formatter.parseInt(split[0]);
				int comp = Formatter.parseInt(split[1]);
				
				criterion = Restrictions.and(
						Restrictions.eq("larg", larg),
						Restrictions.eq("comp", comp));
			}
		}
		// str contains words fetch by nome
		else {
			criterion = Restrictions.ilike("nome", str, MatchMode.ANYWHERE);
		}
		
		final Session session = HibernateUtil.getSession();
		final Criteria crit = session.createCriteria(Blanqueta.class);
		
		List<Blanqueta> list = crit.add(criterion)
				.addOrder(Order.asc("larg"))
				.addOrder(Order.asc("comp"))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		session.close();
		
		return list;
	}

}
