package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Chapa;
import com.marioconcilio.cardmanager.util.Formatter;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.ObservableList;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ChapaDAO extends AbstractDAO<Chapa> {

	@Override
	public void delete(Chapa entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listAll(Consumer<ObservableList<Chapa>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery("from Chapa as c "
				+ "inner join fetch c.chapaMarca as m "
				+ "order by m.marca, c.larg asc, c.comp asc");
		task.setConsumer(consumer);
		task.start();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Chapa> list() {
		Session session = HibernateUtil.getSession();
		List<Chapa> list = session.createQuery("from Chapa as c "
				+ "inner join fetch c.chapaMarca as m "
				+ "order by m.marca, c.larg asc, c.comp asc")
		.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
		.list();
		session.close();
		
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Chapa> fetch(String str) {
		final String[] split = str.split("(?i)x");
		if (split.length == 0) {
			return new ArrayList<>();
		}
		
		final Session session = HibernateUtil.getSession();
		final Criteria crit = session.createCriteria(Chapa.class);
		
		// if only contains one number, search LARG OR COMP
		if (split.length == 1) {
			int larg = Formatter.parseInt(split[0]);
			
			crit.add(Restrictions.or(
					Restrictions.eq("larg", larg),
					Restrictions.eq("comp", larg)));
		}
		// else, search for LARG AND COMP
		else { // lengh > 1
			int larg = Formatter.parseInt(split[0]);
			int comp = Formatter.parseInt(split[1]);
			
			crit.add(Restrictions.and(
					Restrictions.eq("larg", larg),
					Restrictions.eq("comp", comp)));
		}
		
		List<Chapa> list = crit.addOrder(Order.asc("larg"))
				.addOrder(Order.asc("comp"))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		session.close();
		
		return list;
	}

}
