package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Dolar;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.ObservableList;
import org.hibernate.Session;

import java.util.List;
import java.util.function.Consumer;

public class DolarDAO extends AbstractDAO<Dolar> {

	@Override
	public void delete(Dolar entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listAll(Consumer<ObservableList<Dolar>> consumer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Dolar> list() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Dolar get() {
		final Session session = HibernateUtil.getSession();
		final Dolar dolar = (Dolar) session.get(Dolar.class, 0L);
		session.close();
		
		return dolar;
	}

	@Override
	public List<Dolar> fetch(String str) {
		// TODO Auto-generated method stub
		return null;
	}

}
