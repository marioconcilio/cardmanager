package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Envelope;
import com.marioconcilio.cardmanager.util.Formatter;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.ObservableList;
import org.hibernate.Criteria;
import org.hibernate.NullPrecedence;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class EnvelopeDAO extends AbstractDAO<Envelope> {

	@Override
	public void delete(Envelope entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listAll(Consumer<ObservableList<Envelope>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery("from Envelope order by nome nulls first, tipo, larg, comp, grs");
		task.setConsumer(consumer);
		task.start();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Envelope> list() {
		Session session = HibernateUtil.getSession();
		Criteria crit = session.createCriteria(Envelope.class)
				.addOrder(Order.asc("nome").nulls(NullPrecedence.FIRST))
				.addOrder(Order.asc("tipo"))
				.addOrder(Order.asc("larg"))
				.addOrder(Order.asc("comp"));
		
		List<Envelope> list = crit.list();
		session.close();

		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Envelope> fetch(String str) {
		if (str.length() == 0) {
			return new ArrayList<>();
		}
		
		Criterion criterion;
	
		// str contains numbers, fetch by larg and comp
		if (Character.isDigit(str.charAt(0))) {
			final String[] split = str.split("(?i)x");
			
			// if only contains one number, search LARG OR COMP
			if (split.length == 1) {
				int larg = Formatter.parseInt(split[0]);
				
				criterion = Restrictions.or(
						Restrictions.eq("larg", larg),
						Restrictions.eq("comp", larg));
			}
			// else, search for LARG AND COMP
			else { // lengh > 1
				int larg = Formatter.parseInt(split[0]);
				int comp = Formatter.parseInt(split[1]);
				
				criterion = Restrictions.and(
						Restrictions.eq("larg", larg),
						Restrictions.eq("comp", comp));
			}
		}
		// str contains words fetch by nome
		else {
			criterion = Restrictions.ilike("nome", str, MatchMode.ANYWHERE);
		}
		
		final Session session = HibernateUtil.getSession();
		final Criteria crit = session.createCriteria(Envelope.class);
		
		List<Envelope> list = crit.add(criterion)
				.addOrder(Order.asc("nome").nulls(NullPrecedence.FIRST))
				.addOrder(Order.asc("larg"))
				.addOrder(Order.asc("comp"))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		session.close();
		
		return list;
	}

}
