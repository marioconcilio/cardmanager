package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.User;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.function.Consumer;

public class UserDAO extends AbstractDAO<User> {

	@Override
	public void update(User entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(User entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listAll(Consumer<ObservableList<User>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery("from User");
		task.setConsumer(consumer);
		task.start();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> list() {
		Session session = HibernateUtil.getSession();
		List<User> list = session.createQuery("from User")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		session.close();
		
		return list;
	}

	public void userFromLogin(String login, Consumer<User> consumer) {
		Task<User> task = new Task<User>() {

			@Override
			protected User call() throws Exception {
				Session session = HibernateUtil.getSession();
				Criteria crit = session.createCriteria(User.class)
						.add(Restrictions.eq("login", login));
				
				return (User) crit.uniqueResult();
			}
			
			@Override
			protected void succeeded() {
				consumer.accept(getValue());
			}
		};
		
		Thread th = new Thread(task);
		th.setDaemon(true);
		th.start();
	}

	@Override
	public List<User> fetch(String str) {
		// TODO Auto-generated method stub
		return null;
	}

}
