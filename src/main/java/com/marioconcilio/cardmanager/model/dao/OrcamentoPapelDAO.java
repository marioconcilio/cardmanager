package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Orcamento;
import com.marioconcilio.cardmanager.model.vo.OrcamentoPapel;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.ObservableList;
import org.hibernate.Criteria;
import org.hibernate.Session;

import java.util.List;
import java.util.function.Consumer;

public class OrcamentoPapelDAO extends AbstractDAO<OrcamentoPapel> {

	@Override
	public void update(OrcamentoPapel entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(OrcamentoPapel entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listAll(Consumer<ObservableList<OrcamentoPapel>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery(
				"from OrcamentoPapel as op " +
				"inner join fetch op.papel as p " +
				"inner join fetch p.papelTipo as t");
		task.setConsumer(consumer);
		task.start();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OrcamentoPapel> list() {
		Session session = HibernateUtil.getSession();
		List<OrcamentoPapel> list = session.createQuery(
					"from OrcamentoPapel as op " +
					"inner join fetch op.papel as p " +
					"inner join fetch p.papelTipo as t")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		session.close();
		
		return list;
	}
	
	public void listAllFromOrcamento(Orcamento o, Consumer<ObservableList<OrcamentoPapel>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery(
				"from OrcamentoPapel as op " +
				"inner join fetch op.papel as p " +
				"inner join fetch p.papelTipo as t " +
				"where op.orcamento = " + o.getId());
		task.setConsumer(consumer);
		task.start();
	}

	@Override
	public List<OrcamentoPapel> fetch(String str) {
		// TODO Auto-generated method stub
		return null;
	}

}
