package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Bill;
import com.marioconcilio.cardmanager.model.vo.Bill.BillStatus;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.ObservableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class BillDAO extends AbstractDAO<Bill> {

	private static final Logger logger = LogManager.getLogger(BillDAO.class);

	@Override
	public int updateAll(List<Bill> list) {
		Session session = HibernateUtil.getSession();
		Transaction tx = session.beginTransaction();
		int total = 0;

		try {
			int i = 0;
			for (Bill bill : list) {
				session.merge(bill);

				// JDBC batch size in hibernate.cfg.xml
				if(i % 20 == 0) {
					session.flush();
					session.clear();
				}

				i++;
			}

			tx.commit();
			total = list.size();
			logger.info("Batch updated Bill");
		}
		catch (ConstraintViolationException ex) {
			tx.rollback();

			for (Bill entity : list) {
				save(entity);
				total++;
			}
		}
		catch (HibernateException ex) {
			tx.rollback();

			logger.error("Error on batch update Bill", ex);	
			throw new HibernateException(ex);
		}
		finally {
			session.close();
		}

		return total;
	}

	@Override
	public void delete(Bill entity) {
		// TODO Auto-generated method stub

	}

	public Bill getBillFromNumber(long number) {
		Session session = HibernateUtil.getSession();
		Bill bill = (Bill) session.get(Bill.class, number);
		session.close();

		return bill;
	}

	@Override
	public void listAll(Consumer<ObservableList<Bill>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery("from Bill order by date desc");
		task.setConsumer(consumer);
		task.start();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Bill> list() {
		Session session = HibernateUtil.getSession();
		List<Bill> list = session.createQuery("from Bill")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		session.close();

		return list;
	}

	public void updateBillStatus(long number, BillStatus status) {
		Session session = HibernateUtil.getSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			Bill bill = (Bill) session.get(Bill.class, number);
			bill.setStatus(status);
			session.update(bill);

			tx.commit();
		}
		catch (HibernateException ex) {
			tx.rollback();
			logger.error("Error on updating Bill", ex);	
		}
		finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Bill> fetchBills(long number) {
		Session session = HibernateUtil.getSession();
		Bill bill = (Bill) session.get(Bill.class, number);

		if (bill != null) {
			final List<Bill> list = new ArrayList<>();
			list.add(bill);
			return list;
		}
		// bill not found
		// trying to fetch 000 000 pattern
		number *= 1000;
		long lo = number - 1;
		long hi = number + 1000;

		Criteria crit = session.createCriteria(Bill.class)
				.add(Restrictions.between("id", lo, hi));
		
		List<Bill> list = crit.list();
		session.close();

		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Bill> fetchBills(String name) {
		Session session = HibernateUtil.getSession();
		Criteria crit = session.createCriteria(Bill.class)
				.createAlias("client", "c")
				.add(Restrictions.ilike("c.name", name, MatchMode.ANYWHERE))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		List<Bill> list = crit.list();
		session.close();

		return list;
	}

	@Override
	public List<Bill> fetch(String str) {
		// TODO Auto-generated method stub
		return null;
	}

}
