package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Diversos;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.ObservableList;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.function.Consumer;

public class DiversosDAO extends AbstractDAO<Diversos> {

	@Override
	public void delete(Diversos entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listAll(Consumer<ObservableList<Diversos>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery("from Diversos order by nome");
		task.setConsumer(consumer);
		task.start();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Diversos> list() {
		Session session = HibernateUtil.getSession();
		List<Diversos> list = session.createQuery("from Diversos order by nome")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		session.close();

		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Diversos> fetch(String str) {
		Session session = HibernateUtil.getSession();
		Criteria crit = session.createCriteria(Diversos.class)
				.add(Restrictions.ilike("nome", str, MatchMode.ANYWHERE))
				.addOrder(Order.asc("nome"))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		List<Diversos> list = crit.list();
		session.close();
		
		return list;
	}

}
