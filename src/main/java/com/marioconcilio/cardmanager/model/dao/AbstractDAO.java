package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressIndicator;
import oracle.jrockit.jfr.jdkevents.throwabletransform.ConstructorTracerWriter;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import java.io.Serializable;
import java.util.List;
import java.util.function.Consumer;

public abstract class AbstractDAO<T extends Serializable> {
	
	private static final Logger logger = LogManager.getLogger(AbstractDAO.class);
	
	private ProgressIndicator progressIndicator;
	
	public abstract void delete(T entity);
	public abstract void listAll(Consumer<ObservableList<T>> consumer);
	public abstract List<T> list();
	public abstract List<T> fetch(String str);
	
	public void save(T entity) throws HibernateException {
		final Session session = HibernateUtil.getSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			session.save(entity);
			tx.commit();
			
			logger.info("Saved {}", entity);
		}
		catch(HibernateException ex) {
			tx.rollback();
			
			logger.error("Error saving " + entity, ex);
			throw new HibernateException(ex);
		}
		finally {
			session.close();
		}
	}
	
	public void update(T entity) throws Exception {
		final Session session = HibernateUtil.getSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			session.update(entity);
			tx.commit();
			
			logger.info("Updated {}", entity);
		}
		catch (HibernateException ex) {
			tx.rollback();
			
			logger.error("Error updating " + entity, ex);
			throw new Exception(ex);
		}
		finally {
			session.close();
		}
	}
	
	public int saveAll(List<T> list) throws HibernateException {
		final Session session = HibernateUtil.getSession();
		Transaction tx = null;
		int total = 0;

		try {
			tx = session.beginTransaction();
			
			int i = 0;
			for (T entity : list) {
				session.save(entity);
				
				// JDBC batch size in hibernate.cfg.xml
				if(i++ % 20 == 0) {
					session.flush();
					session.clear();
				}
			}
			
			tx.commit();
			total = list.size();
			logger.info("Batch saved {}", list.get(0).getClass().getSimpleName());
		}
		catch (ConstraintViolationException ex) {
			tx.rollback();
			logger.info("{} already saved.", list.get(0).getClass().getSimpleName());
			throw ex;
		}
		catch (HibernateException ex) {
			tx.rollback();
			logger.error("Error on batch saving " + list.get(0).getClass().getSimpleName(), ex);
			throw ex;
		}
		finally {
			session.close();
		}
		
		return total;
	}
	
	public int updateAll(List<T> list) throws HibernateException {
		final Session session = HibernateUtil.getSession();
		Transaction tx = null;
		int total = 0;

		try {
			tx = session.beginTransaction();
			
			int i = 0;
			for (T entity : list) {
				session.update(entity);
				
				// JDBC batch size in hibernate.cfg.xml
				if(i++ % 20 == 0) {
					session.flush();
					session.clear();
				}
			}
			
			tx.commit();
			total = list.size();
			logger.info("Batch updated {}", list.get(0).getClass().getSimpleName());
		}
		catch (ConstraintViolationException ex) {
			tx.rollback();
			
			for (T entity : list) {
				save(entity);
				total++;
			}
		}
		catch (HibernateException ex) {
			tx.rollback();
			logger.error("Error on batch update " + list.get(0).getClass().getSimpleName(), ex);	
			throw ex;
		}
		finally {
			session.close();
		}
		
		return total;
	}
	
	public ProgressIndicator getProgressIndicator() {
		return progressIndicator;
	}

	public void setProgressIndicator(ProgressIndicator progressIndicator) {
		this.progressIndicator = progressIndicator;
	}
	
	/**
	 * Class that query db on background, animatins progress indicator
	 * @author Mario Concilio
	 */
	protected class ListAllTask extends Task<ObservableList<T>> {
		
		private Consumer<ObservableList<T>> consumer;
		private String query;
		
		public ListAllTask() {
			if (progressIndicator != null)
				progressIndicator.progressProperty().bind(this.progressProperty());
		}

		public ListAllTask(String query, Consumer<ObservableList<T>> consumer) {
			this();
			this.query = query;
			this.consumer = consumer;
		}
		
		@Override
		@SuppressWarnings("unchecked")
		protected ObservableList<T> call() throws Exception {
			Session session = HibernateUtil.getSession();
			List<T> list = session.createQuery(query)
					.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
					.list();
			session.close();
			
			return FXCollections.observableList(list);
		}
		
		@Override
		protected void succeeded() {
			consumer.accept(getValue());
		}
		
		public void start() {
			this.exceptionProperty().addListener((observable, oldValue, newValue) -> {
				if (newValue != null) {
					logger.catching(Level.FATAL, newValue);
					newValue.printStackTrace();
				}
			});
			
			Thread thread = new Thread(this);
			thread.setDaemon(true);
			thread.start();
		}

		public Consumer<ObservableList<T>> getConsumer() {
			return consumer;
		}

		public void setConsumer(Consumer<ObservableList<T>> consumer) {
			this.consumer = consumer;
		}

		public String getQuery() {
			return query;
		}

		public void setQuery(String query) {
			this.query = query;
		}
		
	}
}
