package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Client;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.ObservableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.function.Consumer;

public class ClientDAO extends AbstractDAO<Client> {
	
	private static final Logger logger = LogManager.getLogger();

	@Override
	public void update(Client entity) {
		Session session = HibernateUtil.getSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			session.saveOrUpdate(entity);
//			session.update(entity);
			tx.commit();
			
			logger.info("Saving {}", entity.toString());
		}
		catch(HibernateException ex) {
			tx.rollback();
			ex.printStackTrace();
		}
		finally {
			session.close();
		}
	}

	@Override
	public void delete(Client entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listAll(Consumer<ObservableList<Client>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery("from Client order by name asc");
		task.setConsumer(consumer);
		task.start();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Client> list() {
		Session session = HibernateUtil.getSession();
		List<Client> list = session.createQuery("from Client order by name asc")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		session.close();
		
		return list;
	}
	
	public Client getClientFromCnpj(long cnpj) {
		Session session = HibernateUtil.getSession();
		Client client = (Client) session.get(Client.class, cnpj);
		session.close();
		
		return client;
	}
	
	@SuppressWarnings("unchecked")
	public List<Client> fetchClient(String name) {
		Session session = HibernateUtil.getSession();
		Criteria crit = session.createCriteria(Client.class)
				.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		return crit.list();
	}

	@Override
	public List<Client> fetch(String str) {
		// TODO Auto-generated method stub
		return null;
	}

}
