package com.marioconcilio.cardmanager.model.dao;

import com.marioconcilio.cardmanager.model.vo.Papel;
import com.marioconcilio.cardmanager.model.vo.PapelTipo;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import javafx.collections.ObservableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.function.Consumer;

public class PapelDAO extends AbstractDAO<Papel> {
	
	private static final Logger logger = LogManager.getLogger();

	@Override
	public void save(Papel entity) {
		Session session = HibernateUtil.getSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			
			PapelTipo tipo = (PapelTipo) session.get(PapelTipo.class, new Long(entity.getPapelTipo().getId()));
			entity.setPapelTipo(tipo);
			
			session.save(entity);
			tx.commit();
			
			logger.info("Saving {}", entity.toString());
		}
		catch(HibernateException ex) {
			tx.rollback();
			ex.printStackTrace();
		}
		finally {
			session.close();
		}
	}

	@Override
	public void update(Papel entity) {
		Session session = HibernateUtil.getSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			
			PapelTipo tipo = (PapelTipo) session.get(PapelTipo.class, new Long(entity.getPapelTipo().getId()));
			entity.setPapelTipo(tipo);
			
			session.update(entity);
			tx.commit();
			
			logger.info("Updating {}", entity);
		}
		catch(HibernateException ex) {
			tx.rollback();
			ex.printStackTrace();
		}
		finally {
			session.close();
		}
	}

	@Override
	public void delete(Papel entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void listAll(Consumer<ObservableList<Papel>> consumer) {
		ListAllTask task = new ListAllTask();
		task.setQuery(
				"from Papel as p " +
				"inner join fetch p.papelTipo as t " +
				"order by t.tipo, p.grs, p.largura, p.fls, p.qtde");
		task.setConsumer(consumer);
		task.start();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Papel> list() {
		Session session = HibernateUtil.getSession();
		List<Papel> list = session.createQuery(
						"from Papel as p " +
						"inner join fetch p.papelTipo as t " +
						"order by t.tipo, p.grs, p.largura, p.fls, p.qtde")
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		session.close();
		
		return list;
	}

	@Override
	public List<Papel> fetch(String str) {
		// TODO Auto-generated method stub
		return null;
	}

}
