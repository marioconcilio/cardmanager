package com.marioconcilio.cardmanager.scene.control;

import com.marioconcilio.cardmanager.util.Formatter;
import javafx.scene.control.DatePicker;
import javafx.scene.input.KeyCode;

import java.util.Date;

public class DateEditableCell extends AbstractEditableCell<Date> {

	@Override
	protected void createControl() {
		control = new DatePicker();
		DatePicker dp = (DatePicker) control;
		dp.setValue(Formatter.toLocalDate(getItem()));
		dp.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
		dp.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.ENTER) {
				commitEdit(Formatter.toDate(dp.getValue()));
			} else if (event.getCode() == KeyCode.ESCAPE) {
				cancelEdit();
			}
		});
	}

	@Override
	protected String getString() {
		return Formatter.formatDate(getItem());
	}

}
