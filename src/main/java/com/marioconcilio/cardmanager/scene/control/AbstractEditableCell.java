package com.marioconcilio.cardmanager.scene.control;

import com.marioconcilio.cardmanager.model.vo.CieloCard;
import javafx.scene.control.*;

public abstract class AbstractEditableCell<T> extends TableCell<CieloCard, T> {

	protected Control control;

	@Override
	public void startEdit() {
		super.startEdit();

		if (control == null) {
			createControl();
		}

		setGraphic(control);
		setContentDisplay(ContentDisplay.GRAPHIC_ONLY); 

		if (control instanceof TextField)
			((TextField) control).selectAll();
	}

	@Override
	public void cancelEdit() {
		super.cancelEdit();

		setText(String.valueOf(getItem()));
		setContentDisplay(ContentDisplay.TEXT_ONLY);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateItem(T item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} 
		else {
			if (isEditing()) {
				if (control != null) {
					if (control instanceof TextField)
						((TextField) control).setText(getString());
					else
						((ComboBoxBase<T>) control).setValue(item);
				}

				setGraphic(control);
				setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
			} 
			else {
				setText(getString());
				setContentDisplay(ContentDisplay.TEXT_ONLY);
			}
		}
	}

	protected abstract void createControl();
	protected abstract String getString();
}
