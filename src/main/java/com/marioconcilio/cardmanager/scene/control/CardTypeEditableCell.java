package com.marioconcilio.cardmanager.scene.control;

import com.marioconcilio.cardmanager.model.vo.Card.CieloCardType;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyCode;

public class CardTypeEditableCell extends AbstractEditableCell<CieloCardType> {

	@SuppressWarnings("unchecked")
	@Override
	protected void createControl() {
		control = new ComboBox<CieloCardType>();
		ComboBox<CieloCardType> cb = (ComboBox<CieloCardType>) control;
		
		ObservableList<CieloCardType> list = FXCollections.observableArrayList(CieloCardType.values());
		cb.setItems(list);
		cb.setValue(getItem());
        cb.setMinWidth(this.getWidth() - this.getGraphicTextGap()*2);
        cb.setOnKeyPressed(event -> {
        	if (event.getCode() == KeyCode.ENTER) {
                commitEdit(cb.getValue());
            } 
            else if (event.getCode() == KeyCode.ESCAPE) {
                cancelEdit();
            }
        });
	}

	@Override
	protected String getString() {
		return getItem().toString();
	}

}
