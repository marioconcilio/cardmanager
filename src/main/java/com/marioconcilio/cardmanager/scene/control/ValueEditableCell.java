package com.marioconcilio.cardmanager.scene.control;

import com.marioconcilio.cardmanager.util.Formatter;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;

import java.math.BigDecimal;

public class ValueEditableCell extends AbstractEditableCell<BigDecimal> {

	@Override
	protected void createControl() {
		control = new TextField(getString());
        control.setMinWidth(this.getWidth() - this.getGraphicTextGap()*2);
        control.setOnKeyPressed(event -> {
        	if (event.getCode() == KeyCode.ENTER) {
                commitEdit(new BigDecimal(((TextField) control).getText()));
            } 
            else if (event.getCode() == KeyCode.ESCAPE) {
                cancelEdit();
            }
        });
	}

	@Override
	protected String getString() {
		return Formatter.formatCurrency(getItem());
	}

}
