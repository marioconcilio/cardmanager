package com.marioconcilio.cardmanager.report;

import javafx.concurrent.Task;
import javafx.scene.control.ProgressIndicator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.function.Consumer;

public abstract class AbstractReport extends Task<File> {
	
	private static final Logger logger = LogManager.getLogger();

	private Consumer<File> consumer;
	private ProgressIndicator progressIndicator;
	
	public void generate(Consumer<File> consumer) {
		this.consumer = consumer;
		
		this.exceptionProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				logger.catching(Level.FATAL, newValue);
			}
		});
		
		Thread thread = new Thread(this);
		thread.setDaemon(true);
		thread.start();
	}
	
	@Override
	protected void succeeded() {
		consumer.accept(getValue());
	}
	
	/*
	 * Setters
	 */
	public void setProgressIndicator(ProgressIndicator progressIndicator) {
		this.progressIndicator = progressIndicator;
		this.progressIndicator.progressProperty().bind(progressProperty());
	}

}
