package com.marioconcilio.cardmanager.report;

import com.marioconcilio.cardmanager.util.Formatter;
import com.marioconcilio.cardmanager.util.HibernateUtil;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimplePrintServiceExporterConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Date;

public class EstoquePapelReport extends AbstractReport {
	
	private static final Logger logger = LogManager.getLogger();

	@Override
	protected File call() throws Exception {
		logger.info("Generating report");
		
		StringBuilder filename = new StringBuilder();
		filename.append("Relatorios/Estoque/EstoquePapel-")
		.append(Formatter.formatDate(new Date()).replaceAll("/", "-"))
		.append(".pdf");
		
		Connection conn = HibernateUtil.getConnection();
		InputStream is = getClass().getResourceAsStream("/resources/reports/EstoquePapel.jasper");
//		OutputStream os = new FileOutputStream("//192.168.1.27/Publico/Orcamento.pdf");
		
		DefaultJasperReportsContext context = DefaultJasperReportsContext.getInstance();
		JRPropertiesUtil.getInstance(context).setProperty("net.sf.jasperreports.xpath.executer.factory",
			    "net.sf.jasperreports.engine.util.xml.JaxenXPathExecuterFactory");
		
//		JasperReport report = JasperCompileManager.compileReport(is);
		JasperReport report = (JasperReport) JRLoader.loadObject(is);
		JasperPrint print = JasperFillManager.fillReport(report, null, conn);
		JasperExportManager.exportReportToPdfFile(print, filename.toString());
//		JasperExportManager.exportReportToPdfStream(print, os);
		
//		PrinterJob printerJob = PrinterJob.getPrinterJob();
//		PageFormat pageFormat = printerJob.defaultPage();

        PrintRequestAttributeSet att = new HashPrintRequestAttributeSet();
        att.add(MediaSizeName.ISO_A4);
        att.add(new Copies(1));
        att.add(OrientationRequested.PORTRAIT);
//        att.add(new MediaPrintableArea(5.29f, 7.05f, 137.94f, 195.80f, MediaPrintableArea.MM));
        
        SimplePrintServiceExporterConfiguration configuration = new SimplePrintServiceExporterConfiguration();
        configuration.setPrintRequestAttributeSet(att);
        configuration.setDisplayPageDialog(false);
        configuration.setDisplayPrintDialog(false);
        
        JRPrintServiceExporter exporter = new JRPrintServiceExporter();
        exporter.setExporterInput(new SimpleExporterInput(print));
        exporter.setConfiguration(configuration);
        
        // TODO: uncomment to print report
        logger.info("Printing report");
        exporter.exportReport();
        logger.info("Printing finished");
		
		return null;
	}

}
