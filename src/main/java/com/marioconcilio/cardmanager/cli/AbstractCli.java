package com.marioconcilio.cardmanager.cli;

import org.apache.commons.cli.Option;

import java.io.IOException;

import static com.marioconcilio.cardmanager.cli.ConstantsCli.*;
import static java.lang.System.out;

public abstract class AbstractCli {
	
	protected MyBufferedReader br;
	protected final Option help;
	
	public AbstractCli() {
		help = Option.builder("h")
				.longOpt("help")
				.desc("essa mensagem de ajuda")
				.build();
	}
	
	public boolean confirm(String message) throws IOException {
		boolean confirmed = false;
		String input;
		
		do {
			out.print(ANSI_YELLOW);
			out.print(message);
			out.print(ANSI_RESET);
			
			input = br.readLine();
			if (input.equals(CHAR_YES)) {
				confirmed = true;
			}
			
		} while (!input.equals(CHAR_YES) && !input.equals(CHAR_NO));
		
		return confirmed;
	}

}
