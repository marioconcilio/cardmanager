package com.marioconcilio.cardmanager.cli;

public final class ConstantsCli {
	
	public static final String ANSI_RESET;
	public static final String ANSI_BLACK;
	public static final String ANSI_RED;
	public static final String ANSI_GREEN;
	public static final String ANSI_BLUE;
	public static final String ANSI_ORANGE;
	public static final String ANSI_YELLOW;
	
	public static final String DOUBLE_LINE		= "==============================================================";
	public static final String LINE				= "--------------------------------------------------------------";
	
	public static final String CHAR_DEBIT		= "d";
	public static final String CHAR_CREDIT		= "c";
	public static final String CHAR_VISA			= "v";
	public static final String CHAR_MASTER		= "m";
	public static final String CHAR_ELO			= "e";
	public static final String CHAR_YES 			= "y";
	public static final String CHAR_NO 			= "n";
	public static final String CHAR_QUIT	 		= "q";
	public static final String CHAR_HELP			= "h";
	public static final String CHAR_LIST			= "l";
	public static final String CHAR_NEWDATE		= "n";
	public static final String CHAR_PROCESS		= "p";
	
	static {
		final char escape 		= '\033';
		final String foreground 	= "[38;5;";
		
		ANSI_RESET 	= escape + "[0m";
		ANSI_BLACK 	= escape + foreground + "000m";
		ANSI_ORANGE 	= escape + foreground + "208m";
		ANSI_RED 		= escape + foreground + "196m";
		ANSI_GREEN 	= escape + foreground + "082m";
		ANSI_BLUE 		= escape + foreground + "033m";
		ANSI_YELLOW 	= escape + foreground + "184m";
	}

}
