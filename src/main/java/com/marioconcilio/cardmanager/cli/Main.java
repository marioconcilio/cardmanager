package com.marioconcilio.cardmanager.cli;

import com.marioconcilio.cardmanager.model.dao.UserDAO;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;

import static com.marioconcilio.cardmanager.cli.ConstantsCli.*;
import static java.lang.System.out;

public final class Main {
	
	enum MainMenu {
		MODERN("modern", "PagSeguto cards"),
		CIELO("cielo", "Cielo cards"),
		BILL("bill", "Verify bills"),
		STOCK("stock", "Products in stock"),
		HELP("help", "Show this menu");
		
		private String cmd;
		private String description;
		
		MainMenu(String cmd, String description) {
			this.cmd = cmd;
			this.description = description;
		}
		
		public String getDescription() {
			return description;
		}
		
		public String getCmd() {
			return cmd;
		}
		
		@Override
		public String toString() {
			return cmd;
		}
	}
	
	public static void showHelp() {
		final MainMenu[] values = MainMenu.values();
		for (MainMenu menu : values) {
			String str = String.format("  %s \t%s", menu.getCmd(), menu.getDescription());
			out.println(str);
		}
	}

	public static void main(String[] args) {
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.OFF);
		new UserDAO().list();
		
//		out.print("\033[H\033[2J");
//		out.flush();
		out.println(LINE);
		
		try (final MyBufferedReader br = new MyBufferedReader(new InputStreamReader(System.in))) {
			out.println(com.marioconcilio.cardmanager.util.Constants.APP_NAME + " " + com.marioconcilio.cardmanager.util.Constants.APP_VERSION);
			out.println("Type \"help\" for more information.");
			
			String input = "";
			
			while (true) {
				out.print(ANSI_BLUE + "masterprint> " + ANSI_RESET);
				input = br.readLine();
				
				if (input.equals(MainMenu.CIELO.toString())) {
					final CieloCli cli = new CieloCli(br);
					cli.execute();
				}
				else if (input.equals(MainMenu.MODERN.toString())) {
					final PSMCli cli = new PSMCli(br);
					cli.mainMenu();
				}
				else if (input.equals(MainMenu.BILL.toString())) {
					final BillCli cli = new BillCli(br);
					cli.mainMenu();
				}
				else if (input.equals(MainMenu.STOCK.toString())) {
					final StockCli cli = new StockCli(br);
					cli.mainMenu();
				}
				else if (input.equals(MainMenu.HELP.toString())) {
					showHelp();
				}
			}

		}
		catch (IOException ex) {
			ex.printStackTrace();
			System.exit(1);
		}
	}

}
