package com.marioconcilio.cardmanager.cli;

import com.marioconcilio.cardmanager.model.dao.BillDAO;
import com.marioconcilio.cardmanager.model.vo.Bill;
import com.marioconcilio.cardmanager.model.vo.Bill.BillStatus;
import com.marioconcilio.cardmanager.util.Formatter;
import org.apache.commons.cli.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.marioconcilio.cardmanager.cli.ConstantsCli.*;
import static java.lang.System.out;

public class BillCli extends AbstractCli {

	private static final String CMD_END				= "end";

	private List<Bill> list;

	public BillCli(MyBufferedReader br) {
		this.br = br;
		list = new ArrayList<>();
	}

	public void mainMenu() throws IOException {
		final Options options = new Options();
		options.addOption("list", "listar boletos");
		options.addOption("update", "atualizar situação dos boletos");
		options.addOption(help);

		String input;

		while (true) {
			out.print(ANSI_BLUE);
			out.print("bill@masterprint> ");
			out.print(ANSI_RESET);

			input = br.readLine();
			if (input.equals("h") || input.equals("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("bill", options);
			}

			if (input.contains("list")) {
				listAll(input);
			}

			if (input.contains("update")) {
				updateMenu(input);
			}
		}
	}

	public void listAll(String cmd) {
		final String[] args = cmd.split("\\s+");
		final Options options = new Options();
		options.addOption("ab", "aberto", false, "boletos em aberto");
		options.addOption("ca", "cartorio", false, "boletos em cartorio");
		options.addOption("pg", "pago", false, "boletos pagos");
		options.addOption("bx", "baixa", false, "boletos baixados/protestados");
		options.addOption("vc", "venc", false, "boletos vencidos");
		options.addOption("ou", "outro", false, "boletos em situação desconhecida");
		options.addOption("ok", "boletos já conferidos");
		options.addOption("not", "notok", false, "boletos ainda não confirmados");
		options.addOption(help);
		options.addOption(Option.builder("f")
				.longOpt("find")
				.desc("procurar boleto pelo número/nome do cliente")
				.hasArg()
				.build());

		try {
			final CommandLineParser parser = new DefaultParser();
			final CommandLine line = parser.parse(options, args);
			if (line.hasOption("h")) {
				final HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("list", options);
				return;
			}

			List<Bill> list;
			if (line.hasOption("f")) {
				final String str = line.getOptionValue("f");
				final Long number = Formatter.parseLong(str);
				
				// fetch by bill number
				if (number > -1) {
					list = new BillDAO()
							.fetchBills(number)
							.stream()
							.sorted(this::sortBillNumber)
							.collect(Collectors.toList());
				}
				// fetch by client name
				else {
					list = new BillDAO()
							.fetchBills(str)
							.stream()
							.sorted(this::sortBillNumber)
							.collect(Collectors.toList());
				}
			}
			else {
				list = new BillDAO()
						.list()
						.stream()
						.filter(bill -> {
							if (line.hasOption("ab")) return bill.isAberto();
							if (line.hasOption("ca")) return bill.isCartorio();
							if (line.hasOption("pg")) return bill.isLiquidado();
							if (line.hasOption("bx")) return bill.isProtestado();
							if (line.hasOption("vc")) return bill.isVencido();
							if (line.hasOption("ou")) return bill.isOutro();
							if (line.hasOption("ok")) return bill.isOk();
							if (line.hasOption("not")) return !bill.isOk();

							return true;
						})
						.sorted(this::sortBillNumber)
						.collect(Collectors.toList());
			}

			printBills(list);
		}
		catch (ParseException ex) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("list", options);
		}
	}

	private void printBills(List<Bill> list) {
		if (list.isEmpty()) {
			out.println("empty list!");
			return;
		}

		list.forEach(bill -> {
			if (bill.isProtestado()) {
				out.print(ANSI_RED);
			}
			else if (bill.isCartorio()) {
				out.print(ANSI_ORANGE);
			}
			else if (bill.isLiquidado()) {
				out.print(ANSI_GREEN);
			}
			else if (bill.isAberto() && bill.isVencido()) {
				out.print(ANSI_YELLOW);
			}

			if (bill.isOk()) {
				out.println(": " + bill);
			}
			else {
				out.println(": " + bill + " *");
			}

			out.print(ANSI_RESET);
		});
	}

	public void updateMenu(String cmd) throws IOException {
		final String[] args = cmd.split("\\s+");
		final OptionGroup group = new OptionGroup();
		group.setRequired(true);
		group.addOption(Option.builder("ab")
				.longOpt("aberto")
				.desc("atualizar estado para ABERTO")
				.build());

		group.addOption(Option.builder("ca")
				.longOpt("cartorio")
				.desc("atualizar estado para CARTORIO")
				.build());

		group.addOption(Option.builder("pg")
				.longOpt("pago")
				.desc("atualizar estado para LIQUIDADO")
				.build());

		group.addOption(Option.builder("bx")
				.longOpt("baixa")
				.desc("atualizar estado para BAIXADO/PROTESTADO")
				.build());

		group.addOption(help);

		final Options options = new Options();
		options.addOptionGroup(group);

		try {
			final CommandLineParser parser = new DefaultParser();
			final CommandLine line = parser.parse(options, args);
			if (line.hasOption("h")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("update", options);
				return;
			}

			BillStatus status = BillStatus.OUTRO;
			if (line.hasOption("ab")) {
				status = BillStatus.ABERTO;
			}
			if (line.hasOption("ca")) {
				status = BillStatus.CARTORIO;
			}
			if (line.hasOption("pg")) {
				status = BillStatus.PAGO;
			}
			if (line.hasOption("bx")){
				status = BillStatus.PROTESTADO;
			}

			String input;
			while (true) {
				out.print(ANSI_BLUE);
				out.print(String.format("bill@masterprint:update-%s> ", status.getOpt()));
				out.print(ANSI_RESET);
				input = br.readLine();

				if (input.equals(CMD_END)) {
					list.clear();
					return;
				}

				if (input.equals("process")) {
					process();
				}

				long number = Formatter.parseLong(input);
				if (number > 0) {
					Bill bill = new BillDAO().getBillFromNumber(number);

					if (bill == null) {
						out.println("bill not found!");
					}
					else {
						bill.setStatus(status);
						confirmBill(bill);
					}
				}
			}
		}
		catch (ParseException ex) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("update", options);
		}
	}

	private void confirmBill(Bill bill) throws IOException {
		final String message = String.format("%s (y/n)? ", bill);
		if (confirm(message)) {
			list.add(bill);
		}
	}

	private void process() throws IOException {
		if (list.isEmpty()) {
			out.println("list is empty");
			return;
		}

		printBills(list);
		final String message = String.format("process %d bills (y/n)? ", list.size());
		if (confirm(message)) {
			out.println("processing");

			final BillDAO dao = new BillDAO();
			try {
				dao.updateAll(list);
				list.clear();
			}
			catch (Exception e) {
				final String msg = e.getMessage();

				out.print(ANSI_RED);
				out.println("error on updating bills");
				if (msg != null)
					out.println(msg);

				out.print(ANSI_RESET);
			}
		}
	}

	/*
	 * Helper
	 */

	public int sortBillNumber(Bill b1, Bill b2) {
		long num1 = b1.getId();
		long num2 = b2.getId();

		if (num1 / 1000 == 0) num1 *= 1000;
		if (num2 / 1000 == 0) num2 *= 1000;

		if (num1 > num2) return 1;
		if (num1 < num2) return -1;

		return 0;
	}

}
