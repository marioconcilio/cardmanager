package com.marioconcilio.cardmanager.cli;

import com.marioconcilio.cardmanager.controller.PSMController;
import com.marioconcilio.cardmanager.model.dao.PSMCardDAO;
import com.marioconcilio.cardmanager.model.vo.Card.PSMCardType;
import com.marioconcilio.cardmanager.model.vo.PSMCard;
import com.marioconcilio.cardmanager.util.DateUtils;
import org.apache.commons.cli.*;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.marioconcilio.cardmanager.cli.ConstantsCli.*;
import static java.lang.System.out;

public class PSMCli extends AbstractCli {

	private static final String CMD_LIST			= "list";

	private static final String CMD_IMPORT			= "import";
	private static final String CMD_UPDATE			= "update";
	private static final String CMD_END				= "end";
	private static final String CMD_PROCESS 		= "process";
	private static final String CMD_HELP			= "help";

	public static final String CMD_DEBIT			= "d";
	public static final String CMD_CV				= "cv";
	public static final String CMD_CP				= "cp";

	private List<PSMCard> list;

	public PSMCli(MyBufferedReader br) {
		this.br = br;
		list = new ArrayList<>();
	}

	public void mainMenu() throws IOException {
		String input;

		while (true) {
			out.print(ANSI_BLUE);
			out.print("modern@masterprint> ");
			out.print(ANSI_RESET);

			input = br.readLine();

			if (input.contains(CMD_HELP)) {
				out.println("modern main menu");
			}

			if (input.contains(CMD_LIST)) {
				listAll(input);
			}

			if (input.contains(CMD_IMPORT)) {
				importFile(input);
			}

			if (input.contains(CMD_UPDATE)) {
				updateMenu();
			}
		}
	}

	public void updateMenu() throws IOException {
		String input;
		String[] splitted = null;

		Date date = null;
		PSMCardType cardType = null;
		BigDecimal value = null;

		while (true) {
			out.print(ANSI_BLUE + "modern@masterprint:update> " + ANSI_RESET);
			input = br.readLine();

			// check if string contains digit in first position
			if (input.matches("[0-9].*")) {
				splitted = input.split("\\s+");

				if (splitted.length == 3) {
					final String dayStr = splitted[0];
					final String typeStr = splitted[1];
					final String valueStr = splitted[2];

					date = parseDate(dayStr);
					cardType = parseCardType(typeStr);
					value = parseValue(valueStr);

					if (date != null && cardType != null && value != null) {
						final PSMCard card = new PSMCard();
						card.setCardType(cardType);
						card.setDescription(cardType.toString());
						card.setDate(date);
						card.setValue(value);

						confirmCard(card);
					}
				}
			}
			else {
				if (input.contains(CMD_HELP)) {
					out.println("modern main menu");
				}
				else if (input.equals(CMD_LIST)) {
					printCards(list);
				}
				else if (input.equals(CMD_PROCESS)) {
					process();
				}
				else if (input.equals(CMD_END)) {
					list.clear();
					return;
				}
			}	
		}
	}

	public void importFile(String cmd) {
		String[] splitted = cmd.split("\\s+");
		if (splitted.length < 2) {
			out.println("missing filename");
			out.println("usage: import <filename>");
			return;
		}

		final String filename = splitted[1];
		final PSMController controller = new PSMController();
		final File file = new File(filename);

		try {
			List<PSMCard> list = controller.processExtrato(file);
			out.println("cards found in file:");
			printCards(list);

			out.println("updating database...");

			final PSMCardDAO dao = new PSMCardDAO();
			dao.saveAll(list);
		} 
		catch (Exception e) {
			e.printStackTrace();
		} 

	}

	public void listAll(String cmd) {
		final String[] args = cmd.split("\\s+");
		final Options options = new Options();
		options.addOption("ok", "cartões já confirmados");
		options.addOption("not", "notok", false, "cartões ainda não confirmados");
		options.addOption("d", "debito", false, "somente carões de débito");
		options.addOption("cv", "cvista", false, "somente cartões de crédito a vista");
		options.addOption("cp", "cparc", false, "somente cartões de crédito parcelado");
		options.addOption(help);
		options.addOption(Option.builder("f")
				.longOpt("find")
				.desc("procurar cartão pela data/valor")
				.hasArg()
				.build());

		try {
			final CommandLineParser parser = new DefaultParser();
			final CommandLine line = parser.parse(options, args);
			if (line.hasOption("h")) {
				final HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("list", options);
				return;
			}

			List<PSMCard> list;
			if (line.hasOption("f")) {
				final String str = line.getOptionValue("f");
				final Date date = parseDate(str);

				// fetch by date
				if (date != null) {
					list = new PSMCardDAO().fetchCards(date);
				}
				// fetch by value
				else {
					final BigDecimal value = parseValue(str);
					if (value != null) {
						list = new PSMCardDAO().fetchCards(value);
					}
					else {
						list = new ArrayList<>();
					}
				}
			}
			else {
				list = new PSMCardDAO()
						.list()
						.stream()
						.filter(card -> {
							if (line.hasOption("ok")) return card.isOk();
							if (line.hasOption("not")) return !card.isOk();
							if (line.hasOption("d")) return card.getCardType().equals(PSMCardType.DEBITO);
							if (line.hasOption("cv")) return card.getCardType().equals(PSMCardType.CREDITO_AVISTA);
							if (line.hasOption("cp")) return card.getCardType().equals(PSMCardType.CREDITO_PARCELADO);

							return true;

						}).collect(Collectors.toList());
			}

			printCards(list);
		}
		catch (ParseException e) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("list", options);
		}
	}

	public void printCards(List<PSMCard> list) {		
		if (list.isEmpty()) {
			out.println("empty list!");
			return;
		}

		list.forEach(card -> {
			if (card.isOk()) {
				out.print(ANSI_GREEN);
			}

			out.println(": " + card);
			out.print(ANSI_RESET);
		});

	}

	public void confirmCard(PSMCard card) throws IOException {
		final String message = String.format("%s (y/n)? ", card);
		if (confirm(message)) {
			list.add(card);
		}
	}

	public void process() throws IOException {
		final int size = list.size();
		if (size == 0) {
			out.println("list is empty");
			return;
		}

		final String message = String.format("process %d cards (y/n)? ", size);
		if (confirm(message)) {
			out.println("processing");

			final PSMController controller = new PSMController();
			final List<PSMCard> found = controller.processList(list);
			final List<PSMCard> notFound = controller.getNotFound();
			final PSMCardDAO dao = new PSMCardDAO();

			out.print(ANSI_GREEN);
			out.println(LINE);
			out.println("CARDS FOUND");
			found.forEach(c -> out.println(": " + c));

			out.print(ANSI_RED);
			out.println(LINE);
			out.println("CARDS NOT FOUND");
			notFound.forEach(c -> out.println(": " + c));
			out.print(ANSI_RESET);

			try {
				dao.updateAllOK(controller.getEntities());
				list.clear();
			}
			catch (Exception e) {
				final String msg = e.getMessage();

				out.print(ANSI_RED);
				out.println("error on updating cards");
				if (msg != null)
					out.println(msg);

				out.print(ANSI_RESET);
			}
		}
	}

	/*
	 * helper methods
	 */

	public Date parseDate(String str) {
		Date date = null;
		String[] splitted = str.split("/");

		try {
			int day = Integer.parseInt(splitted[0]);
			int month = Integer.parseInt(splitted[1]);
			int year = Integer.parseInt(splitted[2]);
			year = (year / 1000) > 0? year : year + 2000;

			LocalDate localDate = LocalDate.of(year, month, day);
			date = DateUtils.asDate(localDate);
		}
		catch (NumberFormatException | DateTimeException | ArrayIndexOutOfBoundsException ex) {}

		return date;
	}

	public BigDecimal parseValue(String str) {
		BigDecimal value = null;

		try {
			value = new BigDecimal(str);
		}
		catch (NumberFormatException ex) {}

		return value;
	}

	public PSMCardType parseCardType(String str) {
		PSMCardType cardType = null;

		if (str.equals(CMD_DEBIT)) {
			cardType = PSMCardType.DEBITO;
		}
		else if (str.equals(CMD_CV)) {
			cardType = PSMCardType.CREDITO_AVISTA;
		}
		else if (str.equals(CMD_CP)) {
			cardType = PSMCardType.CREDITO_PARCELADO;
		}

		return cardType;
	}

}
