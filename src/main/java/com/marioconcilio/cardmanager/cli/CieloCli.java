package com.marioconcilio.cardmanager.cli;

import com.marioconcilio.cardmanager.controller.CieloController;
import com.marioconcilio.cardmanager.model.vo.Card.CieloCardType;
import com.marioconcilio.cardmanager.model.vo.CieloCard;
import com.marioconcilio.cardmanager.util.DateUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.marioconcilio.cardmanager.cli.ConstantsCli.*;
import static java.lang.System.out;

public class CieloCli {
	
	public enum Type {
		DEBIT, CREDIT;
	}
	
	private static final int MIN_YEAR;
	private static final int MAX_YEAR;
	
	private MyBufferedReader br;
	private int year;
	private int month;
	private List<CieloCard> list;
	private Type type;
	
	static {
		int y = LocalDate.now().getYear();
		MAX_YEAR = y - 2000;
		MIN_YEAR = MAX_YEAR - 1;
	}
	
	public CieloCli(MyBufferedReader br) {
		this.br = br;
		year = 0;
		month = 0;
		list = new ArrayList<>();
		
		out.println(DOUBLE_LINE);
		out.println("Cielo");
		out.println(DOUBLE_LINE);
	}
	
	public void mainMenu() throws IOException {
		String input;
		String[] splitted = null;

		Date date = null;
		CieloCardType cardType = null;
		BigDecimal value = null;
		
		do {
			out.print("day, type (e|m|v) and value: ");
			input = br.readLine();
			
			// check if string contains digit in first position
			if (input.matches("[0-9].*")) {
				splitted = input.split("\\s+");
				
				if (splitted.length == 3) {
					final String dayStr = splitted[0];
					final String typeStr = splitted[1];
					final String valueStr = splitted[2];
					
					date = parseDate(dayStr);
					cardType = parseCardType(typeStr);
					value = parseValue(valueStr);
					
					if (date != null && cardType != null && value != null) {
						final CieloCard card = new CieloCard();
						card.setCardType(cardType);
						card.setDescription(cardType.toString());
						card.setDate(date);
						card.setValue(value);
						
						confirmCard(card);
					}
				}
			}
			else {
				if (input.equals(CHAR_HELP)) {
					showHelp();
				}
				else if (input.equals(CHAR_LIST)) {
					listAll();
				}
				else if (input.equals(CHAR_NEWDATE)) {
					enterMonthYear();
				}
				else if (input.equals(CHAR_PROCESS)) {
					process();
				}
			}
			
		} while (date == null || cardType == null || value == null);

	}
	
	public boolean confirm(String message) throws IOException {
		boolean confirmed = false;
		String input;
		
		do {
			out.print(ANSI_YELLOW);
			out.print(message);
			out.print(ANSI_RESET);
			
			input = br.readLine();
			if (input.equals(CHAR_YES)) {
				confirmed = true;
			}
			
		} while (!input.equals(CHAR_YES) && !input.equals(CHAR_NO));
		
		return confirmed;
	}

	public void confirmCard(CieloCard card) throws IOException {
		final String message = String.format("> %s (y/n)? ", card);
		if (confirm(message)) {
			list.add(card);
		}
	}
	
	public void process() throws IOException {
		final int size = list.size();
		if (size == 0) {
			out.print(ANSI_ORANGE);
			out.println("list is empty");
			out.println("add cards first");
			out.print(ANSI_RESET);
			return;
		}
		
		final String message = String.format("process %d cards (y/n)? ", size);
		if (confirm(message)) {
			out.println("processing");
			out.println(LINE);
			
			final CieloController controller = new CieloController();
			try {
				controller.execute(list, notFound -> {
					if (!notFound.isEmpty()) {
						out.print(ANSI_ORANGE);
						out.println(notFound.size() + " cards not found:");
						notFound.forEach(c -> out.println("> " + c));
						out.print(ANSI_RESET);
					}
					
					final int cardsFound = controller.getCardsFound();
					if (cardsFound > 0) {
						out.print(ANSI_GREEN);
						out.println(cardsFound + " entries updated");
						out.println(ANSI_RESET);
					}
				});
			} 
			catch (Exception e) {
				final String msg = e.getMessage();
				
				out.print(ANSI_RED);
				out.println("error on updating cards");
				if (msg != null)
					out.println(msg);
				
				out.print(ANSI_RESET);
			}
		}
	}
	
	public void enterMonthYear() throws IOException {
		month = 0;
		year = 0;
		
		String input;
		String[] splitted;
		
		do {
			out.print("month and year (mm-yy): ");
			input = br.readLine();
			
			splitted = input.split("-");
			if (splitted.length == 2) {
				try {
					month = Integer.parseInt(splitted[0]);
					year = Integer.parseInt(splitted[1]);
				}
				catch (NumberFormatException ex) {
					month = 0;
					year = 0;
				}
			}
		} while ((month < 1 || month > 12) || (year < MIN_YEAR || year > MAX_YEAR));
		
		year += 2000;
		
		out.print(ANSI_BLUE);
		out.println(DOUBLE_LINE);
		out.println(String.format("%s CARDS FROM %s/%d", type, Month.of(month).name(), year));
		out.println(DOUBLE_LINE);
		out.print(ANSI_RESET);
	}
	
	public void listAll() {
		out.print(ANSI_GREEN);
		if (list.isEmpty()) {
			out.println("list is empty!");
		}
		else {
			out.println("listing:");
			list.stream()
			.sorted((c1, c2) -> {
				// sort by date, type and value asc
				int x = c1.getDate().compareTo(c2.getDate());
				if (x == 0) {
					x = c1.getCardType().toString().compareTo(c2.getCardType().toString());
					
					if (x == 0) {
						x = c1.getValue().compareTo(c2.getValue());
					}
				}
				
				return x;
			})
			.forEach(c -> out.println("> " + c));
		}
		
		out.print(ANSI_RESET);
	}
	
	public void showHelp() {
		out.print(ANSI_ORANGE);
		out.println("commands:");
		out.println("  " + CHAR_LIST + ":\tlist all cards");
		out.println("  " + CHAR_NEWDATE + ":\tenter new month and year");
		out.println("  " + CHAR_PROCESS + ":\tprocess");
		out.println("  " + CHAR_QUIT + ":\tterminate this program");
		out.println("  " + CHAR_HELP + ":\tshow this message");
		out.print(ANSI_RESET);
	}
	
	public void setType() throws IOException {
		String input;
		
		out.print("(d)ebit | (c)redit | (q)uit: ");
		input = br.readLine();

		if (input.equals(CHAR_DEBIT)) {
			this.type = Type.DEBIT;
		}
		else if (input.equals(CHAR_CREDIT)) {
			this.type = Type.CREDIT;
		}

	}

	public void execute() throws IOException {
		setType();
		enterMonthYear();
		
		while (true) {
			mainMenu();
		}
	}
	
	/*
	 * helper methods
	 */
	
	public Date parseDate(String str) {
		Date date = null;
		
		try {
			int day = Integer.parseInt(str);
			LocalDate localDate = LocalDate.of(year, month, day);
			date = DateUtils.asDate(localDate);
		}
		catch (NumberFormatException | DateTimeException ex) {}
		
		return date;
	}
	
	public BigDecimal parseValue(String str) {
		BigDecimal value = null;
		
		try {
			value = new BigDecimal(str);
		}
		catch (NumberFormatException ex) {}
		
		return value;
	}
	
	public CieloCardType parseCardType(String str) {
		CieloCardType cardType = null;
		
		switch (type) {
			case DEBIT:
				if (str.equals(CHAR_ELO)) {
					cardType = CieloCardType.DEBITO_ELO;
				}
				else if (str.equals(CHAR_MASTER)) {
					cardType = CieloCardType.DEBITO_MASTER;
				}
				else if (str.equals(CHAR_VISA)) {
					cardType = CieloCardType.DEBITO_VISA;
				}
				break;
				
			case CREDIT:
				if (str.equals(CHAR_ELO)) {
					cardType = CieloCardType.CREDITO_ELO;
				}
				else if (str.equals(CHAR_MASTER)) {
					cardType = CieloCardType.CREDITO_MASTER;
				}
				else if (str.equals(CHAR_VISA)) {
					cardType = CieloCardType.CREDITO_VISA;
				}
				break; 
		}
		
		return cardType;
	}
	
}
