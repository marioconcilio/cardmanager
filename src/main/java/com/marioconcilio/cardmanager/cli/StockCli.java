package com.marioconcilio.cardmanager.cli;

import com.marioconcilio.cardmanager.model.dao.*;
import com.marioconcilio.cardmanager.model.vo.Product;
import org.apache.commons.cli.*;

import java.io.IOException;
import java.util.List;

import static com.marioconcilio.cardmanager.cli.ConstantsCli.ANSI_BLUE;
import static com.marioconcilio.cardmanager.cli.ConstantsCli.ANSI_RESET;
import static java.lang.System.out;

public class StockCli extends AbstractCli {
	
	public StockCli(MyBufferedReader br) {
		this.br = br;
	}
	
	public void mainMenu() throws IOException {
		final Options options = new Options();
		options.addOption("list", "listar boletos");
		options.addOption("update", "atualizar situação dos boletos");
		options.addOption(help);
		
		String input;

		while (true) {
			out.print(ANSI_BLUE);
			out.print("stock@masterprint> ");
			out.print(ANSI_RESET);

			input = br.readLine();
			if (input.equals("h") || input.equals("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("bill", options);
			}

			if (input.contains("list")) {
				listAll(input);
			}

			if (input.contains("update")) {
//				updateMenu(input);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void listAll(String cmd) {
		final String[] args = cmd.split("\\s+");
		final OptionGroup group = new OptionGroup();
		group.setRequired(true);
		group.addOption(Option.builder("b")
				.longOpt("blanq")
				.desc("blanquetas")
				.build());

		group.addOption(Option.builder("c")
				.longOpt("chapa")
				.desc("chapas")
				.build());

		group.addOption(Option.builder("d")
				.longOpt("div")
				.desc("produtos diversos")
				.build());

		group.addOption(Option.builder("e")
				.longOpt("env")
				.desc("envelopes")
				.build());
		
		group.addOption(Option.builder("q")
				.longOpt("qui")
				.desc("quimicos")
				.build());
		
		group.addOption(Option.builder("t")
				.longOpt("tinta")
				.desc("tintas")
				.build());

		group.addOption(help);
		
		final Options options = new Options();
		options.addOptionGroup(group);
		options.addOption(Option.builder("f")
				.longOpt("find")
				.desc("procurar produto")
				.hasArg()
				.build());
		
		try {
			final CommandLineParser parser = new DefaultParser();
			final CommandLine line = parser.parse(options, args);
			if (line.hasOption("h")) {
				final HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("list", options);
				return;
			}
			
			AbstractDAO<?> dao = null;
			
			if (line.hasOption("b")) {
				dao = new BlanquetaDAO();
			}
			else if (line.hasOption("c")) {
				dao = new ChapaDAO();
			}
			else if (line.hasOption("d")) {
				dao = new DiversosDAO();
			}
			else if (line.hasOption("e")) {
				dao = new EnvelopeDAO();
			}
			else if (line.hasOption("q")) {
				dao = new QuimicoDAO();
			}
			else if (line.hasOption("t")) {
				dao = new TintaDAO();
			}
			
			List<Product> list;
			if (line.hasOption("f")) {
				final String value = line.getOptionValue("f");
				list = (List<Product>) dao.fetch(value);
			}
			else {
				list = (List<Product>) dao.list();
			}
			
			printList(list);
		}
		catch (ParseException ex) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("list", options);
		}
	}
	
	private void printList(List<Product> list) {
		if (list.isEmpty()) {
			out.println("empty list!");
			return;
		}

		list.forEach(p -> System.out.println(": " + p));
	}

}
