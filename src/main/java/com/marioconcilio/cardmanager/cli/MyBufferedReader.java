package com.marioconcilio.cardmanager.cli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

import static com.marioconcilio.cardmanager.cli.ConstantsCli.CHAR_QUIT;
import static java.lang.System.out;

public class MyBufferedReader extends BufferedReader {
	
	public MyBufferedReader(Reader in) {
		super(in);
	}
	
	public MyBufferedReader(Reader in, int sz) {
		super(in, sz);
	}
	
	public static void quit() {
		out.println("bye!");
		System.exit(0);
	}
	
	@Override
	public String readLine() throws IOException {
		final String str = super.readLine().trim();
		if (str.equals(CHAR_QUIT)) {
			quit();
		}
		
		if (str.equals("exit") || str.equals("quit")) {
			quit();
		}
		
		return str;
	}

}
