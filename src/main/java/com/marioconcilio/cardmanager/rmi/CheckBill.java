package com.marioconcilio.cardmanager.rmi;

import com.marioconcilio.cardmanager.rmi.service.BillService;

import java.net.URL;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class CheckBill {
	
	public static void main(String args[]) {
		URL policyURL = CheckBill.class.getResource("/resources/client.policy");
		System.setProperty("java.security.policy", policyURL.toExternalForm());
		
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}
		
		try {
			Registry registry = LocateRegistry.getRegistry("192.168.1.27");
			BillService stub = (BillService) registry.lookup("BillService");
			
//			List<Bill> listRemessa = stub.checkRemessa();
//			List<Bill> listRetorno = stub.checkRetorno();
			
			stub.populateRemessaFile();
			stub.populateRetornoFile();
			
			System.out.println("checked");
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
