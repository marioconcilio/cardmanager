package com.marioconcilio.cardmanager.rmi;

import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RMIService<T extends Remote> {
	
	@SuppressWarnings("unchecked")
	protected T getStub(String stubName) throws RemoteException, NotBoundException {
		URL policyURL = CheckBill.class.getResource("/resources/client.policy");
		System.setProperty("java.security.policy", policyURL.toExternalForm());
		
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}
		
		Registry registry = LocateRegistry.getRegistry("192.168.1.27");
		T stub = (T) registry.lookup(stubName);
//		BillService stub = (BillService) registry.lookup("BillService");
		
		return stub;
	}
	
}
