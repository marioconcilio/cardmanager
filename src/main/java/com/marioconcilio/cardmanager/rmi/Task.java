package com.marioconcilio.cardmanager.rmi;

public interface Task<T> {
	
	T execute();

}
