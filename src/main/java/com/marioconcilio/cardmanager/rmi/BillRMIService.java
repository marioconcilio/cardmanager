package com.marioconcilio.cardmanager.rmi;

import com.marioconcilio.cardmanager.model.vo.Bill;
import com.marioconcilio.cardmanager.rmi.service.BillService;

import java.util.List;

public class BillRMIService extends RMIService<BillService> {
	
	public List<Bill> checkRemessa() throws Exception {
		BillService stub = getStub("BillService");
		return stub.checkRemessa();
	}
	
	public List<Bill> checkRetorno() throws Exception {
		BillService stub = getStub("BillService");
		return stub.checkRetorno();
	}
	
	public void populateRemessaFile() throws Exception {
		BillService stub = getStub("BillService");
		stub.populateRemessaFile();
	}
	
	public void populateRetornoFile() throws Exception {
		BillService stub = getStub("BillService");
		stub.populateRetornoFile();
	}

}
