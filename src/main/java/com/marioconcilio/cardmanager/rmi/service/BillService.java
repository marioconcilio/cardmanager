package com.marioconcilio.cardmanager.rmi.service;

import com.marioconcilio.cardmanager.model.vo.Bill;

import java.rmi.Remote;
import java.util.List;

public interface BillService extends Remote {
	
	public List<Bill> checkRemessa() throws Exception;
	public List<Bill> checkRetorno() throws Exception;
	public void populateRemessaFile() throws Exception;
	public void populateRetornoFile() throws Exception;
 
}
