package com.marioconcilio.cardmanager.rmi.service;

import com.marioconcilio.cardmanager.rmi.Task;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TaskService extends Remote {
	
	<T> T executeTask(Task<T> t) throws RemoteException;

}
