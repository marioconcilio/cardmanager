package com.marioconcilio.cardmanager;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

public class SplashScreen extends JWindow {

	private static final long serialVersionUID = -3803128508336345036L;
	
	private static JProgressBar progressBar;
	private static JLabel label;
	
	static {
		progressBar = new JProgressBar(0, 100);
		progressBar.setIndeterminate(true);
		progressBar.setForeground(Color.WHITE);
		progressBar.setOpaque(false);
		
		label = new JLabel("Iniciando...");
		label.setFont(new Font("SansSerif", Font.PLAIN, 14));
		label.setForeground(Color.WHITE);
	}
	
	public SplashScreen() {
		JPanel content = (JPanel) getContentPane();
		
		ImagePanel imagePanel = new ImagePanel(getClass().getClassLoader().getResource("icons/splash-screen.jpg"));
		imagePanel.setLayout(new BoxLayout(imagePanel, BoxLayout.Y_AXIS));
		imagePanel.setBorder(new EmptyBorder(30, 30, 30, 30));

		imagePanel.add(Box.createVerticalGlue());
		imagePanel.add(label);
		imagePanel.add(Box.createRigidArea(new Dimension(0, 20)));
		imagePanel.add(progressBar);
		content.add(imagePanel, BorderLayout.CENTER);

		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int width = imagePanel.getWidth();
		int height = imagePanel.getHeight();
		int x = (screen.width - width)/2;
		int y = (screen.height - height)/2;
		setBounds(x, y, width, height);

//		Color oraRed = new Color(156, 20, 20,  255);
//		content.setBorder(BorderFactory.createLineBorder(oraRed, 1));      

		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		SplashScreen splash = new SplashScreen();
		splash.setVisible(true);
		
		MainApp.setOnChange((progress, text) -> {
			if (progress == 100) {
				splash.dispose();
			}
			
//			progressBar.setIndeterminate(false);
//			progressBar.setValue(progress);
			label.setText(text);
		});
		
//		Runnable task = () -> MainApp.main(args);
//		new Thread(task).start();
		
		MainApp.main(args);
	}

	private final class ImagePanel extends JPanel {

		private static final long serialVersionUID = -947924349890577042L;

		private BufferedImage image;
		
		public ImagePanel(URL imageURL) {
			try {                
				image = ImageIO.read(imageURL);
			}
			catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.drawImage(image, 0, 0, null);
		}
		
		@Override
		public int getWidth() {
			return image.getWidth();
		}
		
		@Override
		public int getHeight() {
			return image.getHeight();
		}
	}
}
