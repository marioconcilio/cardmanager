package com.marioconcilio.cardmanager.util;

import javax.swing.text.MaskFormatter;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

public class Formatter {
	
	private static SimpleDateFormat dateFormat, dateWithoutMaskFormat;
	private static Locale locale;
	private static NumberFormat numberFormat;

	static {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateWithoutMaskFormat = new SimpleDateFormat("ddMMyyyy");
		locale = new Locale("pt", "BR");
		numberFormat = NumberFormat.getCurrencyInstance(locale);
	}
	
	/**
	 * Parse String date to java.util.Date
	 * @param str String date with format dd/MM/yyyy
	 * @return java.util.Date
	 */
	public static Date parseDate(String str) {
		Date date = null;
		
		try {
			date = dateFormat.parse(str);
		}
		catch(Exception ex) {
//			ex.printStackTrace();
		}
		
		return date;
	}
	
	/**
	 * Parse String date to java.util.Date
	 * @param str String date with format ddMMyyyy
	 * @return java.util.Date
	 */
	public static Date parseDateWithoutMask(String str) {
		Date date = null;
		
		try {
			date = dateWithoutMaskFormat.parse(str);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return date;
	}
	
	/**
	 * Format java.util.Date to String
	 * @param date The date to parse
	 * @return String with format dd/MM/yyyy
	 */
	public static String formatDate(Date date) {
		return dateFormat.format(date);
	}
	
	/**
	 * Parse String with value to BigDecimal
	 * @param value String with format 1.234,56 or R$ 1.234,56
	 * @return BigDecimal with value
	 */
	public static BigDecimal parseCurrency(String value) {
		return new BigDecimal(value.replaceAll("\\.", "").replaceAll(",", ".").replace("R$", "").trim());
	}
	
	public static int parseCurrencyToInt(String value) {
		if (value.contains(",")) {
			return Integer.parseInt(value.replaceAll("[^0-9]", "")) / 100;
		}
		
		return Integer.parseInt(value.replaceAll("[^0-9]", ""));
	}
	
	/**
	 * Format BigDecimal to local currency
	 * @param value BigDecimal value
	 * @return String with format R$ 1.234,56
	 */
	public static String formatCurrency(BigDecimal value) {
		return numberFormat.format(value);
	}
	
	/**
	 * Format double to local currency
	 * @param value double value
	 * @return String with format R$ 1.234,56
	 */
	public static String formatCurrency(double value) {
		return numberFormat.format(value);
	}
	
	/**
	 * Transforms java.time.LocalDate to java.util.Date
	 * Useful on JavaFX TableView to Hibernate
	 * @param localDate
	 * @return java.util.Date
	 */
	public static Date toDate(LocalDate localDate) {
		return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}
	
	/**
	 * Transforms java.util.date to java.time.LocalDate
	 * Useful on Hibernate persistence to JavaFX TableView
	 * @param date
	 * @return java.time.LocalDate
	 */
	public static LocalDate toLocalDate(Date date) {
		return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).toLocalDate();
	}
	
	/**
	 * Parse String containing CNPJ to long
	 * @param cnpj String CNPJ
	 * @return long
	 */
	public static long parseCnpj(String cnpj) {
		return Long.parseLong(cnpj.replaceAll("[^0-9]", ""));
	}
	
	/**
	 * Format long cnpj to String
	 * @param cnpj long cnpj
	 * @return String with format 00.000.000/0000-00
	 */
	public static String formatCnpj(long cnpj) {
		String str = String.format("%014d", cnpj);
		MaskFormatter mask;
		
		try {
			mask = new MaskFormatter("##.###.###/####-##");
			mask.setValueContainsLiteralCharacters(false);
			
			return mask.valueToString(str);
		} 
		catch (ParseException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Parse String containing Cep to int
	 * @param cep String Cep
	 * @return int
	 */
	public static int parseCep(String cep) {
		return Integer.parseInt(cep.replaceAll("[^0-9]", ""));
	}
	
	/**
	 * Format int cep to String
	 * @param cep int cep
	 * @return String with format 00000-000
	 */
	public static String formatCep(int cep) {
		String str = String.format("%08d", cep);
		MaskFormatter mask;
		
		try {
			mask = new MaskFormatter("#####-###");
			mask.setValueContainsLiteralCharacters(false);
			
			return mask.valueToString(str);
		} 
		catch (ParseException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Method to format bill number that is less than 1000.<br>
	 * i.e.<br>
	 * if number has 001, 002 and so on, the formatted output is 123 001, 123 002<br>
	 * if number does not have 001, 002, the formatted output is always with three digits e.g. 056
	 * @param number The bill number
	 * @return Formatted string with bill number
	 */
	public static String formatBillNumber(long number) {
		if (number / 1000 > 0) {
			// number probably has 001
			String str = String.format("%06d", number);
			
			MaskFormatter mask;
			try {
				mask = new MaskFormatter("### ###");
				mask.setValueContainsLiteralCharacters(false);
				
				return mask.valueToString(str);
			}
			catch (ParseException e) {
				return null;
			}
		}
		
		// number does not have 001
		return String.format("%03d", number);
	}
	
	/**
	 * Capitalizes the first letter of String str.<br>
	 * i.e.<br>
	 * "one two three" produces "One two three".
	 * @param str The String.
	 * @return String with first letter capitalized.
	 */
	public static String upperCaseFirst(String str) {
		char[] array = str.toCharArray();
		array[0] = Character.toUpperCase(array[0]);
		return new String(array);
	}
	
	/**
	 * Capitalizes all words from String str.<br>
	 * i.e.<br>
	 * "one two three" produces "One Two Three".
	 * @param str The String.
	 * @return String with all words capitalized.
	 */
	public static String upperCaseAll(String str) {
		char[] array = str.toCharArray();
		array[0] = Character.toUpperCase(array[0]);
		
		for (int i=1; i<array.length; i++) {
			if (Character.isWhitespace(array[i-1]))
				array[i] = Character.toUpperCase(array[i]);
		}

		return new String(array);
	}
	
	public static int parseInt(String str) {
		int number;
		try {
			number = Integer.parseInt(str.replaceAll("\\s+", ""));
		}
		catch (NumberFormatException e) {
			number = -1;
		}
		
		return number;
	}
	
	public static long parseLong(String str) {
		long number;
		try {
			number = Long.parseLong(str.replaceAll("\\s+", ""));
		}
		catch (NumberFormatException ex) {
			number = -1;
		}

		return number;
	}
	
}
