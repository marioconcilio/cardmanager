package com.marioconcilio.cardmanager.util;

public class Constants {
	
	public static final String APP_NAME 	= "MasterPrint";
	public static final String APP_VERSION 	= "v0.2";
	
	public static final String ALIGNMENT_CENTER 	= "-fx-alignment: CENTER;";
	public static final String ALIGNMENT_RIGHT 	= "-fx-alignment: CENTER-RIGHT;";
	public static final String ALIGNMENT_LEFT 		= "-fx-alignment: CENTER-LEFT;";
	
}
