package com.marioconcilio.cardmanager.util;

import org.apache.commons.codec.binary.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Cipher {
	
	private static String hash(String valor, String algoritmo) throws NoSuchAlgorithmException {
        Base64 encoder = null;
        MessageDigest md = null;
        
        if (valor == null) {
            throw new IllegalArgumentException("O valor é nulo");
        }
        
        encoder = new Base64(0);
        md = MessageDigest.getInstance(algoritmo);
        md.reset();      
        md.update(valor.getBytes());
        
        return encoder.encodeToString(md.digest());        
    }
	
	public static String digestSHA512(String original) {
		String hashText = "";
		
        try {
            hashText = hash(original, "SHA-512");
        }
        catch (NoSuchAlgorithmException ex) {
           	ex.printStackTrace();
        }
        
        return hashText;
	}

}
