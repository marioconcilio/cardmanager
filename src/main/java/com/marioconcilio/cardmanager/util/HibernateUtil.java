package com.marioconcilio.cardmanager.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.internal.SessionImpl;
import org.hibernate.service.ServiceRegistry;

import java.sql.Connection;

/**
 * Hibernate Utility class with a convenient method to get Session Factory object.
 * jdbc:h2:tcp://192.168.1.27:9092/~/cardmanager/db
 * @author Mario Concilio
 *
 */
public class HibernateUtil {
	
	private static final SessionFactory sessionFactory;
	private static final ServiceRegistry serviceRegistry;
	private static final Configuration configuration;
	
	static {   
        configuration = new Configuration().configure("hibernate.cfg.xml");
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        sessionFactory = configuration.configure("hibernate.cfg.xml").buildSessionFactory(serviceRegistry);
        
        if (!configuration.getProperty("hibernate.connection.url").equals("jdbc:postgresql://192.168.1.27:5432/masterpg")) {
        	System.out.println("+------------------------------------------------------------+");
        	System.out.println("|                     !!! WARNING !!!                        |");
        	System.out.println("|                   USING TEST DATABASE                      |");
        	System.out.println("+------------------------------------------------------------+");
        	System.out.println();
        }
    }
    
    public static Session getSession() {
    	Session session = sessionFactory.openSession();
        return session;
    }
    
    public static Connection getConnection() {
    	SessionImpl sessionImpl = (SessionImpl) sessionFactory.openSession();
    	return sessionImpl.connection();
    }
}
