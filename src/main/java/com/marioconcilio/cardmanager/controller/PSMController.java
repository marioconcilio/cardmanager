package com.marioconcilio.cardmanager.controller;

import com.marioconcilio.cardmanager.cli.ConstantsCli;
import com.marioconcilio.cardmanager.model.dao.PSMCardDAO;
import com.marioconcilio.cardmanager.model.vo.Card.PSMCardType;
import com.marioconcilio.cardmanager.model.vo.PSMCard;
import com.marioconcilio.cardmanager.util.Formatter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static com.marioconcilio.cardmanager.model.vo.Card.PSMCardType.*;

public class PSMController {
	
	private static final String TAG_UUID		= "Transacao_ID";
	private static final String TAG_LIST		= "Table";
	private static final String TAG_CLIENT	= "Cliente_Nome";
	private static final String TAG_DESCR	= "Tipo_Pagamento";
	private static final String TAG_STATUS 	= "Status";
	private static final String TAG_VALUE 	= "Valor_Bruto";
	private static final String TAG_DATE 	= "Data_Transacao";
	private static final String TAG_SHARES 	= "Parcelas";
	
	private List<PSMCard> notFound = null;
	private List<PSMCard> entities = null;
	
	public List<PSMCard> processExtrato(File xmlFile) throws Exception {
		final InputStream inputStream = new FileInputStream(xmlFile);
		final Reader reader = new InputStreamReader(inputStream, "ISO-8859-1");
		final InputSource is = new InputSource(reader);
		
		final DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		final Document doc = builder.parse(is);
		doc.getDocumentElement().normalize();
		
		final NodeList list = doc.getElementsByTagName(TAG_LIST);
		final List<PSMCard> cardList = new ArrayList<>();
		
		for (int i=0; i<list.getLength(); i++) {
			final Node node = list.item(i);
			final PSMCard card = nodeToCard(node);
			
			if (card != null)
				cardList.add(card);
		}
		
		return cardList;
	}
	
	public PSMCard nodeToCard(Node node) {
		if (node.getNodeType() != Node.ELEMENT_NODE)
			return null;
		
		final Element element = (Element) node;
		
		// if payment was not approved or if it is not Moderninha
		final String client = element.getElementsByTagName(TAG_CLIENT).item(0).getTextContent();
		final String status = element.getElementsByTagName(TAG_STATUS).item(0).getTextContent();
		if (!status.contains("Aprovada") || !client.contains("Venda pela Moderninha"))
			return null;
		
		final String uuid = element.getElementsByTagName(TAG_UUID).item(0).getTextContent();
		final String descr = element.getElementsByTagName(TAG_DESCR).item(0).getTextContent();
		final String value = element.getElementsByTagName(TAG_VALUE).item(0).getTextContent();
		final String date = element.getElementsByTagName(TAG_DATE).item(0).getTextContent();
		final int shares = Integer.parseInt(element.getElementsByTagName(TAG_SHARES).item(0).getTextContent());
		
		// type of payment
		PSMCardType cardType;
		if (descr.contains("Débito")) {
			cardType = DEBITO;
		}
		else if (descr.contains("Crédito")) {
			cardType = (shares > 1)? CREDITO_PARCELADO : CREDITO_AVISTA;
		}
		else {
			cardType = OTHER;
		}
		
		final PSMCard card = new PSMCard();
		card.setUUID(UUID.fromString(uuid));
		card.setDescription(descr);
		card.setShares(shares);
		card.setValue(Formatter.parseCurrency(value));
		card.setDate(Formatter.parseDate(date));
		card.setCardType(cardType);
		card.setOk(false);
		
		return card;
	}
	
	public List<PSMCard> processList(List<PSMCard> list) {
		notFound = new ArrayList<>(list);
		entities = new ArrayList<>();
		
		final List<PSMCard> found = new ArrayList<>();
		final PSMCardDAO dao = new PSMCardDAO();
		
		// temporary list holding card from same CardType
		List<PSMCard> temp = null;
		
		// iterate through all CardTypes
		final PSMCardType[] values = PSMCardType.values();
		for (PSMCardType type : values) {	
			
			// populate tempList with only one CardType
			temp = list.stream()
					.filter(card -> card.getCardType() == type)
					.collect(Collectors.toList());
			
			// check if tempList has elements
			if (!temp.isEmpty()) {
				System.out.println(ConstantsCli.DOUBLE_LINE);
				System.out.println(type);
				
				// group cards by date
				final Map<Date, List<PSMCard>> dateMap = temp.stream()
						.collect(Collectors.groupingBy(c -> c.getDate()));
				
				// for each date on map, process list
				dateMap.forEach((date, cardList) -> {
					System.out.println(ConstantsCli.LINE);
					System.out.println("processing list:");
					cardList.forEach(c -> System.out.println(": " + c));
					
					List<BigDecimal> valueList = dao.listCardsFrom(date, type)
							.stream()
							.map(c -> c.getValue())
							.collect(Collectors.toList());
					
					valueList.forEach(v -> System.out.println("target: " + Formatter.formatCurrency(v)));
					if (!valueList.isEmpty()) {
						valueList.forEach(value -> {
							final List<PSMCard> f = cardsSubsetSum(cardList, value);
							if (f != null) {
								found.addAll(f);
							}
							
							final PSMCard entity = dao.fetchCard(date, type, value);
							if (entity != null) {
								entities.add(entity);
							}
						});
					}
				});
				
				// remove cards already iterated
				list.removeAll(temp);
				
				// clear tempList to start over again
				temp.clear();
			}
			
			// go to next cardType
		}
		
		notFound.removeAll(found);
		return found;
	}
	
	public List<PSMCard> cardsSubsetSum(List<PSMCard> list, BigDecimal value) {
		final List<PSMCard> found = new ArrayList<>();
		for (int i = 0; i< list.size(); i++) {
			if (hasSubsetSum(list, i, found, value)) {
				System.out.println("MATCH!");
				return found;
			}
		}
		
		return null;
	}
	
	private boolean hasSubsetSum(List<PSMCard> list, int index, List<PSMCard> subset, BigDecimal value) {
		if (index >= list.size()) {
			return false;
		}
		
		final PSMCard card = list.get(index);
		final BigDecimal sub = value.subtract(card.getValue());
		if (sub.compareTo(BigDecimal.ZERO) == 0) {
			subset.add(card);
			
			System.out.println(ConstantsCli.LINE);
			subset.forEach(c -> System.out.println("\t: " + c));
			BigDecimal total = subset.stream()
					.map(c -> c.getValue())
					.reduce(BigDecimal.ZERO, BigDecimal::add);
			System.out.println("\tttotal: " + Formatter.formatCurrency(total));
			return true;
		}
		
		if (sub.compareTo(BigDecimal.ZERO) < 0) {
			return false;
		}

		value = value.subtract(card.getValue());
		for (int i = index+1; i < list.size(); i++) {
			if (hasSubsetSum(list, i, subset, value)) {
				subset.add(card);
				
				System.out.println(ConstantsCli.LINE);
				subset.forEach(c -> System.out.println("\t: " + c));
				BigDecimal total = subset.stream()
						.map(c -> c.getValue())
						.reduce(BigDecimal.ZERO, BigDecimal::add);
				System.out.println("\ttotal: " + Formatter.formatCurrency(total));
				return true;
			}
		}
		
		return false;
	}
 
	public final List<PSMCard> getNotFound() {
		return notFound;
	}

	public final List<PSMCard> getEntities() {
		return entities;
	}
	
}
