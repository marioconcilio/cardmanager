package com.marioconcilio.cardmanager.controller;

import com.marioconcilio.cardmanager.model.dao.BillDAO;
import com.marioconcilio.cardmanager.model.dao.ClientDAO;
import com.marioconcilio.cardmanager.model.vo.Bill;
import com.marioconcilio.cardmanager.model.vo.Bill.BillStatus;
import com.marioconcilio.cardmanager.model.vo.Client;
import com.marioconcilio.cardmanager.util.Formatter;
import one.util.streamex.StreamEx;

import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BillController {
	
	private static final String RET_LINE 		= "7 2513";
	private static final String REM_LINE1		= "90019008869";
	private static final String REM_LINE2		= "0000000000000000000000012345678";
	
	private static final String PAGO        	= "LQ";
	private static final String CARTORIO 		= "TEC";
	private static final String PROTESTO 	    = "BXP";
	
	public List<Bill> processListRetorno(List<File> fileList) throws IOException {
		final List<File> sortedList = fileList.stream()
				.sorted(this::sortRetornoFiles)
				.collect(Collectors.toList());

		final List<Bill> list = new ArrayList<>();
		for (File file : sortedList) {
			list.addAll(processRetorno(file));
		}
		
		return list;
	}
	
	public List<Bill> processRetorno(File file) throws IOException {
		return Files.lines(file.toPath())
				.filter(line -> line.contains(RET_LINE))
				.map(this::updateBill)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}
	
	public List<Bill> processListRemessa(List<File> fileList) throws IOException {
		List<Bill> list = new ArrayList<>();
		for (File file : fileList) {
			list.addAll(processRemessa(file));
		}
		
		return list;
	}
	
	public List<Bill> processRemessa(File file) throws IOException {
		Stream<String> lines = Files.lines(file.toPath())
				.filter(line -> line.contains(REM_LINE1) || line.contains(REM_LINE2));

		return StreamEx.of(lines)
				.pairMap(this::recoverBill)
				.filter(Objects::nonNull)
				.toList();
	}
	
	Bill recoverBill(String l1, String l2) {
		if (l1.contains(REM_LINE2)) return null;
		
		/*
		 * process doc number, date and value
		 */
		String[] splitted = l1.split("\\s+");
		long num = Long.parseLong(splitted[3].substring(5));
		Date date = Formatter.parseDateWithoutMask(splitted[4].substring(0, 8));
		String rawValue = splitted[4].substring(8, 23);
		String formattedValue = new StringBuffer(rawValue).insert(rawValue.length()-2, '.').toString();
		BigDecimal value = new BigDecimal(formattedValue);
		
		/*
		 * get client data
		 */
		String cnpj = l2.substring(19,33);
		System.out.println("searching cnpj: "+cnpj);
		
		ClientDAO dao = new ClientDAO();
		Client client = dao.getClientFromCnpj(Long.parseLong(cnpj));
		if (client == null) {
			System.out.println("not found. searching client data...");
			String name = l2.substring(33, 73).trim();
			System.out.println("\tname: "+name);
			
			String address = l2.substring(73, 128).trim();
			System.out.println("\taddress: "+address);
			
			String cep = l2.substring(128, 136);
			System.out.println("\tcep: "+cep);
			
			String city = l2.substring(136, 151).trim();
			System.out.println("\tcity: "+city);
			
			String state = l2.substring(151, 153);
			System.out.println("\tstate: "+state);
			
			client = new Client();
			client.setId(Long.parseLong(cnpj));
			client.setName(name);
			client.setAddress(address);
			client.setCep(Integer.parseInt(cep));
			client.setCity(city);
			client.setState(state);
			client.setCreditLimit(2000);
			
			System.out.println("new client: " + client);

			try {
				dao.save(client);
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Bill bill = new Bill();
		bill.setId(num);
		bill.setDate(date);
		bill.setValue(value);
		bill.setStatus(BillStatus.ABERTO);
		bill.setClient(client);
		client.getBills().add(bill);
		return bill;
	}
	
	Bill updateBill(String str) {
        long number;
        String occ;

		try {
			number = Long.parseLong(str.substring(21, 34).trim());
			occ = str.substring(83, 86);
		}
		catch (Exception e) {
			return null;
		}

		BillStatus status;
		if (occ.contains(PAGO)) {
			status = BillStatus.PAGO;
		}
		else if (occ.equals(CARTORIO)) {
			status = BillStatus.CARTORIO;
		}
		else if (occ.equals(PROTESTO)) {
			status = BillStatus.PROTESTADO;
		}
		else {
			return null;
		}

		Bill bill = new BillDAO().getBillFromNumber(number);
		if (bill == null) return null;

		bill.setStatus(status);
		return bill;
	}

	/**
	 * Sort RETORNO files by its
	 * @param file1 The first file to be compared.
	 * @param file2 The second file to be compared.
	 * @return -1 if file1 is older, 1 if newer, and 0 if they are equal.
	 */
	int sortRetornoFiles(File file1, File file2) {
		String line1, line2;

		try (final BufferedReader br1 = new BufferedReader(new FileReader(file1));
			 final BufferedReader br2 = new BufferedReader(new FileReader(file2))) {
			line1 = br1.readLine().trim();
			line2 = br2.readLine().trim();
		}
		catch (IOException e) {
			e.printStackTrace();
			return 0;
		}

		final BigInteger i1 = new BigInteger(line1.substring(9));
		final BigInteger i2 = new BigInteger(line2.substring(9));

		return i1.compareTo(i2);
	}

}
