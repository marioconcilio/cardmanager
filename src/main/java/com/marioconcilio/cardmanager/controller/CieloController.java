package com.marioconcilio.cardmanager.controller;

import com.marioconcilio.cardmanager.cli.ConstantsCli;
import com.marioconcilio.cardmanager.model.dao.CieloCardDAO;
import com.marioconcilio.cardmanager.model.dao.SettingsDAO;
import com.marioconcilio.cardmanager.model.vo.Card.CieloCardType;
import com.marioconcilio.cardmanager.model.vo.CieloCard;
import com.marioconcilio.cardmanager.model.vo.Settings;
import com.marioconcilio.cardmanager.util.Formatter;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class CieloController {
	
	private static final BigDecimal ERROR 		= BigDecimal.valueOf(0.05);
	private static final BigDecimal HALF_ERROR 	= BigDecimal.valueOf(0.025);
	private static final String NDOC 			= "4862049";
	private static final int NDOC_POSITION 		= -3;
	private static final int VALUE_POSITION 	= -2;
	private static final int DATE_POSITION 		= 0;
	private static final String STOP_STRING 	= "Últimos Lançamentos";

	private BigDecimal rateDebit, rateCreditOnDemand, rateCreditOnTerm;
	private List<CieloCard> cardsNotFound;
	private int cardsFound = 0;
	
	/**
	 * Controller that receives a list card, calculate all the rates and organize by CardType and date.
	 * Then fetch the database and try to find a match. 
	 * If found, automatically flags the Card.
	 * If not, all the cards not found are available through <code>getCardsNotFound</code>
	 */
	public CieloController() {
		this.cardsNotFound = new ArrayList<>();
	}

	public List<CieloCard> processExtrato(File file) throws IOException {
		final List<String> lines = Files.readAllLines(file.toPath());
		int index = 0;
		for (String line : lines) {
			if (line.contains(STOP_STRING)) break;
			index++;
		}

		final List<CieloCard> list = lines.subList(0, index)
				.stream()
                .filter(line -> line.contains(NDOC))
                .map(this::createCard)
                .collect(Collectors.toList());

		return list;
	}

	private CieloCard createCard(String str) {
		String[] splitted = str.split("\\s+");
		int size = splitted.length;

		StringBuilder description = new StringBuilder();
		for (int i = 1; i < size + NDOC_POSITION; i++) {
			description.append(splitted[i]);
			description.append(' ');
		}

		CieloCardType type = CieloCardType.OTHER;
		String strType = description.toString().toUpperCase();

		if(strType.contains("DEBITO ELO")) {
			type = CieloCardType.DEBITO_ELO;
		}
		else if(strType.contains("DEBITO MASTER")) {
			type = CieloCardType.DEBITO_MASTER;
		}
		else if(strType.contains("VISA ELECTRON")) {
			type = CieloCardType.DEBITO_VISA;
		}
		else if(strType.contains("CREDITO MASTER")) {
			type = CieloCardType.CREDITO_MASTER;
		}
		else if(strType.contains("VENDA CARTAO DE CREDITO")) {
			type = CieloCardType.CREDITO_VISA;
		}

		final CieloCard card = new CieloCard();
		card.setDate(Formatter.parseDate(splitted[DATE_POSITION]));
		card.setValue(Formatter.parseCurrency(splitted[size + VALUE_POSITION]));
		card.setDescription(description.toString());
		card.setCardType(type);
		card.setOk(false);

		return card;
	}
	
	public void execute(List<CieloCard> listCard, Settings s, Consumer<List<CieloCard>> notFoundConsumer) throws Exception {
		this.rateDebit = new BigDecimal(s.getRateDebit());
		this.rateCreditOnDemand = new BigDecimal(s.getRateCreditOnDemand());
		this.rateCreditOnTerm = new BigDecimal(s.getRateCreditOnTerm());
		
		try {
			splitListByCardType(listCard);
			notFoundConsumer.accept(this.cardsNotFound);
		} 
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Execute the fetch and all the calculations. Here is where the magic happens.
	 * @param listCard The List with Cards to fetch the database
	 * @param notFoundConsumer Consumer which returns List of Cards not found
	 */
	public void execute(List<CieloCard> listCard, Consumer<List<CieloCard>> notFoundConsumer) throws Exception {
		// load rates from database
		new SettingsDAO().listAll(list -> {
			final Settings s = list.get(0);
			this.rateDebit = new BigDecimal(s.getRateDebit());
			this.rateCreditOnDemand = new BigDecimal(s.getRateCreditOnDemand());
			this.rateCreditOnTerm = new BigDecimal(s.getRateCreditOnTerm());
			
			try {
				splitListByCardType(listCard);
				notFoundConsumer.accept(this.cardsNotFound);
			} 
			catch (Exception e) {
				throw new RuntimeException(e);
			}
			
		});
		
	}
	
	private void splitListByCardType(List<CieloCard> listCard) throws Exception {
		// sort list by date asc
//		listCard.sort((c1, c2) -> c1.getDate().compareTo(c2.getDate()));
		
		// temporary list holding card from same CardType
		List<CieloCard> tempList = new ArrayList<CieloCard>();
		
		// iterate through all CardTypes
		final CieloCardType[] values = CieloCardType.values();
		for (CieloCardType type : values) {	
			
			// populate tempList with only one CardType
			tempList = listCard.stream()
					.filter(card -> card.getCardType() == type)
					.collect(Collectors.toList());
			
			// check if tempList has elements
			if (!tempList.isEmpty()) {
				System.out.println(type);
				
				// group cards by date
				Map<Date, List<CieloCard>> groupByDate = tempList.stream().collect(Collectors.groupingBy(c -> c.getDate()));
				
				// for each date on map, process list
				groupByDate.forEach((d, l) -> {
						try {
							this.processListFromDate(d, l);
						} 
						catch (Exception e) {
							throw new RuntimeException(e);
						}
					});
				
				// remove cards already iterated
				listCard.removeAll(tempList);
				
				// clear tempList to start over again
				tempList.clear();
			}
			
			// go to next cardType
		}
	}
	
	/**
	 * Process list of cards of same CardType, get the total and fetch database for any match
	 * @param date the date of cards in list
	 * @param listCard
	 */
	private void processListFromDate(Date date, List<CieloCard> listCard) throws Exception {
		// get the total sum of all cards in listCard
		BigDecimal total = listCard.stream()
				.map(card -> card.getValue())
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		
		System.out.println(Formatter.formatDate(date) + ": ");
		listCard.forEach(card -> System.out.println("\t" + card));
		System.out.println("total: " + total);
		
		// fetch database for cards entry that match listCard
		List<CieloCard> cardsFromDB = getListCardFrom(date, listCard.get(0).getCardType(), total);
		
		// if list is null, have not found any entry, add all listCard to cardsNotFound
		if (cardsFromDB == null) {
			this.cardsNotFound.addAll(listCard);
		}
		// if has found, mark them on database as ok
		else {
			final CieloCardDAO dao = new CieloCardDAO();
			dao.updateAllOK(cardsFromDB);
			
			this.cardsFound += cardsFromDB.size();
		}
	}
	
	/**
	 * Fetch database for entries that match CardType and value and are older than date
	 * @param date starting date for the backward search
	 * @param type the PSMCardType
	 * @param value
	 * @return list of cards that match the entry, or null if have not found anything
	 */
	private List<CieloCard> getListCardFrom(Date date, CieloCardType type, BigDecimal value) {
		value = value.multiply(getRate(type)).add(HALF_ERROR);
		System.out.println("with rate and margin: " + value);
		
		if (type == CieloCardType.CREDITO_MASTER || type == CieloCardType.CREDITO_VISA) {
			// 27 days later
			// credit on demand happens 30 days later
			// 27 so the listCardsFrom query 7 days forward, giving margin of 3 days before and 4 days after date
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.DATE, 27);
			date = calendar.getTime();
		}
		
		// get cards with less values than the original
		List<CieloCard> lessValues = new CieloCardDAO().listCardsFrom(date, type, value);
		System.out.println("query result: " + lessValues.size() + " elements");
		
		// try to find card
		CieloCard card = null;
		for (CieloCard c : lessValues) {
			// calculate the margin error
			BigDecimal diff = c.getValue()
					.subtract(value)
					.abs();
			
			System.out.println(c + "\tdiff: " + diff);
			
			// consider the margin error
			if (diff.compareTo(ERROR) <= 0) {
				card = c;
				System.out.print(" -> match!");
				break;
			}
			
			System.out.println(ConstantsCli.LINE);
		}
		
		// if found card
		if (card != null) {
			List<CieloCard> list = new ArrayList<CieloCard>(1);
			list.add(card);
			
			return list;
		}
		
		// if card not found, try to find combination
		System.out.println("card not found, trying combinations...");
		return this.getCardCombination(lessValues, value, new ArrayList<CieloCard>());
	}
	
	/**
	 * Try to find combination to match the sum, like subset-sum problem. Recursively.
	 * See <code>http://stackoverflow.com/questions/4632322/finding-all-possible-combinations-of-numbers-to-reach-a-given-sum</code>
	 * @param listCard The list to search
	 * @param value The value to search for
	 * @param partial Should be an empty list. Used only on recursion
	 * @return List of cards that match the requirements, null if no card found
	 */
	private List<CieloCard> getCardCombination(List<CieloCard> listCard, BigDecimal value, List<CieloCard> partial) {	
		if (partial.isEmpty())
			System.out.println("partial list empty");
		else
			partial.forEach(card -> System.out.println("\t" + card));
		
		// sum all card values from partial list
		BigDecimal sum = partial.stream()
				.map(card -> card.getValue())
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		
		BigDecimal diff = sum.subtract(value).abs();
		
		System.out.println("sum: " + sum + ", diff: " + diff);
		
		// consider the margin error
		if (diff.compareTo(ERROR) <= 0) {
			System.out.print(" -> match!");
			return partial;
		}
		
		System.out.println(ConstantsCli.LINE);
		
		for(int i = 0; i < listCard.size(); i++) {
            ArrayList<CieloCard> remaining = new ArrayList<CieloCard>();
            CieloCard card = listCard.get(i);
            
            for (int j=i+1; j<listCard.size();j++)
            	remaining.add(listCard.get(j));
            
            ArrayList<CieloCard> partial_rec = new ArrayList<CieloCard>(partial);
            partial_rec.add(card);
            
            getCardCombination(remaining, value, partial_rec);
		}

		return null;
	}
	
	private BigDecimal getRate(CieloCardType type) {
		// TODO: settings should have rate for each PSMCardType
		BigDecimal rate;
		
		if(type == CieloCardType.DEBITO_ELO ||
				type == CieloCardType.DEBITO_MASTER ||
				type == CieloCardType.DEBITO_VISA) {
			rate = rateDebit;
		}
		else {
			rate = rateCreditOnDemand;
		}
		
		rate = rate.divide(new BigDecimal("100"))
				.subtract(new BigDecimal("1"))
				.abs();
		
		return rate;
	}

	/**
	 * Getter for <code>cardsNotFound</code>
	 * @return List of cards not found in database
	 */
	/*
	public List<CieloCard> getCardsNotFound() {
		if (this.cardsNotFound.size() > 0) {
			logger.debug("entries not found:");
			this.cardsNotFound.forEach(card -> logger.debug("\t{}", card));
		}
		else {
			logger.debug("all entries found!");
		}
		
		return this.cardsNotFound;
	}
	*/
	
	public int getCardsFound() {
		return this.cardsFound;
	}

}
